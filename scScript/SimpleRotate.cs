﻿using System.Collections;
using UnityEngine;

public class SimpleRotate : MonoBehaviour {

	private bool isRotate;

	public bool IsRotate
	{
		get {return isRotate;}
		set {isRotate=value;}
	}

	[SerializeField]
	private float rotationSpeed;

	public float RotationSpeed
	{
		get {return this.rotationSpeed;}
		set {rotationSpeed=value;}
	}

	// Use this for initialization
	void Start () {
		this.isRotate=false;
		if (this.rotationSpeed==null)
			this.rotationSpeed=150f;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.isRotate)
		{
			transform.Rotate(0f,this.rotationSpeed * Time.deltaTime,0f);
		}
	}
}
