using System.Collections;
using UnityEngine;
using Vuforia;

public class SwitchColor : MonoBehaviour, IVirtualButtonEventHandler
{
	private bool switchState = true;
	public GameObject myGameObject;

	void Start () {
		VirtualButtonBehaviour[] virtualButtons=
		GetComponentsInChildren<VirtualButtonBehaviour>();

		Debug.Log(string.Format("found <0> button",virtualButtons.Length));
		for(int i=0;i<virtualButtons.Length;i++)
		{
			virtualButtons[i].RegisterEventHandler(this);
		}
		Debug.Log("Debut");
	}
	
    void Update () {}
	
	private void switchTheColor() {
        if (switchState) {
            switchState = false;
            myGameObject.GetComponent<Light>().color = Color.blue;
        } else {
            switchState = true;
            myGameObject.GetComponent<Light>().color = Color.red;
        }
    }

	public void OnButtonPressed(VirtualButtonBehaviour virtualButton)
	{
        Debug.Log("Button pressed!");
        switchTheColor();
	}

	public void OnButtonReleased(VirtualButtonBehaviour virtualButton)
	{
	}	
}
