﻿using UnityEngine;
using System.Collections;

namespace Entity
{
	public class ApplicationUser:MonoBehaviour
	{
		#region Enumeration
		
		public enum Gender
		{
			Man,
			Woman
		}
		
		#endregion
		
		#region Fields
		
		[SerializeField]
		private string userName;
		[SerializeField]
		private Gender genderType;
		[SerializeField]
		[Range(7, 77)]
		private int age;
		[SerializeField]
		private bool isSubscriber;
		
		#endregion
		
		#region Properties
		
		public string UserName
		{
			get { return userName; }
			set { userName = value; }
		}
		
		
		public int Age
		{
			get { return age; }
			set { age = value; }
		}
		
		public bool IsSubscriber
		{
			get { return isSubscriber; }           
		}
		
		public Gender GenderType
		{
			get { return genderType; }
			set { genderType = value; }
		}
		
		#endregion
		
		#region Events
		
		public delegate void SubscribtionHandler(object sender);
		public event SubscribtionHandler subscriptionChanged;
		
		#endregion       
		
		#region Methods
		
		private void Awake()
		{
			this.userName = "Luke";
			this.genderType = Gender.Man;
		}
		
		
		public void ManageSubscription(bool state)
		{
			this.isSubscriber = state;
			subscriptionChanged(this);
		}
		
		#endregion
	}
}
