using UnityEngine;
using System.Collections;

public class Entity
{	
	#region Fields
	...		
	#endregion
	
	#region Properties
	...
	#endregion
	
	#region Constructor
	public Entity() {...}
	#endregion       
	
	#region Methods
	...
	#endregion
}
