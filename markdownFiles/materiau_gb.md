# Colours and materials

[PF Villard](http://members.loria.fr/PFVillard/)



Outline
----

- Colours
- Pixel colours computation
  - *Ambient* colour
  - *Diffuse*  colour 
  - *Specular*  colour 
- Transparency

<img data-src="webGL_files/Unity/coul1.jpg" style="height:30%;">



When should the colour be calculated?
-----------------------------------
<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;">



RGB additive colours
--------------------

<div id="container"></div>
<iframe  width="600" height="600" data-src="webGL/demo/coul1.html">
</iframe>



Gamut 
-----

=all the colours that a certain type of material can reproduce

</br></br></br></br></br></br>
            <span class="fragment fade-out" data-fragment-index="1">
            </br><img data-src="webGL_files/images/col7.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
            </span>
            <span class="fragment fade-in" data-fragment-index="1">
            <span class="fragment fade-out" data-fragment-index="2">
            </br><img data-src="webGL_files/images/col8.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
            </span></span>
            <span class="fragment fade-in" data-fragment-index="2">
            </br><img data-src="webGL_files/images/col9.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
            </span>



Choice of a colour
--------------------

- Create a new material
- Choose the colour via the **albedo**
=proportion of incident light reflected from the surface

<img data-src="webGL_files/Unity/coul2.png" style="width:30%;">



How to calculate the colour of each pixel of the object?
---------------------------------------------------------
<img data-src="webGL_files/Unity/coul3.png" style="width:40%;">



Simple lighting model
--------------------------

<p><font color="red">emissive</font> + <font color="green">ambient</font> + <font color="blue">diffuse</font> + <font color="orange">specular</font></p>
</br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/images/col10_0.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/images/col10_1.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/images/col10_2.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="3">
</br><img data-src="webGL_files/images/col10_3.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span> 



Lighting calculation
----------------------

<pre style="font-size:22pt;">
Couleur en surface =
<font color="red">couleur Emissive</font> +
<font color="green">couleur Ambiante</font> +
Pour chaque source de lumière :
<font color="blue">couleur diffuse</font>(source) +
<font color="orange">couleur spéculaire</font>(source, point de vue)
</pre>

C= <font color="red">E</font> + <font color="green">A</font> + &sum; (<font color="blue">D</font>(L) + <font color="orange">S</font>(L,V))



Diffuse light on a plane
---------------------------
<br><br><br><br><br><br><br><br><br><br>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/images/spot1.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/images/spot2.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/images/spot3.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="4">
</br><img data-src="webGL_files/images/spot4.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="4">
</br><img data-src="webGL_files/images/spot5.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> 



Diffuse light on a plane
---------------------------

<img data-src="webGL_files/images/col15.png" style="background:none; border:none; box-shadow:none;max-width:150%;max-height:150%;">



Diffuse light on a sphere
------------------------------
</br><img data-src="webGL_files/images/col11.png" style="background:none; border:none; box-shadow:none;width:40%;">



Diffuse light on a sphere
------------------------------

</br></br></br></br></br></br>
<img data-src="webGL_files/images/col12.png" style="background:none; border:none; box-shadow:none;max-width:120%;max-height:70%;position:absolute;top:160pt;left:200pt;">




Need for normalization of normals
-----------------------------------
</br><img data-src="webGL_files/images/col15.png" style="background:none; border:none; box-shadow:none;width:60%;">



The dot product
-------------------

<img data-src="webGL_files/images/col17.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;">



Diffuse materials
----------------

-   C= ambient + colour\* ∑ (N⃗.L⃗i)
- It's called the **Lambertien model**
- Included in **Unity**

<img data-src="webGL_files/Unity/coul4.png" style="width:40%;">



Use of normals for shadow calculation
---------------------------------------

How to switch from known normals to geometric normals?

<img data-src="webGL_files/images/col18.png" style="background:none; border:none; box-shadow:none;height:75%;">



Use of smooth normal
----------------------------

-   By default (**Smooth shading**)
<span class="fragment fade-in" data-fragment-index="1">
-   Otherwise, write your own **shader**
</span>

<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/coul5.png" style="max-height:100%;">
</span>



Demo
----
- couleur = <b>K<sub>A</sub></b>\*material+<b>K<sub>D</sub></b>\*material\*(N&#8407;.L&#8407;)

<iframe  class="stretch" data-src="webGL/demo/coul2.html" data-preload></iframe>



Specular materials
---------------------

-  Non-specular material:

<img data-src="webGL_files/images/col19.png" style="background:none; border:none; box-shadow:none;width:60%;">



Specular materials
---------------------

-   Phong model:

<font color="orange">specular colour</font> = max(N&#8407;.H&#8407;, 0)<sup>n</sup>

<img data-src="webGL_files/images/col20.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Specular materials
---------------------

<font color="orange">specular colour</font> = max(N&#8407;.H&#8407;, 0)<sup>n</sup>

<img data-src="webGL_files/images/col23.png" style="background:none; border:none; box-shadow:none;width:60%;">



Lighting effect
------------------

<img data-src="webGL_files/images/cosinusn2.png">



Generalisation
--------------

</br></br></br></br></br></br></br></br></br></br>
<div class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/col32a.png" style="background:none; border:none; box-shadow:none;max-width:60%;max-height:90%;position:absolute;top:120pt;left:100pt;">
</div>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/col32b.png" style="background:none; border:none; box-shadow:none;max-width:60%;max-height:90%;position:absolute;top:120pt;left:100pt;">
</span> 



Demo
----
<iframe  class="stretch" data-src="webGL/demo/coul3.html" data-preload></iframe>



Physically Based Rendering (PBR)
--------------------------------

- Takes into account physical properties   
e.g. energy conservation or light scattering
- By default, **Metallic workflow** with the properties :
  - **Metallic**, usually 0 or 1
  - **Smoothness** how light is reflected

<img data-src="webGL_files/Unity/coul6.png" style="width:40%;">



Baking
------

<img data-src="webGL_files/images/col21.png" style="background:none; border:none; box-shadow:none;max-width:160%;"><br>
<img data-src="webGL_files/images/col22.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



When is it possible to save the pre-calculated shading colours?
---------------------------------------------------------------------------

<ul class="noBullet">
  <li><input type="checkbox" id="option9"/>
  <label for="option9"> <span></span> If the orientation of the object does not change </label></li>
  <li><input type="checkbox" id="option10"/>
  <label for="option10"> <span></span> If the camera position and light direction do not change </label></li>
  <li><input type="checkbox" id="option11"/>
  <label for="option11"> <span></span> If the orientation of the object and the camera position do not change </label></li>
  <li><input type="checkbox" id="option12"/>
  <label for="option12"> <span></span> If the light direction and object orientation do not change </label></li>
</ul>



Gouraud shading
------------------
<img data-src="webGL_files/images/col31.png">



Mach bands
-------------

- The human visual system increases the contrast on the contours

<img style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;" data-src="https://upload.wikimedia.org/wikipedia/commons/9/97/Bandes_de_mach.PNG">
<img  style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;" data-src="webGL_files/images/col33.png">



Phong shading
----------------

<img data-src="webGL_files/images/col24.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Phong shading
----------------

<img data-src="webGL_files/images/col34.png" style="background:none; border:none; box-shadow:none;width:60%;">



Calculation example
-----------------

Each vertex is shared by **5 triangles**

Each triangle covers an average of **60 pixels**

→ Phong shading is <input type="text" style="font-size: 25pt" size="8" value=" X times "> times slower than Gouraud shading
(*help:* example for 100 triangles)

<img data-src="webGL_files/images/col25.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Transparency - Introduction
---------------------------

<img data-src="webGL_files/images/col26.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Transparency - caustic
------------------------

<img data-src="webGL_files/images/col27_1.jpg" style="background:none; border:none; box-shadow:none;max-width:50%;">
<img data-src="webGL_files/images/col27_3.jpg" style="background:none; border:none; box-shadow:none;max-width:50%;">



Screen-Door Transparency
-----------------------------

<img data-src="webGL_files/images/col35.png" style="background:none; border:none; box-shadow:none;width:30%;"><br>
<img data-src="http://blog.bonzaisoftware.com/wp-content/uploads/2013/08/oit2.png" style="background:none; border:none; box-shadow:none;width:40%;">



Demo
----

<iframe  class="stretch" data-src="webGL/demo/coul4.html" data-preload></iframe>



Alpha blending
--------------

<img data-src="webGL_files/images/col28.png" style="background:none; border:none; box-shadow:none;max-width:50%;">



Alpha blending
--------------

-   Operation : 
<pre style="font-size:22pt;">
C = <font color="red">&alpha;<sub>s</sub>C<sub>s</sub></font>+(1-<font color="red">&alpha;<sub>s</sub></font>)<font color="blue">C<sub>d</sub></font>
</pre>
-   Examples :
<pre style="font-size:22pt;">       
C =    <font color="blue">C<sub>d</sub></font> si <font color="red">&alpha;<sub>s</sub></font> =0
C = <font color="red">C<sub>s</sub></font>    si <font color="red">&alpha;<sub>s</sub></font> =1
</pre>
-   → **Linear Interpolation**



Demo
----

<iframe  class="stretch" data-src="webGL/demo/coul5.html" data-preload></iframe>



Demo
----

<iframe  class="stretch" data-src="webGL/demo/coul6.html" data-preload></iframe>



Transparency in Unity
-----------------------

-   Linked to **Albedo**
-   It contains the value of the **alpha** channel 

<img data-src="webGL_files/Unity/coul7.png" style="width:40%;">



Follow up :
-------

[Textures](#textures)
