My story
---------

- **2002-2006**: Thèse à l'Université de Lyon 
- **2006-2007**: Post-doc au CIMIT à Boston
- **2007-2009**: Post-doc à Imperial College à Londres
- **Depuis 2009**: Maître de conférence à l’UL

- Expertise: **Réalité augmentée et virtuelle** en Médecine

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/simulator.jpg)  | ![](webGL_files/Intro/anim.gif)<!-- .element: style="width: 40%;" -->



Recherches
--------
- Modélisation de la respiration
- Simulation de valves cardiaques
- Déformations et mouvements d'organes

|   |   |
:-------------------------:|:-------------------------:
![](https://members.loria.fr/PFVillard/files/results/model_respi.gif)  | ![](https://team.inria.fr/curative/files/2011/07/sofa.gif)<!-- .element: style="width: 50%;" -->



Exemples en Réalité augmentée
--------  

- Exemple 1 : **Daqri**

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/daqri_glasses.png) | ![](webGL_files/Intro/daqri.gif)<!-- .element: style="width: 8	0%;" -->



Réalité augmentée
--------  

- Exemple 2 : **Edusafe** au CERN

![](webGL_files/Intro/radioactivity.jpg)

&rarr; Niveaux de radioactivité



Réalité augmentée
--------  

- Exemple 3 : **iAR**

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/iAR_ar1.jpg) | ![](webGL_files/Intro/iAR.gif)<!-- .element: style="width: 100%;" -->
