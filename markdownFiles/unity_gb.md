# Introduction to Unity

[PF Villard](http://members.loria.fr/PFVillard/)



## outline 

1. Presentation  of <b>Unity</b> 
2. Installation of <b>Unity</b> 
3. Basic use of <b>Unity</b> 



## Presentation 
-  Tool to design games
	- Download here: <a href="www.unity3d.com">www.unity3d.com</a> 
- Features : 
	- 3D model import 	
	- Terrain modelling 
	- Physic engine 
	- Audio 
	- Network
	- Script programming (C#) 
	- Rapid game prototyping



## Why using Unity 
- Cross-platform game editor 
- Available in pay or free versions 
	- the free version offers fewer features such as advanced rendering effects 
- Allows you to create games 
	- Standalone PC, MAC, Linux  
	- Games in a webGL player  
	- Games for iOS and Android  
	- PS4, Xbox One, PS Vista Games
	- Facebook 
- Based on C# (POO)  



## Differences with Java Games 

- A lot of programming 
- Start from scratch  
- Result not automatically visible 
		
<img data-src="webGL_files/Unity/im5a.png"><!-- .element: style="width: 40%" -->



## Unity Games

- Little programming 
- Components already available  
- Very visual  
- Interactive  

<img data-src="webGL_files/Unity/im5b.png"><!-- .element: style="width: 40%" -->



## Unity Games
<ul>
<li>(Almost) complete list of <a href="https://en.wikipedia.org/wiki/List_of_Unity_games">Games under Unity</a></li>
<li>Some games highlighted on the <a href="https://unity3d.com/fr/games-made-with-unity">Unity web site</a></li>
</ul>



## Selection of games under Unity

<div style="width: 1000px; height: 400px; overflow-y: scroll;">

	<hr>
	<h3 id="cuphead">Cuphead</h3>
	<iframe src="https://player.vimeo.com/video/235625505" width="640" height="360" frameborder="0" allowfullscreen></iframe>
	<p><a href="http://www.cupheadgame.com/">Official website</a></p>
	<hr>

	<h3 id="monumentvalley">Monument Valley</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/wC1jHHF_Wjo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.monumentvalleygame.com/mv1">Official website</a></p>
	<hr>

	<h3 id="pokemongo">Pokemon Go</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/2sj2iQyBTQs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.pokemongo.com/fr-fr/">Official website</a></p>
	<hr>

	<h3 id="7daystodie">7 days to die</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/aZCXqoKSQ5c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://7daystodie.com/">Official website</a></p>
	<hr>

	<h3 id="citiesskylines">Cities: Skylines</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/CpWe03NhXKs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.paradoxplaza.com/cities-skylines/CSCS00GSK-MASTER.html">Official website</a></p>
	<hr>

	<h3 id="kerbalspaceprogram">Kerbal Space Program</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/aAa9Ao26gtM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.kerbalspaceprogram.com/">Official website</a></p>
	<hr>

	<h3 id="overcooked">Overcooked</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/J39h5o-m1EI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="http://www.ghosttowngames.com/overcooked/">Official website</a></p>
	<hr>

	<h3 id="rust">Rust</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/gcvDUxdmntw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://rust.facepunch.com/">Official website</a></p>
	<hr>

	<h3 id="hearthstone">Hearthstone</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/o84Y_cSjVyE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://playhearthstone.com/fr-fr/">Official website</a></p>
</div>



## Install Unity
- Download and install Unity from[website](https://unity3d.com)

![](webGL_files/Unity/install.png)<!-- .element: style="width: 40%" -->
- Select also:
  - webGL &rarr; project export
  - Vuforia &rarr; to make augmented reality



## Starting Unity 

<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  <img class="fragment" src="webGL_files/Unity/im6a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6c.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6d.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6e.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6f.png" style="position:absolute;top:0;left:0;" />	
  <img class="fragment" src="webGL_files/Unity/im6g.png" style="position:absolute;top:0;left:0;" />
</div>



## Graphical user interface
<img data-src="webGL_files/Unity/im7.png" ><!-- .element: style="width: 80%" -->



## The main interface 
- <spam><b>Scene</b> : The scene you are currently working on </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Game</b> : Overview of your video game in real time </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Hierarchy</b> : The hierarchy of the different components of your game </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Project</b> : The different assets loaded in the project </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Inspector</b> : Area that allows to have details on each component of the game </spam><!-- .element: style="font-size: x-large" -->

<img data-src="webGL_files/Unity/im7.png" ><!-- .element: style="width: 50%" -->



## The top menu 
<small>
- <b>File</b> : Create, open, close scenes and projects, and compile your game  
- <b>Edit</b> : Copy/Paste/Cut and different selection and zoom options  
- <b>Assets</b> : Create or import assets  
- <b>GameObject</b> : Create the objects/elements of your game (3D, 2D, lights, sound, interface, particle, camera,...)    
- <b>Components</b> : Add components or properties to your objects/elements  
- <b>Window</b> : Open/Close the different parts of the Unity interface  
- <b>Help</b> : Unity Help, About and Updates   
</small>

<img data-src="webGL_files/Unity/im7.png"><!-- .element: style="width: 40%" -->



## Importing an existing asset 

1. <spam>Open the asset store: <b>Window</b> &rarr; <b>Asset Store</b> </spam> <!-- .element: class="fragment" data-fragment-index="1" -->
+ <spam> Search the package <b>Altar Ruins</b> </spam> <!-- .element: class="fragment" data-fragment-index="2" -->
+ <spam> Click on <b>Import</b> then <b>Download </b> asset </spam> <!-- .element: class="fragment" data-fragment-index="3" -->
+ <spam>Observe <b>structure</b> and <b>data</b> in the part <b>Project</b> </spam> <!-- .element: class="fragment" data-fragment-index="4" -->

<p style="text-align: center;">
<div style="position:relative;  height:300px; margin:0 auto;">
	<div class="fragment fade-out" data-fragment-index="2">
  <img class="fragment" src="webGL_files/Unity/jeu1.png" style="position:absolute;top:0;left:0;" data-fragment-index="1"/>
</div>
<div class="fragment fade-out" data-fragment-index="3">
  <img class="fragment" src="webGL_files/Unity/jeu2.png" style="position:absolute;top:0;left:0;" data-fragment-index="2"/>
</div>
  <img class="fragment" src="webGL_files/Unity/jeu3.png" style="position:absolute;top:0;left:0;" data-fragment-index="3"/>
  <img class="fragment" src="webGL_files/Unity/jeu4.png" style="position:absolute;top:0;left:0;" data-fragment-index="4"/>
</div>
</p>



## Modification of the layout 

- <!-- .element: class="fragment" data-fragment-index="1" --> Load the scene contained in the **scene** folder 
- <!-- .element: class="fragment" data-fragment-index="2" --> In the part **Hierarchy** select **Tree** 
- <!-- .element: class="fragment" data-fragment-index="3" --> Moving in the scene (**arrows** + **wheel**)  
- <!-- .element: class="fragment" data-fragment-index="4" --> In the **Inspector** modify position, rotation and scale 
<img data-src="webGL_files/Unity/jeu5.png" style="background:none; border:none; box-shadow:none;max-height:70%;position:absolute;top:250pt;left:140pt;"><!-- .element: class="fragment" data-fragment-index="1" -->



## Upper left part 

- Change the display of the scene to <b>Shaded wireframe</b> 
- Try all display modes 
- Try the button to activate the 2D mode 
- Try the buttons to activate/deactivate the lights
- Try to activate/deactivate effects (fog, sky,...)  

<p align="center"><img data-src="webGL_files/Unity/jeu7.png" style="background:none; border:none; box-shadow:none;width:30%;"></p>



## Other tools

- The drop-down menu <b>Gizmos</b> allows you to choose which components will be displayed or not in the scene 
- On the far right, above <b>Inspector</b> from the drop-down menus presented below: 
<p align="center"><img data-src="webGL_files/Unity/jeu8.png" style="background:none; border:none; box-shadow:none;max-height:50%;"></p>

- <b>Layers</b> : allows you to select the graphic layers to be displayed 
- <b>Layout</b> : allows you to view the scene differently 



## Import the package Standard Assets

![Standard Assets](webGL_files/Unity/standardAssets.png)<!-- .element: style="width: 85%" -->



## Import a First Person Character
1. <spam>Import the package **Character** </spam> <!-- .element: class="fragment" data-fragment-index="1" -->
+ <spam>Confirm import </spam> <!-- .element: class="fragment" data-fragment-index="2" -->
+ <spam>Drag and drop **FPSControler** in the scene</spam> <!-- .element: class="fragment" data-fragment-index="3" -->
+ <spam>Delete the **Camera** </spam> <!-- .element: class="fragment" data-fragment-index="4" -->
<p style="text-align: center;">
<div style="position:relative;  height:300px; margin:0 auto;">
<div class="fragment fade-out" data-fragment-index="2">
<img class="fragment" src="webGL_files/Unity/fpc1.png" style="position:absolute;top:0;left:0;" data-fragment-index="1"/>
</div>
<div class="fragment fade-out" data-fragment-index="3">
<img class="fragment" src="webGL_files/Unity/fpc2.png" style="position:absolute;top:0;left:0;" data-fragment-index="2"/>
</div>
<img class="fragment" src="webGL_files/Unity/fpc3.png" style="position:absolute;top:0;left:0;" data-fragment-index="3"/>
<img class="fragment" src="webGL_files/Unity/fpc4.png" style="position:absolute;top:0;left:0;" data-fragment-index="4"/>
</div>
</p>



## Test the game 

- Test the game by clicking on the icon <b>play</b> 
<p align="center"><img data-src="webGL_files/Unity/jeu9.png" style="background:none; border:none; box-shadow:none;max-height:50%;"></p>
<lI>Browse the scene to see the possibilities</lI>
- Test the buttons <b>pause</b> and <b>frame by frame</b> 
- Change the <b>dimensions</b> of the image and the <b>ratio</b> 
- Mute the sound and display statistics 
- Click on <b>Maximize on play</b> the game will be displayed in large 
- To change the keys, go to <b> Preference</b>&rarr;<b> Keys</b>



Follow up :
-------

[Graphics pipeline](#pipeline)