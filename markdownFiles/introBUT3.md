<style scoped>
table {
  font-size: 13px;
}
</style>

# Sensibilisation à la prog multimédia

[PF Villard](http://members.loria.fr/PFVillard/)



Répartition
---------

- Programmation sous Unity
- En salle IN pour utiliser Unity.

|CM|TD|TP|TD saé|TP saé |
|--|--|--|------|-------|
|2 | 4| 8|  2   |   0   |



Progression
--------

1. 2h CM (*Introduction à Unity et aux scripts*)
2. 2h TD (*Introduction à la réalité virtuelle et augmentée*)
3. 2h TD (*Début des travaux sur un projet*)
4. 4h TP sur 2 groupes (*RA ou RV selon groupes*)
5. 2h TD saé (travail dans le contexte de la saé)



Notation
-------

- 1 petit projet pour la matière (RA ou RV)
  
- 1 petit exercice pour la SAé
  - Modélisation d'un terrain
  - Textures variées avec normal map
  - Arbres et herbe
  - Eau
  - FPS
  - Jardin avec fruits



Projets
-------

![](img/proj.png)