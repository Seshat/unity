# ProBuilder

[PF Villard](http://members.loria.fr/PFVillard/)



## Introduction

- Accessible via the Asset Store
- To build basic or advanced **geometries** directly in **Unity**
- Design of complete UV textures
- Possible to export models 

![](webGL_files/proBuilder/pb1.png)<!-- .element: style="width: 35%; border-style: none;" -->



## GUI

- Select **Tools**>**ProBuilder**>**ProBuilder Window**
- Select **New Shape**
	- Select a shape and set it up

![](webGL_files/proBuilder/pb4.png)<!-- .element: style="width: 60%; border-style: none;" -->



- Use the edit bar to change the shape:
	- object, points, edge or surface
	- Displacement (&rarr; **extrusion**), rotation, scale
	- Can be combined with **shift**

![](webGL_files/proBuilder/pb6.png)<!-- .element: style="width: 60%; border-style: none;" -->



## Other tools
The Toolbar is color-coded to help:
- **Orange** for Tool Panels
- **Blue** for Selection Tools
- **Green** for Object Actions
- **Red** for Geometry Actions (Vertex, Edge, Face) 

![](webGL_files/proBuilder/pb3.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Texturing
- Open the material editor
- Sliding/leaving materials
- Apply the material (alt+nb)
- or select faces
- For UV texture editing, open **UV Editor**	
	- Move, Rotate, Scale

![](webGL_files/proBuilder/pb5.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Exporting in other format
- **Tools**->**ProBuilder**->**Export**
- Formats:
	- stl, obj, ply, Unity asset 

![](webGL_files/proBuilder/pb7.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Exercise
- Building a plot of land with a **castle**
	- a hill to access it
	- crenelated walls
	- a door
	- towers
	- a patrol path



## Tips
- Subdivide the surfaces
- Use the **extrusion** tool
- **Merge** the faces
- Select **all the faces concerned** by an extrusion to have a constant height



## Result

<iframe  class="stretch"  data-src="demo/chateau/index.html"/>