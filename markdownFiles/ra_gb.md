# Augmented reality

[PF Villard](http://members.loria.fr/PFVillard/)



## Plan 

1. Introduction 
+ Augmented reality based on **sensor**
+ Augmented reality based on **vision** 
+ Augmented reality with **Unity**



## Introduction
- Augmented reality: complete our perception of the real world by adding fictitious elements that are not naturally perceptible
- Most often: Realistically embed 3D</b>virtual objects in a <b>image sequence</b>
- For purists: interactive, real time
- At the confluence of <b>various domains</b> (computer graphics, physics, computer vision, optics, robotics, HMI, etc.)
- In the laboratories for 20 years, growing success with the public



## Difficulties
<div style="position:absolute;  top:100pt;left:100pt;height:70%; margin:0 auto;">
<img data-src="webGL_files/RA/ra1.png" >
</div>
<br><br><br><br>

- Perspective <!-- .element: class="fragment" data-fragment-index="1" -->
- Shape <!-- .element: class="fragment" data-fragment-index="2" -->
- Light <!-- .element: class="fragment" data-fragment-index="3" -->
<br><br><br><br><br><br><br><br>



## Example
<div style="  width:640px; height:480px;   margin:0 auto;">
  <img  src="webGL_files/RA/ra2a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra2b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra2c.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra2d.png" style="position:absolute;top:0;left:0;" />      
</div>



## Perspective projection
<div style=" position:absolute;  width:640px; height:480px;   margin:0 auto;">
  <img  src="webGL_files/RA/ra3a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra3b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra3c.png" style="position:absolute;top:0;left:0;" />   
  <img class="fragment" src="webGL_files/RA/ra3d.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra3e.png" style="position:absolute;top:0;left:0;" /> 
  <img class="fragment" src="webGL_files/RA/ra3f.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra3g.png" style="position:absolute;top:0;left:0;" />    
</div><br><br><br><br><br>

- Pinhole camera model  :
	- plan <font  color="green">P</font> (grid of the drawing)
	- 3D point <font  color="blue">M</font> of the scene projected on <font  color="black">m</font> belonging to <font  color="green">P</font>
	-  projection centre <font  color="red">C</font> (pinhole)



## Perspective inversion 
- Find the point of view of the eyes in real time
- Different methods
  - <b>Sensors</b> : robust, not very accurate
  - <b>Image analysis</b> : accurate, not very robust
  - <b>Hybrid solutions</b> : accurate and robust



## Sensor-based techniques



## Magnetic sensors 
3 orthogonal coils emit a magnetic field picked up by 3 receptive coils
<div>
- **+** Good accuracy (0.2 mm, 0.1 deg)
- **-** Sensitive to magnetic field disturbances
- **-** Small environment (3m × 3m × 3m × 3m × 3m)
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="webGL_files/RA/ra4.png" >



## Acoustic sensors
3 microphones / speakers, spheres intersection
<div>
- **+** Good accuracy (≅ magnetic)
- **+** Not sensitive to metal objects
- **-** Small environment (3m × 3m × 3m × 3m × 3m)
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="webGL_files/RA/ra5.png" >



## Optical sensors 
Stereo cameras + targets + triangulation
<div>
- **+** Very accurate (0.1 mm)
- **-** Limited volume
- **-** Risk of target occultation
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="webGL_files/RA/ra6.png" >



## Inertial sensors 
Fusion of data from "autonomous" sensors (gyroscopes, accelerometers, magnetometers)
<div>
- **+** Unrestricted volume (approx. outside ok)
- **-** Rotations only
- **-** Inaccurate
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="webGL_files/RA/ra7.png" >



## GPS   
Satellite navigation system (disk intersection)
<div>
- **+** Outdoor environment
- **-** Positions only **-** Positions only
- **-** Very inaccurate (10-20 m)
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="webGL_files/RA/ra8.gif" >



## Techniques based on algorithms

Several image-based techniques exist:
- Use the **Vanishing points** (Sketchup...)
- Use **3D-2D matches** of points (3DS Max...)
- **Manually**, by trial and error (all modeling software that accepts a background image)

Only the 2nd can be applied automatically in video sequences! <!-- .element: class="fragment" data-fragment-index="1" -->



## Vanishing points method
- Find the projection matrix by leak point

|             |   |   |
:-------------------------:|:-------------------------:
![](webGL_files/RA/ra9a.png)  |  ![](webGL_files/RA/ra9b.png)  |  ![](webGL_files/RA/ra9c.png)
One-point perspective projection | 	Frankfurt International Airport | 	Pietro Perugino in Sistine Chapel (1481–82)



## Example in Sketchup
![](webGL_files/RA/ra10.png)



Use two vanishing points corresponding to horizontal lines of orthogonal directions
<div style=" position:absolute;  width:640px; height:480px;   margin:0 auto;">
<!-- for file in *.png; do convert ${file} -transparent white ${file}; done -->
<!-- convert ptFuite*.png -crop 500x400+250+280 fuite%03d.png -->
	<span class="fragment fade-out" data-fragment-index="2">
	  <img src="webGL_files/RA/ptFuite/fuite000.png" style="position:absolute;top:0;left:0;" />	
	</span>
	<span class="fragment fade-out" data-fragment-index="3">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite001.png" style="position:absolute;top:0;left:0;" data-fragment-index="1" />
	</span>
	<span class="fragment fade-out" data-fragment-index="4">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite002.png" style="position:absolute;top:0;left:0;" data-fragment-index="2" />
	</span>
	<span class="fragment fade-out" data-fragment-index="5">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite003.png" style="position:absolute;top:0;left:0;" data-fragment-index="3" />
	</span>
	<span class="fragment fade-out" data-fragment-index="6">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite004.png" style="position:absolute;top:0;left:0;" data-fragment-index="4" />
	</span>
	<span class="fragment fade-out" data-fragment-index="7">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite005.png" style="position:absolute;top:0;left:0;" data-fragment-index="5" />
	</span>
	<span class="fragment fade-out" data-fragment-index="8">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite006.png" style="position:absolute;top:0;left:0;" data-fragment-index="6" />
	</span>
	<span class="fragment fade-out" data-fragment-index="9">
	  <img class="fragment" src="webGL_files/RA/ptFuite/fuite007.png" style="position:absolute;top:0;left:0;" data-fragment-index="7" />
	</span>
	<img class="fragment" src="webGL_files/RA/ptFuite/fuite008.png" style="position:absolute;top:0;left:0;" data-fragment-index="8" />
</div>



## Example in Sketchup
<div style="  width:1200px; height:900px;   margin:0 auto;">
  <img src="webGL_files/RA/sketchup/sketchup.001.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.003.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.004.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.005.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.006.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.007.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.008.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.009.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.010.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.011.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.012.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
</div>



## The method of invading perspectives
Find the projection matrix by knowledge of the points in the real world

![](webGL_files/RA/ra11.png)



## System of equations

Camera inversion = calculation of R, t :
- known focal length: 4 points are enough
- focal length not known: 6 points

Use of 3D-2D correspondences 

<div style="  position:absolute;  width:640px; height:480px;    margin:0 auto;">
<!-- for file in *.png; do convert ${file} -transparent white ${file}; done -->
<!-- convert inv*.png -crop 525x300+255+170 inverse%03d.png -->
  <img src="webGL_files/RA/inv/inverse000.png" style="position:absolute;top:0;left:0;" />
  <span class="fragment fade-out"  data-fragment-index="2">
  	<img class="fragment" src="webGL_files/RA/inv/inverse001.png" style="position:absolute;top:0;left:0;" data-fragment-index="1"/>
  </span>
  <span class="fragment fade-out" data-fragment-index="3">
  	<img class="fragment" src="webGL_files/RA/inv/inverse002.png" style="position:absolute;top:0;left:0;" data-fragment-index="2" />  
  </span>
  <span class="fragment fade-out"  data-fragment-index="4">
  	<img class="fragment" src="webGL_files/RA/inv/inverse003.png" style="position:absolute;top:0;left:0;" data-fragment-index="3" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="5">	
  	<img class="fragment" src="webGL_files/RA/inv/inverse004.png" style="position:absolute;top:0;left:0;" data-fragment-index="4" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="6">
  	<img class="fragment" src="webGL_files/RA/inv/inverse005.png" style="position:absolute;top:0;left:0;" data-fragment-index="5" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="7">
  	<img class="fragment" src="webGL_files/RA/inv/inverse006.png" style="position:absolute;top:0;left:0;" data-fragment-index="6" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="8">
  	<img class="fragment" src="webGL_files/RA/inv/inverse007.png" style="position:absolute;top:0;left:0;" data-fragment-index="7" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="9">
  	<img class="fragment" src="webGL_files/RA/inv/inverse008.png" style="position:absolute;top:0;left:0;" data-fragment-index="8" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="10">
  	<img class="fragment" src="webGL_files/RA/inv/inverse009.png" style="position:absolute;top:0;left:0;" data-fragment-index="9" />
  </span>
  <img class="fragment" src="webGL_files/RA/inv/inverse010.png" style="position:absolute;top:0;left:0;" data-fragment-index="10" />
</div>



## Hand calculation

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/RA/ra12a.png)  | ![](webGL_files/RA/ra12d.png)
![](webGL_files/RA/ra12b.png) | 	![](webGL_files/RA/ra12c.png) 



![](webGL_files/RA/ra13.png)

![](webGL_files/RA/ra14.jpg)<!-- .element: style="width: 60%;" -->



## Ex : student projects
|   |   |
:-------------------------:|:-------------------------:
![](http://projetspolar.iutsd.univ-lorraine.fr/img/animation.gif)<!-- .element: style="width: 77%;" -->  |  ![](http://projetspolar.iutsd.univ-lorraine.fr/img/YiLu.gif)<!-- .element: style="width: 70%;" -->

avec <a href="http://polar.inria.fr"><strong><span style="color: #33cccc">PoLAR</span></strong></a>  <img src="http://project.inria.fr/polar/files/2016/10/dessin6.png" alt="PoLAR" width="60" height="50" style="border:0;">



## Automatic calculation by fiducial markers
- **Fiducial** = detectable and identifiable
- Consists of 4 main steps:
  1. Extraction of **points of interest** in each image
  + Robust **mapping** of points of interest between adjacent images
  + **Projective reconstruction** of the paired points of interest
  * **Metric construction** (autocalibration)
- Example : [ARToolkit](https://hitl.washington.edu/artoolkit/)



## Algorithm in ARToolkit

![](webGL_files/RA/ra15.png)



## Augmented reality with Unity



## Solution in Unity: Vuforia
- Plugin developed by **Qualcomm** for
  - Camera management
  - Environmental detection
  - Displaying projected 3D objects

![](https://www.vuforia.com/content/dam/vuforia/hompage/landing/img/vuforia-rgb.svg)<!-- .element: style="width: 34%;" -->

- Create an account on[Vuforia](https://developer.vuforia.com) and log in
- Create a license in the menu **License Manager**



## Workflow for using Vuforia plugin
1. Create an account on the Vuforia website
+ **Download** and **install** the plugin
+ **Define images** to be interpreted as markers
+ Constitute the **database** for Unity (*Target Manager*)
+ **Import** this database into Unity

Les fichiers images correspondants aux marqueurs utilisés en classe se trouvent [ici](webGL_files/RA/Images_Vuforia.zip)



## Using the Target Manager
<div>
- Go to the menu **Target Manager**
- Add a **Database**
- Select **Device** (the cloud is not free)
- Define a name
- Add a **Target**
- Select a **Single image**
- Fill in the fields
- **Download** the corresponding *Database*
</div> <!-- .element: style="float: left; width: 65%;" -->

![](webGL_files/RA/ra16.png)<!-- .element: style="float: right; width: 34%;" -->



## Quality of the marker
![](webGL_files/RA/ra17.png)<!-- .element: style="width: 80%;" -->



## Advice for a good marker quality
- Have a maximum of **details**
- Have a **high contrast** 
- The image must be **large enough**
- The image must be acquired in **planar position**
- The image must be acquired by **avoiding reflections**
- The marker must be **easy to handle** (e. g. *playing cards*)



## Unity integration
<div>
- If Vuforia is not installed, go to the [plugin site](https://unity3d.com/get-unity/download), select Unity and install Unity with Vuforia
- In an already created Unity project:
  - Check that Vuforia is enabled (*Edit*&rarr;*Project settings*&rarr;*Player*)
  - In the *Project* tab, put the license number in *Resources*&rarr;*VuforiaConfiguration*
  - Import your *Database* (*Asset*&rarr;*Import package*&rarr;*Custom package...*)
</div> <!-- .element: style="float: left; width: 70%;" -->

![](webGL_files/RA/ra18.png)<!-- .element: style="width: 29%;float: right;" -->



## Exercise: Creating a scene with RA
- Add a **ImageTarget** (*GameObject*&rarr;*Vuforia*&rarr;*Image*)
- Replace *camera* with **ARcamera** (*...*&rarr;**ARcamera*)
- Select a marker from the *Inspector*
- Add a **sphere** in *ImageTarget* of the *Hierachy* tab

![](webGL_files/RA/ra19.png)<!-- .element: style="width: 55%;float: left;" class="fragment" -->

<div>
- &rarr; Test the **different images** with **more complicated 3D models**
- &rarr; Test with several markers at the **same time**
</div><!-- .element: style="width: 44%;float: right;" class="fragment" -->



## Interaction between markers
- In Vuforia configurations, allow tracking of **2 targets****
- Example (to be reproduced) with two 3D objects:

![](webGL_files/RA/ra21.jpg)<!-- .element: style="width: 40% -->



## Exercise
1. Create **3 textures**
2. Create a **script** to attach to a 3D object
3. Set **2 material variables** and drag and drop the materials (```public Material mat```)
4. Retrieve the other 3D object with ```GameObject.Find("nom_objet3D");```
5. Retrieve the **positions** of objects with ```objet3D.transform.position;```
6. Make a material change according to the **distance** between objects with ```Vector3.Distance(position1, position2);```



## Texture animation
- Apply a texture to a **Material mat1** belonging to a **GameObject**:
```
this.GetComponent<Renderer>().material = mat1;
```
- Change its offset: 
```
rend.material.SetTextureOffset("_MainTex", new Vector2(timer,1));
```
With **rend** define by:
```
public Renderer rend;
rend = GetComponent<Renderer>();
```
**timer** is a variable to increment for each frame



## Expected outcome
![](webGL_files/RA/ra22.mp4)<!-- .element: style="width: 1% -->