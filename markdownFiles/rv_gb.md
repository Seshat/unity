# Virtual reality

[PF Villard](http://members.loria.fr/PFVillard/)



## Prehistory of VR
<div>
- Simulate the feeling of the presence of a **virtual space**.
- Difficulties :
  - Simulate movement (walking)
  - Graphic Realism
  - Apparatus
  - &rarr; Little chance
- Ex: The Lawnmower Man
</div><!-- .element: style="width: 50%; float: left;" -->

![](webGL_files/RV/rv1.png)<!-- .element: style="width: 30%; border-style: none;" -->

![](http://fr.web.img4.acsta.net/c_215_290/medias/nmedia/18/36/14/90/18455828.jpg)<!-- .element: style="height: 150px; border-style: none;" --> ![](https://supermarcey.files.wordpress.com/2014/05/the-lawnmower-man-fi.jpg)<!-- .element: style="height: 150px; border-style: none;" --> 



## Virtual life

- More and more common, universe with **avatars**
- Users can experience a "second life".
- Real economy:
  - Buying, selling, getting rich
  - Real companies with advertising and services
- Several **activities**:
  - Games
  - Education, culture, culture

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Second_Life_11th_Birthday_Live_Drax_Files_Radio_Hour.jpg/220px-Second_Life_11th_Birthday_Live_Drax_Files_Radio_Hour.jpg)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://upload.wikimedia.org/wikipedia/en/thumb/6/63/WoWScrnShot_011710_164414.jpg/220px-WoWScrnShot_011710_164414.jpg)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://upload.wikimedia.org/wikipedia/en/7/74/Minecraft_city_hall.png)<!-- .element: style="height: 150px; border-style: none;" -->



## 3D Virtual Environment

- Monitor the development of information technology
  - algorithm
  - hardware
- Internet format vrml then X3D
- Linked to **server/client** connections
- Game engine for modelling virtual worlds
   - ex: UNITY3D

![](https://media.giphy.com/media/FxCZAIapF7HeU/giphy.gif)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://thumbs.gfycat.com/SaltyGrayCero-max-1mb.gif)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://static1.squarespace.com/static/58586fa5ebbd1a60e7d76d3e/t/5912d8ae414fb5703828be66/1494407358523/)<!-- .element: style="height: 150px; border-style: none;" -->



![](webGL_files/RV/rv.001.png)<!-- .element: style="width: 90%; border-style: none;" -->



![](webGL_files/RV/rv.002.png)<!-- .element: style="width: 90; border-style: none;"% -->



## 6 basic rules of immersion

- Guide with light
- Take advantage of the background and scale
- Create a sound universe
- Interact with the eye
- Do not neglect the visual
- Build on existing projects



## Guide with light

- The user has complete freedom to look 360°.
- Light is very often used to attract the eye
  - ex: Clickable luminous aura

![](webGL_files/RV/rv.004.png)<!-- .element: style="width: 70%; border-style: none;" -->



## Take advantage of the background and scale

- An overly large environment allows the user to be told that he or she is small
- Increase the scale when using doors or ceilings



## Create a sound universe
- Sound and sound effects help to locate the user in the 3D universe
- Use zones of influence
- Allows to create an atmosphere
  - Ex: fire, water, animals,...

![](webGL_files/RV/rv.006.png)<!-- .element: style="width: 90; border-style: none;"% -->



## Interact with the eye

- Do not use controllers, keyboard, mouse 
- Use of the manhole instead
  - ex: Turn on the light by looking at the switch
  - ex: An object becomes luminous when you look at it

![](webGL_files/RV/rv.007.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Do not neglect the visual

- The application must always be beautiful and fluid
- The user must be interested in discovering and exploring the universe
- Finding a compromise between fluidity and beauty

![](webGL_files/RV/rv.008.png)<!-- .element: style="width: 90; border-style: none;"% -->



## Build on existing projects 

- Many projects exist on Unity, Unreal Engine, Google VR, etc....
- Free and Open Source
- There are many 3D models and free scripts

![](webGL_files/RV/rv.009.png)<!-- .element: style="width: 60%; border-style: none;" -->



## Which interface for RV?

- 3D display 
- Augmented reality

![](https://i.makeagif.com/media/9-27-2015/6DPYpu.gif) | ![](https://media.giphy.com/media/Hx0uWHnh1A6bK/giphy.gif)<!-- .element: style="height: 165px;" -->
--- | ---
MYO | Leap Motion



## Devices

- Goal: to improve the visualization of the scene
- 2 characteristics:
  - Best 3D printing
  - Better impression of immersion
- Can be combined



## Passive glasses
- Can be very simple: red/blue
  - Not very precise
  - Cheap
  - No immersion

![](webGL_files/RV/rv.013.png)<!-- .element: style="height: 150px; border-style: none;" -->  ![](https://orig00.deviantart.net/6592/f/2017/353/0/a/3d_anaglyph_yoda_gif_by_gogu1234-dbx8s1m.gif)<!-- .element: style="height: 150px; border-style: none;" --> 



- More complicated: polarized glasses
  - Accuracy depends on calibration
  - More expensive, with or without sensor
  - No immersion

![](webGL_files/RV/rv.015.png)<!-- .element: style="height: 150px; border-style: none;"% --> ![](webGL_files/RV/rv.016.png)<!-- .element: style="height: 150px; border-style: none;" -->



## Active glasses

- More expensive, with or without sensor

![](webGL_files/RV/rv.017.png)<!-- .element: style="height: 250px; border-style: none;"% -->![](webGL_files/RV/rv.018.png)<!-- .element: style="height: 250px; border-style: none;" -->



## Google Cardboard

- Possible to tinker yourself but purchase of lenses necessary
  - Minimum configuration: 5" screen, full HD, 1.6 GHz 4-cores processor, accelerometer, gyroscope and Android or iOS.
- 2 to 20€

![](webGL_files/RV/rv.019.png)<!-- .element: style="width: 60%; border-style: none;" -->



## Oculus Rift/Go/Quest/Half Dome

![](webGL_files/RV/rv.020.png)<!-- .element: style="width: 20%; border-style: none;" -->

![](https://i.gifer.com/Kf6R.gif)<!-- .element: style="width: 60%; border-style: none;" -->



## HTC Vive

![](webGL_files/RV/rv.022.png)<!-- .element: style="border-style: none;" -->

![](https://thumbs.gfycat.com/GoodnaturedColorfulGreatdane-size_restricted.gif)



## Next advances in VR:
- Best **resolution** of the screen (like smartphone)
- Improved focus: 
  - Utilisation of **eye-tracker**
  - Variable screen **position**e 
<!-- interaction avec la scène
calcul précis avec bonne résolution uniquement où il y a le focus -->

![](webGL_files/RV/rv1.gif)<!-- .element: style="width: 40%; border-style: none;" -->
![](webGL_files/RV/rv2.gif)<!-- .element: style="width: 40%; border-style: none;" -->



## Integration with Unity

![](webGL_files/RV/RV.jpg)<!-- .element: style="width: 40%; border-style: none;" -->

&rarr; See updated instructions for use on [Unity web page](https://developer.oculus.com/documentation/unity/latest/concepts/unity-integration-tutorial-rollaball-intro/)



## Example with CardBoard

![](webGL_files/RV/RV29.jpg)<!-- .element: style="border-style: none;width: 50%;" -->


## 1 - In the Unity scene

|   |   |
|---|---|
|Allow VR in **Edit** -> **Project Settings** -> **Player Settings**| Modify camera settings for [separation fields](https://docs.unity3d.com/ScriptReference/Camera-stereoSeparation.html) and [convergence](https://docs.unity3d.com/ScriptReference/Camera-stereoConvergence.html)   
|![](webGL_files/RV/RV31.png)<!-- .element: style="border-style: none;width: 80%;" --> | ![](webGL_files/RV/RV30.png)<!-- .element: style="border-style: none;width: 80%;" --> |


## 2 - Make a project for Smartphone
- Select **File** -> **Build Settings**
- Select a scene
- Select the OS corresponding to the Smartphone
- Click on **Build**

![](webGL_files/RV/RV32.png)<!-- .element: style="border-style: none;width: 40%;" -->


## 3 - Make the link with the Smartphone

- With **iOS** : [doc](https://unity3d.com/learn/tutorials/topics/mobile-touch/building-your-unity-game-ios-device-testing)
- With **Android** : [doc](https://unity3d.com/learn/tutorials/topics/mobile-touch/building-your-unity-game-android-device-testing?playlist=17138)


## 4 - Compile the project

- Double click on the project to be compiled (extension **.xcworkspace** for iOS)

![](webGL_files/RV/RV33.png)<!-- .element: style="border-style: none;width: 40%;" -->


## For iOS
- Sign the project to compile under iOS

|   |   |   |
|---|---|---|
|Press **Automatically manage signing** |Select the [Team](https://9to5mac.com/2016/03/27/how-to-create-free-apple-developer-account-sideload-apps/)|Compiler|
|![](webGL_files/RV/RV34.png)<!-- .element: style="border-style: none;width: 100%;" --> | ![](webGL_files/RV/RV35.png)<!-- .element: style="border-style: none;width: 100%;" --> | ![](webGL_files/RV/RV36.png)<!-- .element: style="border-style: none;width: 100%;" --> |