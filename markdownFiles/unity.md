# Introduction à Unity

[PF Villard](http://members.loria.fr/PFVillard/)



## Plan 

1. Présentation d'<b>Unity</b> 
2. Installation d'<b>Unity</b> 
3. Utilisation d'<b>Unity</b> 

<img data-src="webGL_files/Unity/im3.png" style="max-height:100%;">




## Présentation 
-  Outil de développement de jeu 
	- Téléchargeable à <a href="www.unity3d.com">www.unity3d.com</a> 
- Caractéristiques : 
	- Importation de modèles 3D 	
	- Modélisation de terrain 
	- Intégration d’un moteur physique 
	- Audio 
	- Réseau 
	- Scriptable (C#) 
	- Prototypage rapide de jeux 



## Pourquoi utiliser Unity 
- Editeur de jeux multiplateforme 
- Existe en version payante et en version gratuite 
	- la version gratuite propose moins de fonctionnalités comme des effets poussés de rendu 
- Permet de créer des jeux 
	- Standalone PC, MAC, Linux  
	- Jeux dans un player webGL  
	- Jeux pour iOS et Androïd  
	- Jeux PS5, Xbox, Switch
	- Facebook 
- Se base sur C# (POO) 
								



## Différences avec Jeux sous Java 

-  Beaucoup de programmation 
-  Commencer à partir de rien  
-  Résultat pas automatiquement visible  
		
<img data-src="webGL_files/Unity/im5a.png"><!-- .element: style="width: 40%" -->



## Jeux sous Unity

-  Peu de programmation 
-  Composants déjà disponibles  
-  Très visuel  
-  Intéractif  

<img data-src="webGL_files/Unity/im5b.png"><!-- .element: style="width: 40%" -->



## Jeux sous Unity (liens)
<ul>
<li>Quelques jeux sur la <a href="https://fr.wikipedia.org/wiki/Unity_(moteur_de_jeu)#Quelques_jeux_utilisant_le_moteur">page Wikipedia de Unity</a> (FR)</li>
<li>Liste complète (ou presque) des <a href="https://en.wikipedia.org/wiki/List_of_Unity_games">jeux sous Unity</a> (EN)</li>
<li>Quelques jeux mis en avant sur le <a href="https://unity3d.com/fr/games-made-with-unity">site de Unity</a> (EN)</li>
</ul>



## Sélection de jeux sous Unity

<div style="width: 1000px; height: 400px; overflow-y: scroll;">



	<h3 id="monumentvalley">Monument Valley</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/wC1jHHF_Wjo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.monumentvalleygame.com/mv1">Site officiel</a></p>
	<hr>

	<h3 id="pokemongo">Pokemon Go</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/2sj2iQyBTQs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.pokemongo.com/fr-fr/">Site officiel</a></p>
	<hr>

	<h3 id="citiesskylines">Cities: Skylines</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/CpWe03NhXKs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.paradoxplaza.com/cities-skylines/CSCS00GSK-MASTER.html">Site officiel</a></p>
	<hr>

	<h3 id="kerbalspaceprogram">Kerbal Space Program</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/aAa9Ao26gtM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://www.kerbalspaceprogram.com/">Site officiel</a></p>
	<hr>

	<h3 id="overcooked">Overcooked</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/J39h5o-m1EI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="http://www.ghosttowngames.com/overcooked/">Site officiel</a></p>
	<hr>

	<h3 id="rust">Rust</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/gcvDUxdmntw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://rust.facepunch.com/">Site officiel</a></p>
	<hr>

	<h3 id="hearthstone">Hearthstone</h3>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/o84Y_cSjVyE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p><a href="https://playhearthstone.com/fr-fr/">Site officiel</a></p>
</div>



## Installer Unity
- Télécharger et installer Unity à partir du [site web](https://unity3d.com)

![](webGL_files/Unity/install.png)<!-- .element: style="width: 70%" -->



## Démarrage de Unity 

<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  <img class="fragment" src="webGL_files/Unity/im6a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6c.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6d.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6e.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/Unity/im6f.png" style="position:absolute;top:0;left:0;" />	
  <img class="fragment" src="webGL_files/altar/altar1.png" style="position:absolute;top:0;left:0;" />
</div>




## Se mettre en config 2 by 3

<img data-src="webGL_files/altar/altar3.png"><!-- .element: style="width: 80%" -->



## Interface 
<img data-src="webGL_files/Unity/im7.png" ><!-- .element: style="width: 80%" -->



	
## L'interface principale 
- <spam><b>Scene</b> : La scène sur laquelle vous travaillez actuellement </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Game</b> : Aperçu de votre jeu vidéo en temps réel  </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Hierarchy</b> : La hiérarchie des différents composants de votre jeu  </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Project</b> : Les différents assets chargés dans le projet  </spam><!-- .element: style="font-size: x-large" -->
- <spam><b>Inspector</b> : Zone qui permet d'avoir les détails sur chaque composant du jeu   </spam><!-- .element: style="font-size: x-large" -->

<img data-src="webGL_files/Unity/im7.png" ><!-- .element: style="width: 50%" -->




## Le menu supérieur 
<small>
- <b>File</b> : Créer, ouvrir, fermer des scènes et des projets, et compiler votre jeu  
- <b>Edit</b> : Copier/Coller/Couper et différentes options de sélection et de zoom  
- <b>Assets</b> : Créer ou importer des assets  
- <b>GameObject</b> : Créer les objets/éléments de votre jeu (3D, 2D, lumières, son, interface, particule, caméra, ...)    
- <b>Components</b> : Ajouter des composants ou des propriétés à vos objets/éléments  
- <b>Window</b> : Ouvrir/Fermer les différentes parties de l'interface de Unity  
- <b>Help</b> : L'aide, l'à propos et les mises à jour de Unity   
</small>

<img data-src="webGL_files/Unity/im7.png"><!-- .element: style="width: 40%" -->



## Import d'un asset existant (import)

### Importer Altar
<img data-src="webGL_files/altar/altar4.png"><!-- .element: style="width: 80%" -->



### Cliquer sur Import

<img data-src="webGL_files/altar/altar5.png"><!-- .element: style="width: 80%" -->



### Patienter

<img data-src="webGL_files/altar/altar6.png"><!-- .element: style="width: 80%" -->



### Double cliquer sur Demo puis Play

<img data-src="webGL_files/altar/altar7.png"><!-- .element: style="width: 80%" -->



## Importer FPS
Permet d'avoir un personnage manipulable
<img data-src="webGL_files/altar/altar8.png"><!-- .element: style="width: 80%" -->



## Sélectionner FPS en bleu

<img data-src="webGL_files/altar/altar9.png"><!-- .element: style="width: 80%" -->



## Glisser/lâcher dans la scène

<img data-src="webGL_files/altar/altar10.png"><!-- .element: style="width: 80%" -->



## Position de la capsule sur le terrain

<img data-src="webGL_files/altar/altar11.png"><!-- .element: style="width: 80%" -->



## Supprimer la caméra par défaut

<img data-src="webGL_files/altar/altar12.png"><!-- .element: style="width: 80%" -->



## Observer structure et données dans la partie Project

<img data-src="webGL_files/Unity/jeu4.png"><!-- .element: style="width: 80%" -->



## Modification de l'agencement 

- <!-- .element: class="fragment" data-fragment-index="1" --> Charger la scène contenue dans le dossier **scene** 
- <!-- .element: class="fragment" data-fragment-index="2" --> Dans la partie **Hierarchy** sélectionner  **Tree** 
- <!-- .element: class="fragment" data-fragment-index="3" --> Se déplacer dans la scène (**flèches** + **molette**)  
- <!-- .element: class="fragment" data-fragment-index="4" --> Dans l'**Inspector** modifier position, rotation et échelle 

<img data-src="webGL_files/Unity/jeu5.png" style="background:none; border:none; box-shadow:none;max-height:70%;position:absolute;top:250pt;left:140pt;"><!-- .element: class="fragment" data-fragment-index="1" -->




## Partie supérieure gauche 

- Changer l'affichage de la scène en <b>Shaded wireframe</b> 
- Essayer tous les modes d'affichage 
- Essayer le bouton pour activer le mode 2D 
- Essayer les boutons pour activer/désactiver les lumières
- Essayer d'activer/désactiver des effets ( brouillard, ciel,...)    

<p align="center"><img data-src="webGL_files/Unity/jeu7.png" style="background:none; border:none; box-shadow:none;width:30%;"></p>



## suite 

- Le menu déroulant <b>Gizmos</b> permet de choisir les composants qui seront affichés ou non dans la scène 
- Tout à droite, au-dessus de <b>Inspector</b> des menus déroulants présentés ci-dessous : 
<p align="center"><img data-src="webGL_files/Unity/jeu8.png" style="background:none; border:none; box-shadow:none;max-height:50%;"></p>

-  <b>Layers</b> : permet de sélectionner les calques graphiques à afficher 
-  <b>Layout</b> : permet de visualiser différemment la scène 



## Tester le jeu 

- Tester le jeu en cliquant sur l’icône <b>play</b> 
<p align="center"><img data-src="webGL_files/Unity/jeu9.png" style="background:none; border:none; box-shadow:none;max-height:50%;"></p>
<lI>Parcourir la scène pour se rendre compte des possibilités</lI>
- Tester les boutons <b>pause</b> et <b>frame by frame</b> 
- Changer les <b>dimensions</b> de l'image et le <b>ratio</b> 
- Couper le son et afficher des stats 
- Cliquer sur <b>Maximize on play</b> le jeu s'affichera en grand 
- Pour changer les touches, aller dans <b> Preference</b>&rarr;<b> Keys</b>
