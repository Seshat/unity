# Ajout de matériaux



### Création d'un dossier matériau

<img data-src="webGL_files/mat/mat1.png">



## Matériaux avec couleurs



### Création d'un nouveau matériau 

<img data-src="webGL_files/mat/mat2.png">



### Changer la couleur dans Albedo

<img data-src="webGL_files/mat/mat9.png">



### Matériau sur objet

1. Créer un cube
+ Glisser/Lâcher le matériau sur le cube
   
<img data-src="webGL_files/mat/mat10.png"><!-- .element: style="width: 60%" -->



## Matériaux avec texture



### Choix d'une image de texture

<img data-src="webGL_files/mat/mat3.png">



### Glisser/Lâcher l'image dans le dossier créé

<img data-src="webGL_files/mat/mat4.png">



### Dupliquer l'image

<img data-src="webGL_files/mat/mat5.png">



### Passage en normal map sur une image

1. ```Texture Type``` &rarr; ```Normal map```
+ ```Create from Grayscale``` &rarr; **checked**
+ Cliquer sur **Apply**

<img data-src="webGL_files/mat/mat6.png"><!-- .element: style="width: 30%" -->



### Textures sur le matériau

1. Dans l'inspecteur du materiau 
+ Glisser/Lâcher la première image dans **albedo** 
+ Glisser/Lâcher la seconde image dans **normal map** 
  
<img data-src="webGL_files/mat/mat7.png"><!-- .element: style="width: 60%" -->



### Matériau sur objet

1. Créer un cube
+ Glisser/Lâcher le matériau sur le cube
   
<img data-src="webGL_files/mat/mat8.png"><!-- .element: style="width: 60%" -->