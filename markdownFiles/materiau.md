# Couleur et matériau

[PF Villard](http://members.loria.fr/PFVillard/)



Plan
----

- Couleur
- Calcul couleur d'un pixel
  -   couleur *ambiante*
  -   couleur *diffuse*
  -   couleur *spéculaire*
- Transparence

<img data-src="webGL_files/Unity/coul1.jpg" style="height:30%;">



Quand faut-il calculer la couleur ?
-----------------------------------
<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;">



Utilisation de couleurs additives RGB
-------------------------------------

<iframe  class="stretch" data-src="webGL/demo/coul1.html" data-preload></iframe>



Gamut de couleur
----------------

=ensemble des couleurs qu'un certain type de matériel permet de
reproduire

</br></br></br></br></br></br>
            <span class="fragment fade-out" data-fragment-index="1">
            </br><img data-src="webGL_files/images/col7.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
            </span>
            <span class="fragment fade-in" data-fragment-index="1">
            <span class="fragment fade-out" data-fragment-index="2">
            </br><img data-src="webGL_files/images/col8.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
            </span></span>
            <span class="fragment fade-in" data-fragment-index="2">
            </br><img data-src="webGL_files/images/col9.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
            </span>



Choix d'une couleur
--------------------

-   Créer un nouveau matériau
-   Choisir la couleur via l'**albedo**
= proportion de lumière incidente réfléchie par la surface

<img data-src="webGL_files/Unity/coul2.png" style="width:30%;">



Comment calculer la couleur de chaque pixel de l'objet ?
---------------------------------------------------------
<img data-src="webGL_files/Unity/coul3.png" style="width:40%;">



Modèle d'éclairage simple
--------------------------

<p>Lumière <font color="red">émissive</font> + <font color="green">ambiante</font> + <font color="blue">diffuse</font> + <font color="orange">spéculaire</font></p>
</br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/images/col10_0.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/images/col10_1.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/images/col10_2.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="3">
</br><img data-src="webGL_files/images/col10_3.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;position:absolute;top:160pt;left:200pt;">
</span> 



Calcul de l'éclairage
----------------------

<pre style="font-size:22pt;">
Couleur en surface =
<font color="red">couleur Emissive</font> +
<font color="green">couleur Ambiante</font> +
Pour chaque source de lumière :
<font color="blue">couleur diffuse</font>(source) +
<font color="orange">couleur spéculaire</font>(source, point de vue)
</pre>

C= <font color="red">E</font> + <font color="green">A</font> + &sum; (<font color="blue">D</font>(L) + <font color="orange">S</font>(L,V))



Lumière diffuse sur un plan
---------------------------
<br><br><br><br><br><br><br><br><br><br>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/images/spot1.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/images/spot2.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/images/spot3.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="4">
</br><img data-src="webGL_files/images/spot4.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="4">
</br><img data-src="webGL_files/images/spot5.png" style="background:none; border:none; box-shadow:none;max-width:250%;max-height:250%;position:absolute;top:75pt;left:50pt;">
</span> 



Lumière diffuse sur un plan
---------------------------

<img data-src="webGL_files/images/col15.png" style="background:none; border:none; box-shadow:none;max-width:150%;max-height:150%;">



Lumière diffuse sur une sphère
------------------------------
</br><img data-src="webGL_files/images/col11.png" style="background:none; border:none; box-shadow:none;width:40%;">



Lumière diffuse sur une sphère
------------------------------

</br></br></br></br></br></br>
<img data-src="webGL_files/images/col12.png" style="background:none; border:none; box-shadow:none;max-width:120%;max-height:70%;position:absolute;top:160pt;left:200pt;">




Besoin de normalisation de normales
-----------------------------------
</br><img data-src="webGL_files/images/col15.png" style="background:none; border:none; box-shadow:none;width:60%;">



Le produit scalaire
-------------------

<img data-src="webGL_files/images/col17.png" style="background:none; border:none; box-shadow:none;max-width:75%;max-height:75%;">



Matériaux diffus
----------------

-   C= ambient + couleur\* ∑ (N⃗.L⃗i)
-   Cela s'appelle le **modèle Lambertien**
-   Inclus dans **Unity**

<img data-src="webGL_files/Unity/coul4.png" style="width:40%;">



Utilisation des normales pour le calcul d'ombre
------------------------------------------------

Comment passer des normales connues aux normales géométriques ?

<img data-src="webGL_files/images/col18.png" style="background:none; border:none; box-shadow:none;height:75%;">



Utilisation de normale douce
----------------------------

-   Par défaut (**Smooth shading**)
-   Sinon, écrire son propre **shader**
<span class="fragment fade-in" data-fragment-index="1">
- Sinon, écrire son propre **shader**
</span>

<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/coul5.png" style="max-height:100%;">
</span>



Démo
----
- couleur = <b>K<sub>A</sub></b>\*material+<b>K<sub>D</sub></b>\*material\*(N&#8407;.L&#8407;)

<iframe  class="stretch" data-src="webGL/demo/coul2.html" data-preload></iframe>



Matériaux spéculaires
---------------------

-   Matériau non spéculaire :

<img data-src="webGL_files/images/col19.png" style="background:none; border:none; box-shadow:none;width:60%;">



Matériaux spéculaires
---------------------

-   Modèle de Phong :

<font color="orange">couleur spéculaire</font> = max(N&#8407;.H&#8407;, 0)<sup>n</sup>

<img data-src="webGL_files/images/col20.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Matériaux spéculaires
---------------------

<font color="orange">couleur spéculaire</font> = max(N&#8407;.H&#8407;, 0)<sup>n</sup>

<img data-src="webGL_files/images/col23.png" style="background:none; border:none; box-shadow:none;width:60%;">



Effet d'éclairage
------------------

<img data-src="webGL_files/images/cosinusn2.png">



Généralisation
--------------

</br></br></br></br></br></br></br></br></br></br>
<div class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/col32a.png" style="background:none; border:none; box-shadow:none;max-width:60%;max-height:90%;position:absolute;top:120pt;left:100pt;">
</div>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/col32b.png" style="background:none; border:none; box-shadow:none;max-width:60%;max-height:90%;position:absolute;top:120pt;left:100pt;">
</span> 



Démo
----
<iframe  class="stretch" data-src="webGL/demo/coul3.html" data-preload></iframe>



Physically Based Rendering (PBR)
--------------------------------

- Prend en compte les propriétés physiques   
ex : conservation de l'énergie ou diffusion de la lumière
- Par défaut, **Metallic workflow** avec les propriétés :
  -   **Metallic**, en général 0 ou 1
  -   **Smoothness** comment la lumière est réfléchie

<img data-src="webGL_files/Unity/coul6.png" style="width:40%;">



baking
------

<img data-src="webGL_files/images/col21.png" style="background:none; border:none; box-shadow:none;max-width:160%;"><br>
<img data-src="webGL_files/images/col22.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Quand est-il possible d'enregistrer les couleurs d'ombrage précalculées ?
---------------------------------------------------------------------------

<ul class="noBullet">
  <li><input type="checkbox" id="option9"/>
  <label for="option9"> <span></span> Si l'orientation de l'objet de change pas </label></li>
  <li><input type="checkbox" id="option10"/>
  <label for="option10"> <span></span> Si la position de la caméra et la direction de la lumière ne changent pas </label></li>
  <li><input type="checkbox" id="option11"/>
  <label for="option11"> <span></span> Si l'orientation de l'objet et la position de la caméra ne changent pas </label></li>
  <li><input type="checkbox" id="option12"/>
  <label for="option12"> <span></span> Si la direction de la lumière et l'orientation de l'objet ne changent pas </label></li>
</ul>



Ombrage de Gouraud
------------------
<img data-src="webGL_files/images/col31.png">



Bande de Mach
-------------

- Le système visuel humain augmente le contraste sur les contours

<img style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;" data-src="https://upload.wikimedia.org/wikipedia/commons/9/97/Bandes_de_mach.PNG">
<img  style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;" data-src="webGL_files/images/col33.png">



Ombrage de Phong
----------------

<img data-src="webGL_files/images/col24.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Ombrage de Phong
----------------

<img data-src="webGL_files/images/col34.png" style="background:none; border:none; box-shadow:none;width:60%;">



Exemple de calcul
-----------------

Chaque sommet est partagé par **5 triangles**

Chaque triangle couvre en moyenne **60 pixels**

→ L'ombrage de Phong est <input type="text" style="font-size: 25pt" size="8" value="   X fois"> fois plus lent que l'ombrage de Gouraud
(*aide :* exemple pour 100 triangles)

<img data-src="webGL_files/images/col25.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Transparence - Introduction
---------------------------

<img data-src="webGL_files/images/col26.png" style="background:none; border:none; box-shadow:none;max-width:160%;">



Transparence - caustique
------------------------

<img data-src="webGL_files/images/col27_1.jpg" style="background:none; border:none; box-shadow:none;max-width:50%;">
<img data-src="webGL_files/images/col27_3.jpg" style="background:none; border:none; box-shadow:none;max-width:50%;">



Transparence de type grillage
-----------------------------

*Screen-Door Transparency*

<img data-src="webGL_files/images/col35.png" style="background:none; border:none; box-shadow:none;width:30%;"><br>
<img data-src="http://blog.bonzaisoftware.com/wp-content/uploads/2013/08/oit2.png" style="background:none; border:none; box-shadow:none;width:40%;">



Démo
----

<iframe  class="stretch" data-src="webGL/demo/coul4.html" data-preload></iframe>



Alpha blending
--------------

<img data-src="webGL_files/images/col28.png" style="background:none; border:none; box-shadow:none;max-width:50%;">



Alpha blending
--------------

-   Opération : 
<pre style="font-size:22pt;">
C = <font color="red">&alpha;<sub>s</sub>C<sub>s</sub></font>+(1-<font color="red">&alpha;<sub>s</sub></font>)<font color="blue">C<sub>d</sub></font>
</pre>
-   Exemples :
<pre style="font-size:22pt;">       
C =    <font color="blue">C<sub>d</sub></font> si <font color="red">&alpha;<sub>s</sub></font> =0
C = <font color="red">C<sub>s</sub></font>    si <font color="red">&alpha;<sub>s</sub></font> =1
</pre>
-   → **Interpolation linéaire**



Démo
----

<iframe  class="stretch" data-src="webGL/demo/coul5.html" data-preload></iframe>



Démo
----

<iframe  class="stretch" data-src="webGL/demo/coul6.html" data-preload></iframe>



Transparence dans Unity
-----------------------

-   Lié à l'**Albedo**
-   Il contient la valeur du canal **alpha**

<img data-src="webGL_files/Unity/coul7.png" style="width:40%;">



Exercice
--------

-   Penser à 8 différents matériaux et les reproduire
-   Faire 8 sphères avec ces différents matériaux



Suite :
-------

[Textures](#textures)
