# Les scripts dans Unity

[PF Villard](http://members.loria.fr/PFVillard/)



## Les scripts dans Unity
- Introduction
- Les scripts liés à **GameObject**
- Le débogage
- Les interfaces utilisateurs



## Introduction



## A quoi servent les scripts dans Unity

* Déjà vu : les éléments d'une scène dans Unity
* -> Propriétés et comportements
* Interaction : à l'aide de scripts



## Ecriture de script
- Change le comportement des objets
- Écrit en <b>C&#35;</b>
- Fonctions importantes :
    - <code>Start</code> : Appelée quand un script est instancié
	- <code>Update</code> : Appelée à chaque frame
	- <code>OnGUI</code> : Utilisée pour afficher une IG (score, santé, ...)
	- <code>OnCollisionEnter</code> : Détection de collision
	- <code>OnTriggerEnter</code> : Collision avec un déclencheur

<img data-src="webGL_files/Unity/phys5.png"><!-- .element: style="width: 30%" -->



## Classes importantes
- Mathématiques
 	- <code>Vector3, Quaternion, Mathf, Ray,</code> ...
- Audio
	- <code>AudioClip, audio,</code> ...
- Physique
	- <code>Rigidbody, Collider, Physics,</code> ...
- GUI
	- <code>Texture2D, GUI,</code> ...
- Autres
	- <code>GameObject, Input, Application,</code> ...

<img data-src="webGL_files/Unity/phys6.png"><!-- .element: style="width: 15%" -->



## Création de script

- Clic droit sur l'onglet **Projet**
- Sélectionner **Create** puis **C#Script**
- Renommer le script avec le nom désiré
- Double-cliquer sur le script pour l'ouvrir
<p align="center">
![](scFig/sc1.png)
</p>



## Un premier script
<pre><code  data-trim data-noescape>using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NewBehaviourScript : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	void Update () {
	}
}
</code></pre>



## Compilation du code

- S'effectue à chaque sauvegarde et retour à Unity : ![](scFig/sc2.png)
- Permet à Unity de valider le script
- Si on rajoute une erreur, elle sera remontée :<!-- .element: class="fragment" data-fragment-index="1" -->

<pre><code >void Start()
	{
		score=500;
	}
</code></pre><!-- .element: class="fragment" data-fragment-index="1" -->

![](scFig/sc3.png)<!-- .element: class="fragment" data-fragment-index="2" -->



## Exécution du code

- Exécution : script doit être assigné à un **GameObject**
- -> Faire un *glissé/lâché* du script vers le **gameObject**
- Peut se voir dans le panneau **Inspector** du **gameObject**
<p align="center">
![](scFig/sc4.png)
</p>



## Organisation

- Utiliser un dossier spécifique pour les scripts
- Éviter arborescence de dossiers longue et complexe
- Nommer les dossiers de façon explicite
- Séparer les scripts liés à l'*UI* et aux fonctionnalités
- Mettre des commentaires



## Les scripts liés à GameObject



## Rôle de MonoBehavior
<pre><code > public class NewBehaviourScript : MonoBehaviour { 	... }
</code></pre>
- Lié à un **GameObject** 
- Permet l’accès à de nombreuses fonctionnalités:
  - **Awake** appelée à la toute première instanciation
  - **Start** appelée à la création après **Awake**
  - **Update** appelée à chaque *frame* (ex : 60 fois/sec)
  - **onDestroy** appelée à la destruction du **GameObject**
- Doc : <a href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.html">ici</a>



<iframe class="stretch" data-src="https://docs.unity3d.com/ScriptReference/MonoBehaviour.html"/>



## Communication entre entités
- Avant de communiquer avec un script, il faut le **localiser**
- Liste des méthodes liées à la recherche d'un **GameObject**:
  - <code>GameObject.Find(string name)</code>
  - <code>GameObject.FindGameObjectWithTag(string tag)</code>
  - <code>GameObject.FindGameObjectsWithTag(string tag)</code>



## Le débogage



## Déboguer le code
- Utiliser le log :

<pre><code>void Start()
{
	Debug.Log("Entrée dans la méthode Start()");
}
</code></pre>

- Utiliser la gestion d'exception :

<pre><code>try{
	int result=100/0; // Provoque une erreur
}
catch()System.Exception e{
	Debug.LogException("Division impossible");
}
</code></pre>



## Déboguer graphiquement
- Dessiner une ligne entre des points :

<pre><code>Debug.DrawLine(pointDeDepart, pointDarrivee, couleur);//Rajouter la 
										 //durée si pas dans Update
</code></pre>

ex :

<pre><code>void Update()
{
	Debug.DrawLine(Vector3.zero,new Vector(10, 0,0), Color.green);
}
</code></pre>



## Les interfaces utilisateurs



## Les outils de création d'UI
- Utilisation de **MonoBehavior**
  - La méthode <code>OnGUI</code> appelée à chaque frame
  - **&rarr;** Vérifier le nombre de Draw Calls
- Deux solutions de positionnement :
  - **GUI** : Résolution fixe

<pre><code>//Exemple :
GUI.Button(new Rect(0,0,80,20),"Cliquer")
</code></pre>
  - **GUILayout** : Résolution variable

<pre><code>//Exemple :
GUILayout.Button("Cliquer sur un bouton pour afficher quelque chose")
</code></pre>



## Éléments d'UI (1)
- **Button** &rarr; mettre dans un <code>if</code>
	- ex :
<pre><code>if (GUI.Button(new Rect(0,0,80,20),"Cliquer"))
	Debug.Log("Clic !");</code></pre>

- **RepeatButton** similaire à **Button**, se répète à chaque frame 
- **Label** affiche un label (pas d'interaction)

<pre><code>GUI.Label(new Rect(0,0,80,20),"Bonjour !")</code></pre>

- **Toggle** affiche une case à cocher
	- ex :

<pre><code>GUI.Label(new Rect(0,0,80,20),"Bonjour !")</code></pre>



## Éléments d'UI (2)
- **Toolbar**, **SelectionGrid** contrôlent la création de barres d'outils
	- &rarr; <a href="https://docs.unity3d.com/ScriptReference/GUI.Toolbar.html"> documentation</a>
- **HorizontalSlider, VerticalSlider** Modificateur de valeurs par sélection
	- &rarr; <a href="https://docs.unity3d.com/ScriptReference/GUI.HorizontalSlider.html"> documentation</a>
- **HorizontalScrollbar, VerticalScrollbar** Similaires aux précédents avec ajout d'une taille de contrôle
	- &rarr; <a href="https://docs.unity3d.com/ScriptReference/GUI.HorizontalScrollbar.html"> documentation</a>



## Exercices
Une série d'exercices est disponible [ici](#exo)
