# Graphics pipeline

[PF Villard](http://members.loria.fr/PFVillard/)



Outline
----

1.  Notions of *fps*
+  Graphics pipeline



Interactivity
-------------

<iframe  width="1000" height="400" data-src="webGL/demo/intro1.html" data-preload></iframe>



Interactivity
-------------

-   **6 fps** usable but slow
-   Between **30 fps** and **60 fps** for video games
-   It depends on the material
-   Computer screens generally **60Hz**
-   **Try it on your computer!**



Motion Blur
-----------

<iframe  width="1000" height="600" data-src="https://www.babylonjs.com/demos/motionblur/" data-preload></iframe>



<iframe  width="1000" height="400" data-src="https://www.clicktorelease.com/tmp/threejs/mblur/" data-preload></iframe>



Graphics pipeline
---------------------

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;">

Sequential operations applied to the data to obtain an image



Graphics pipeline : 1 - Unity
------------------------------

→ Development of an abstract description of the scene

- **Geometry** (shapes, location)
- **Photometry** (colors, materials, textures, opacity, etc.)
- **atmosphere** (lights, sound, fog)
- **behaviour** (animation, interaction)
- **visualization** (type of projection, observation position)

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">



Graphics pipeline : 2 - 2D Projection
-----------------------------------------

→ For each vertex:

- Transforms the position **3D** into position **2D**
- Calculates the **attributes** (ex: **color**)
- Proceed to **clipping**

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">

→ On the **GPU** and programmable via the **Vertex Shader**



<iframe   class="stretch"  data-src="http://www.realtimerendering.com/udacity/transforms.html" data-preload></iframe>



Clipping
--------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/cam16.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/cam16b.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>



Clipping
--------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/cam17_1.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>
<span class="fragment fade-out" data-fragment-index="2">
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/cam17_2.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span></span>
<span class="fragment fade-out" data-fragment-index="3">
<span class="fragment fade-in" data-fragment-index="2">
<img data-src="webGL_files/images/cam17_3.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span></span>
<span class="fragment fade-out" data-fragment-index="4">
<span class="fragment fade-in" data-fragment-index="3">
<img data-src="webGL_files/images/cam17_4.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="4">
<img data-src="webGL_files/images/cam17_5.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>
<div style="text-align: left;">
<p><b  class="fragment fade-in" data-fragment-index="5">vertex shader &rarr;</b>(X,Y,Z,W) &nbsp;&nbsp;<font color="blue"  class="fragment fade-in" data-fragment-index="5">Coordonnées clippées</font></p> 
<div class="fragment fade-out" data-fragment-index="5">
<p style="margin-left: 360px;"> W=0 : vecteur</p>
<p style="margin-left: 360px;"> W=1 : point</p>
</div>



Le vertex shader
----------------

<br><br><br><br><br><br><br><br><br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/shad3_1.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:100pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/shad3_2.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:100pt;">
</span>



graphics Pipeline  : 3 - Rasterisation
--------------------------------------

→ Setting up the triangles:

- Search for the pixels contained in each triangle
- Uses the **Bresenham** algorithm:

<img data-src="webGL_files/Unity/Bresenham.png" style="background:none; border:none; box-shadow:none;width:20%;">

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">
→ On the **GPU**



Graphics Pipeline : 4 - Pixel colours
-------------------------------------------

→ For each triangle:
- Interpret the **attributes** of the vertices over the whole triangle
- Calculates the color of each pixel

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">

→ On the **GPU** and programmable via the **Fragment Shader**



The Fragment shader
------------------

<img data-src="webGL_files/images/shad4.png" style="background:none; border:none; box-shadow:none;max-height:100%;">



Graphics Pipeline : 5 - Z-Buffer
---------------------------------

→ Tests pixel visibility:

- Test the **depth**
- Mix the **colors**

<img data-src="webGL_files/Unity/zbuffer.png" style="background:none; border:none; box-shadow:none;width:30%;">

→ On the **GPU**



Mysterious depth handling
---------------------------------
<iframe  width="1000" height="400" data-src="http://www.realtimerendering.com/erich/udacity/exercises/unit1_painters_zbuffer.html" data-preload></iframe>



Follow up : Virtual Reality
----------------------------------

[Virtual Reality](#vr)
