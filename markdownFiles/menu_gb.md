# Menu
[PF Villard](http://members.loria.fr/PFVillard/)



## The main menu

- Use the signs (**GameObject**&rarr;**UI**&rarr;**Panel**)
- Choose a background image on the internet
 - convert it to Sprite (with **Textute Type**)
- Add a **Vertical Layout** component and adjust the layout
- Add buttons (**GameObject**&rarr;**UI**&rarr;**Button**)

![](webGL_files/menu/menu1.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Link to a static panel

- Add a new panel with:
	- of the text
	- an image
	- a button to return to the menu

![](webGL_files/menu/menu2.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Create a simple scene

- Save the scene and add it to **File**&rarr;**Build Settings**
- Create a new scene
- Create a sphere with a simple texture
- Add a return button to the menu
- Save the scene and add it to **File**&rarr;**Build Settings** 

![](webGL_files/menu/menu3.png)<!-- .element: style="width: 50%; border-style: none;" -->



## To a button panel

- Add a new panel with:
	- several level buttons
	- a button to return to the menu

![](webGL_files/menu/menu4.png)<!-- .element: style="width: 50%; border-style: none;" -->


## Script to switch from one menu to another
- Each menu is a **GameObject**
- They can be enabled or disabled with **SetActive(true/false)**

```
public GameObject menu1;
public GameObject menu2;
public GameObject menu3;
	
public void ShowMenu2()
     {
         menu1.SetActive(false);
         menu2.SetActive(true);
         menu3.SetActive(false);
     }
```



## Script to load a scene
- Use :

```
using UnityEngine.SceneManagement;
```
- Then :

```
SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
```

with the name of the scene instead of **SampleScene**




## Button / action association
- Select a button
- Click on **+** in the section **On Click()**
- Put the **GameObject** which contains the script in **None (Object)**
- Select the right function in **No Function**



## Result

<iframe  class="stretch"  data-src="demo/menu/index.html"/>



