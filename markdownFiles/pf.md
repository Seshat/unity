# Présentation <!-- omit in toc --> 



### Présentation <!-- omit in toc --> 

- Maître de conférences
	- Enseignement à l'IUT de St-Dié
	- Recherche au Loria à Nancy

<table>
	<tr>
<td><div style="height:150px">![alt text](https://factuel.univ-lorraine.fr/sites/factuel.univ-lorraine.fr/files/field/image/2019/06/logo_loria_abrege.jpg)
</div></td>
<td><div style="height:150px">![alt text](img/UL.jpg)
</div></td>
<td><div style="height:150px">![alt text](https://axeptio.imgix.net/2021/02/CNRS_2019.png?auto=format&fit=crop&w=170&h=auto&dpr=1)
</div></td>
<td><div style="height:150px">![alt text](http://www.ens-lyon.fr/LIP/images/LIP/blurb/inria-200x200-blurb.jpg)
</div></td>
</tr>
</table>



## Parcours  <!-- omit in toc --> 

- Maître de conférences à l'UL **depuis 2009**
- Professeur affilié au Harvard Biorobotics Lab **depuis 2014**
- Professeur invité à l'Universite de Bangor **en 2012**
- Post-Doc à Imperial College **2007-2009**
- Post-Doc au CIMIT (MIT/Harvard) **2006-2007**
- Thèse en informatique **2002-2006**
- Ecole d'ingénieur avant

<table>
<tr>
<td><div style="width:100px">![alt text](http://www.ixxi.fr/actualites/bourse-de-these-a-l2019universite-claude-bernard-lyon-ucbl-france-3-ans/image___fr____mini) </div></td>
<td><div style="width:100px">![alt text](https://www.epicicons.com/imager/worklarge/3351/Imperial-College.-SQ-1700-pix_4ae6118128962af1996a94e5370e1193.jpg)</div></td>
<td><div style="width:100px">![alt text](https://s3-eu-west-1.amazonaws.com/global-graduates/institutions/250/d648789f717e609236b6421994e8ca8810c23ed4.png)</div></td>
<td><div style="width:100px">![alt text](https://wgs.fas.harvard.edu/files/wgs/files/harvard_shield_wreath.png)</div></td>
<td><div style="width:100px">![alt text](img/UL.jpg)</div></td>
</tr>
</table>



## Métier = maître de conférences  <!-- omit in toc -->
	
- Effectuer des travaux de **Recherche**
  - **Publier** des articles
  - **Former** aux métiers de la recherche
- Effectuer des activités **administratives**
- Effectuer des **enseignements**
- Effectuer de la **médiation** et de la **vulgarisation**



<iframe  class="stretch"  data-src="https://members.loria.fr/PFVillard/" >
</iframe>



### Recherches :
- Réalité **virtuelle**
- Réalité **augmentée**

&rarr; Dans le domaine **médical**

<!-- <table>
<tr>
<td><div style="width:100px">![alt text]() </div></td>
<td><div style="width:100px">![alt text]() </div></td>
<td><div style="width:100px">![alt text]() </div></td>
<td><div style="width:100px">![alt text]() </div></td>
<td><div style="width:100px">![alt text]() </div></td>
</tr>
</table> -->
<table>
	<tr>
<td><div style="height:100px">![alt text](https://members.loria.fr/PFVillard/files/results/hernia1.png) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/supervision/img/Samy.jpg) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/results/skeleton.jpg) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/results/anatomy.jpg) </div></td>
<td><div style="height:100px">![alt text](http://members.loria.fr/PFVillard/files/results/shapeimage_2.png) </div></td>
</tr>
</table>



### Exemple 1 : Valve cardiaque

<table>
<tr>
<td><div style="height:400px">![alt text](https://team.inria.fr/curative/files/2017/11/P5prolapse.gif) </div></td>
<td><div style="height:400px">![alt text](https://team.inria.fr/curative/files/2011/07/sofa.gif) </div></td>
</tr>
</table>



### Exemple 2 : Modélisation de la respiration

<table>
<tr>
<td><div style="height:300px">![alt text](http://members.loria.fr/PFVillard/files/results/model_respi.gif) </div></td>
<td><div style="height:300px">![alt text](http://members.loria.fr/PFVillard/files/results/parameters.jpg) </div></td>
</tr>
</table>

&rarr; Realité augmentée et réalité virtuelle



#### Réalité virtuelle

![alt text](http://members.loria.fr/PFVillard/files/results/shapeimage_3.png)

---

<div style="height:300px">![alt text](img/us.mp4) </div>



#### Realité augmentée
![alt text](img/ribs.jpg)

---

<div style="height:300px">
![alt text](webGL_files/Intro/anim.gif)
</div>