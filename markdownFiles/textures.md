# Textures

[PF Villard](http://members.loria.fr/PFVillard/)



Principe des textures
---------------------

<img data-src="webGL_files/images/tex1.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Principe des textures
---------------------

<img data-src="webGL_files/images/tex2.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Textures libres de droit
------------------------

1.  [Humus](http://www.humus.name/index.php?page=Textures)
2.  [Open Game Arts'](http://opengameart.org/textures/all)  
3.  [TurboSquid](http://www.turbosquid.com/Search/TextureMaps/free)
4.  [Autodesk's AREA](http://area.autodesk.com/downloads/textures)



Les textures UV
---------------

</br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex3_1.png" style="background:none; border:none; box-shadow:none;max-width:220%;position:absolute;top:100pt;left:100pt;max-width:220%;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
<img data-src="webGL_files/images/tex3_2.png" style="background:none; border:none; box-shadow:none;max-width:220%;position:absolute;top:100pt;left:100pt;">
</span>
<span class="fragment fade-in" data-fragment-index="2">
<img data-src="webGL_files/images/tex3_3.png" style="background:none; border:none; box-shadow:none;max-width:220%;position:absolute;top:100pt;left:100pt;">
</span>            



<img data-src="webGL_files/images/tex34.png" style="background:none; border:none; box-shadow:none;max-height:100%;">



Démo textures UV
----------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_geometries.html" data-preload></iframe>



Texture mapping
---------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_loader_md2_control.html" data-preload></iframe>



Texture mapping
---------------

<img data-src="webGL_files/images/tex4.png" style="background:none; border:none; box-shadow:none;max-width:100%;"><br>
<span class="fragment fade-in" data-fragment-index="1">
<font color="blue">Atlas de texture</font><br>
<b>Mosaic</b>
</span>



Coordonnées Texel
-----------------

<p style="text-align: left;">
<img data-src="webGL_files/images/tex5.png" style="background:none; border:none; box-shadow:none;max-height:60%;">
</p>
<div style="position:absolute;top:200pt;left:450pt;width: 350px">
<ol class="noBullet">
<li>Quel <b>Texel</b> est à la position:<br>
<font color="blue">U, V</font>= <b>(0.72, 0.40)</b></li>
<li>Colonne : &nbsp;<input type="text-area" style="font-size:20pt" size="2"></li>
<li>Ligne : &nbsp;<input type="text-area" style="font-size:20pt" size="2"></li>
</ol>
<div>



Démo sur les coordonnées UV
---------------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV.html" data-preload></iframe>



Distorsion des coordonnées UV
-----------------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV2.html" data-preload></iframe>



Quizz sur la distorsion
-----------------------

**Quelle est l'origine des distorsions ?**

<ul class="noBullet">
    <li><input type="checkbox" id="option1"/>
    <label for="option1"> <span></span> La texture d'origine a subi une distorsion avant le déplacement des coins</label></li>
    <li><input type="checkbox" id="option2"/>
    <label for="option2"> <span></span> La bibliothèque Three.js a un bug </label></li>
    <li><input type="checkbox" id="option3"/>
    <label for="option3"> <span></span> Le carré est constitué d'un ensemble de petits triangles </label></li>
    <li><input type="checkbox" id="option4"/>
    <label for="option4"> <span></span> Le carré est constitué d'uniquement deux triangles </label></li>
</ul>



Textures dans Unity
-------------------

</br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/Unity/text2.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:70pt;left:140pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/text3.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:70pt;left:140pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/text4.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:70pt;left:140pt;">
</span> 



Modes d'application d'une texture
-----------------------------------

-   Répéter
-   Répéter en miroir
-   Étirer aux arêtes

<iframe  width="800" height="400" data-src="webGL/demo/texturesUV3.html" data-preload></iframe>



Modes d'application d'une texture
-----------------------------------

**→** Uniquement disponible par écriture de shader

<img data-src="webGL_files/Unity/text1.png" style="background:none; border:none; box-shadow:none;width:25%;">



Transformation de texture
-------------------------

<img data-src="webGL_files/images/tex6.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Grossissement de texture
------------------------

<span class="fragment fade-out" data-fragment-index="3">
<img data-src="webGL_files/images/tex7.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:0pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="5">
<img data-src="webGL_files/images/tex10.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:0pt;">
</span></span>                
<span class="fragment fade-in" data-fragment-index="5">
<img data-src="webGL_files/images/tex11.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:0pt;">
</span> 

<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
<img data-src="webGL_files/images/tex8.png" style="background:none; border:none; box-shadow:none;max-width:80%;position:absolute;top:100pt;left:400pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
<img data-src="webGL_files/images/tex9.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:400pt;">
</span></span>  
 <span class="fragment fade-in" data-fragment-index="6">
<img data-src="webGL_files/images/tex12.png" style="background:none; border:none; box-shadow:none;max-width:80%;position:absolute;top:100pt;left:400pt;">
</span>



Grossissement de texture
------------------------

<img data-src="webGL_files/images/tex13.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Grossissement de texture dans **Unity**
---------------------------------------

<img data-src="webGL_files/Unity/text5.png" style="background:none; border:none; box-shadow:none;width:300px;height:200px;"> &nbsp;
<img data-src="webGL_files/Unity/text6.png" style="background:none; border:none; box-shadow:none;width:300px;height:200px;">   



Grossissement de texture
------------------------

<table>
<tr>
    <td><b>Grossissement</b></td><td><b>Réduction</b></td>
</tr>
<tr>
    <td>
    <img data-src="webGL_files/images/tex15.png" style="background:none; border:none; box-shadow:none;height:300px;">
    </td>
    <td>
    <img data-src="webGL_files/images/tex16.png" style="background:none; border:none; box-shadow:none;height:300px;">            
    </td>
</tr>
<tr>
    <td>Texels/<b>Pixel</b> < <font color="blue"> 1</font> </td>
    <td>Texels/<b>Pixel</b> > <font color="blue"> 1</font> </td>                
</tr>
</table>



Démo sur le grossissement de texture
-------------------------------------
<iframe  class="stretch" data-src="webGL/demo/texturesUV4.html" data-preload></iframe>



Passage de floue à nette
------------------------

**Pourquoi les arrêtes passent-elles de floues à nettes ?**

<ul class="noBullet">
    <li><input type="checkbox" id="optiond1"/>
    <label for="optiond1"> <span></span> C'est le phénomène de dé-zoom sur une texture </label></li>
    <li><input type="checkbox" id="optiond2"/>
    <label for="optiond2"> <span></span> L'interpolation linéaire n'est plus utilisée</label></li>
    <li><input type="checkbox" id="optiond3"/>
    <label for="optiond3"> <span></span> En moyenne, chaque pixel couvre plus qu'un texel </label></li>
    <li><input type="checkbox" id="optiond4"/>
    <label for="optiond4"> <span></span> Le filtre de grossissement n'est plus appliqué </label></li>
</ul>



Dézoomer sur une texture
------------------------

<table>
    <tr>
        <td><b>Grossissement</b></td><td><b>Réduction</b></td>
    </tr>
    <tr>
        <td>
        <img data-src="webGL_files/images/tex15.png" style="background:none; border:none; box-shadow:none;height:300px;">
        </td>
        <td>
        <img data-src="webGL_files/images/tex16.png" style="background:none; border:none; box-shadow:none;height:300px;">            
        </td>
    </tr>
    <tr>
        <td>Texels/<b>Pixel</b> < <font color="blue"> 1</font> </td>
        <td>Texels/<b>Pixel</b> > <font color="blue"> 1</font> </td>                
    </tr>
</table>



Démo en dézoomant sur une texture
---------------------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV5.html" data-preload></iframe>



Mipmapping
----------

<table>
<tr>
    <td><b>Grossissement</b></td><td><b>Réduction</b></td>
</tr>
<tr>
    <td>
    <img data-src="webGL_files/images/tex15.png" style="background:none; border:none; box-shadow:none;height:300px;">
    </td>
    <td>
    <img data-src="webGL_files/images/tex16.png" style="background:none; border:none; box-shadow:none;height:300px;">            
    </td>
</tr>
<tr>
    <td>Texels/<b>Pixel</b> < <font color="blue"> 1</font> </td>
    <td>Texels/<b>Pixel</b> > <font color="blue"> 1</font> </td>                
</tr>
</table>



Mipmapping
----------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex17.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:50pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/tex17b.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:50pt;">
</span>



Chaîne de Mipmaps
-----------------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex18.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:0pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/tex18b.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:0pt;">
</span>



Démo sur les mipmaps
--------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV6.html" data-preload></iframe>



Quiz sur l'augmentation de la taille d'une mipmap
---------------------------------------------------

<ul>
<li> <font color="blue">MIP: </font> <b>"Multum In Pavo"</b> = Beaucoup dans une petit espace</li>
<li>Soit une texture de 32x32x1 qui peut prendre jusqu'à 1024 octets,</li>
<li>Combien de mémoire est utilisée au total par une version mipmap de cette texture ?</li>
<li> <b>Réponse : </b>  &nbsp;<input type="text-area" style="font-size:20pt" size="5"> </li>
</ul>



Exemples dans Unity
-------------------

</br></br></br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/Unity/text7.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/text8.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>



Exemples dans Unity
-------------------

<img data-src="webGL_files/Unity/text8c.png" style="background:none; border:none; box-shadow:none;max-height:90%;">



Anisotropie
-----------

<p style="text-align: left;">
<img data-src="webGL_files/images/tex21.png" style="background:none; border:none; box-shadow:none;width:60%;">
</p>
<div style="position:absolute;top:100pt;left:450pt;width: 420px">
  <ul class="noBullet">
  <li><font color="blue"> Échantillonnage anisotropique</font></li><br>
  <li><b>Échantillonnage :</b> Récupérer la couleur d'une texture d'un fragment</li><br>
  <li><b>Anisotropique :</b> Avoir une valeur dans les différentes directions</li>
  </ul>
<div>



Démo sur l'anisotropie
-----------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV7.html" data-preload></iframe>



Exemple dans Unity
------------------

</br></br></br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/Unity/text9a.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>
<span class="fragment fade-out" data-fragment-index="2">
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/text9b.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
<img data-src="webGL_files/Unity/text9c.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="3">
<img data-src="webGL_files/Unity/text9d.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>



Texture transparente
--------------------

<img data-src="webGL_files/images/tex19.png" style="background:none; border:none; box-shadow:none;width:50%;">
<img data-src="webGL_files/Unity/text10.png" style="background:none; border:none; box-shadow:none;width:40%;">



Texture transparente
--------------------

<img data-src="webGL_files/images/tex22.png" style="background:none; border:none; box-shadow:none;max-width:200%;">



La signification du coefficient alpha
-------------------------------------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex23b.png" style="background:none; border:none; box-shadow:none;max-width:200%;position:absolute;top:100pt;left:10pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/tex23.png" style="background:none; border:none; box-shadow:none;max-width:200%;position:absolute;top:100pt;left:10pt;">
</span>



Alpha pré-multiplié
-------------------

<ul class="noBullet">
<li>Quel est l'avantage à avoir un alpha <font color="green">pré-multiplié</font> </b>?</li><br>
<span class="fragment fade-in" data-fragment-index="1">
<li>C=<b>&alpha;<sub>s</sub>C<sub>s</sub></b>+(1-<b>&alpha;<sub>s</sub></b>)*<font color="blue">C<sub>d</sub></font> </li> <br>
</span>

<div class="fragment fade-in" data-fragment-index="2">
  <li><font color="green">C<sub>p</sub></font>=<b>&alpha;<sub>s</sub>C<sub>s</sub></b> </li>
  <li>C=<font color="green">C<sub>p</sub></font>+(1-<b>&alpha;<sub>s</sub></b>)*<font color="blue">C<sub>d</sub></font> </li>
</div>
</ul>



Particules et billboard
-----------------------
<iframe  width="800" height="300" data-src="https://mrdoob.com/lab/javascript/webgl/clouds/" data-preload></iframe><br>

Autres exemples :

<div>
-   [Simple squares](http://threejs.org/examples/#webgl_points_random)
-   [Circle
    cutouts](http://threejs.org/examples/#webgl_points_billboards)
-   [potree.org](http://potree.org/)
-   [Spheres](http://threejs.org/examples/#webgl_points_billboards_colors)
-   [Snowflakes](http://threejs.org/examples/#webgl_points_sprites)
-   [Clouds](http://mrdoob.com/lab/javascript/webgl/clouds/)
-   [Lensflare](http://threejs.org/examples/#webgl_lensflares)
</div> <!-- .element: style="height:150px;overflow:auto;"-->



Displacement et normal map
--------------------------
<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_materials_displacementmap.html" data-preload></iframe>



Displacement et normal map
--------------------------
<img data-src="webGL_files/images/tex25.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Displacement et normal map
--------------------------
<img data-src="webGL_files/images/tex26.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Exemple dans Unity
------------------

</br></br></br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/Unity/text11a.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:240pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/text11b.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:240pt;">
</span>



Displacement et normal map
--------------------------

<p>A quel endroit se calcule le déplacement de la surface ?</p>
<ul class="noBullet">
    <li><input type="checkbox" id="b1"/>
    <label for="b1"> <span></span> Sur le <b>Vertex Shader</b> </label></li>
    <li><input type="checkbox" id="b2"/>
    <label for="b2"> <span></span> Pendant la <b>rasteurisation</b> </label></li>
    <li><input type="checkbox" id="b3"/>
    <label for="b3"> <span></span> Sur le <b>Fragment Shader</b> </label></li>
    <li><input type="checkbox" id="b4"/>
    <label for="b4"> <span></span> Sur le <b>Z-Buffer</b> </label></li>
</ul>
<img data-src="webGL_files/images/tex27.png" style="background:none; border:none; box-shadow:none;max-width:100%;">   



Création de fumée dans Unity
----------------------------

-   Créer un environnement composé de 6 murs (cubes)
-   Appliquer un **matériau** quelconque sur ces murs
-   Ajouter un cylindre avec une **texture de bois**
-   Ajouter un **système de particule** (à partir de
    GameObject)

<iframe  class="stretch"  data-src="demo/fire/index.html" data-preload></iframe>



## Solution de paramétrisation:
- **Start Lifetime** pour modifier la hauteur de flamme :
![](./webGL_files/PS/LifeTime.gif)<!-- .element: style="width: 10%;" -->
- **Start Speed** pour modifier la propagation :
![](./webGL_files/PS/StartSpeed.gif)<!-- .element: style="width: 10%;" -->
- **Start Size** pour modifier la taille d'une particule :
![](./webGL_files/PS/StartSize.gif)<!-- .element: style="width: 5%;" -->
- Les paramètres de **Shape**
- **Rate over Time** de **Emission** pour modifier le nombre de particule
- La courbe de **Size Over Lifetime** pour avoir une forme de flamme
- Le matériel avec un shader **Particles/Additive** avec une texture de flamme dans **Renderer** (comme [celle-ci](./webGL_files/PS/ParticleFirecloud.png))



Light mapping
-------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_materials_lightmap.html" data-preload></iframe>



Light mapping
-------------

<img data-src="webGL_files/images/tex29.png" style="background:none; border:none; box-shadow:none;max-width:35%;"> 
<img data-src="webGL_files/images/tex28.png" style="background:none; border:none; box-shadow:none;max-width:25%;"> 
<img data-src="webGL_files/Unity/text13.png" style="background:none; border:none; box-shadow:none;max-width:35%;">



Skybox
------

<iframe  class="stretch" data-src="https://stemkoski.github.io/Three.js/Skybox.html" data-preload></iframe>



Skybox
------

 <img data-src="webGL_files/images/tex30.png" style="background:none; border:none; box-shadow:none;width:50%;">
<p style="max-height:80%;position:absolute;top:100pt;right:10pt;">&rarr;Texture de type <b>cube map</b></p>



Skybox sur l'île
-----------------

-   Aller dans l'**asset store** pour importer un (petit) package de
    **skybox** (ex : **Cope!**)
-   Sélectionner **Window**>**Rendering**>**Lightning Settings**
-   Appliquer la skybox précédemment téléchargée
-   Observer le résultat

<img data-src="common/CourseCoversmall.png" style="background:none; border:none; box-shadow:none;width:50%;">



Reflexion map
-------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_materials_envmaps.html" data-preload></iframe>



Reflexion map
-------------

<img data-src="webGL_files/images/tex31.png" style="background:none; border:none; box-shadow:none;max-height:40%;">



Sphère avec reflexion map
-------------------------

- **Component** -> **Rendering**  -> **Reflection Probe**
  - ```Type``` &rarr; ```realtime```
- Créer un nouveau matériel avec  **Metallic** et **Smoothness** à 1, J'ai trouvé pour la reflexion : il faut aller sur la sphere, reflexion probe et mettre le baking en real-time

<img data-src="webGL_files/images/reflexionMap.png" style="background:none; border:none; box-shadow:none;width:40%;">



Refraxion map
-------------

<iframe  class="stretch" data-src="https://stemkoski.github.io/Three.js/Refraction.html" data-preload></iframe>