# 3D Modelling

[PF Villard](http://members.loria.fr/PFVillard/)



Outline
----

1.  3D space
2.  Meshing
3.  Construction
4.  Terrain modeling
5.  Using Blender

![](webGL_files/Unity/im1.png)<!-- .element: style="width: 50%;" -->



Vertices, line and triangles
-----------------------------

- That's all you need!
- No volume

<img data-src="webGL_files/images/Pts14.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Vertices
-----
</br><img data-src="webGL_files/images/Pts15.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Line
-------
</br><img data-src="webGL_files/images/Pts16.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Triangle
--------
</br><img data-src="webGL_files/images/Pts17.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Space in Unity
-----------------
<img data-src="webGL_files/Unity/im8.png" style="background:none; border:none; box-shadow:none;max-width:90%;max-height:60%;">



Creating and manipulating objects
----------------------------------

- Create a new **3D project**
- Add a **Cube** (**GameObject**→**3D Object**)
- A cube of size 1x1x1x1 is then created at **coordinates** (0,0,0,0)
- To manipulate it, use the buttons at the top left.
<p align="center"><img data-src="webGL_files/Unity/jeu6.png" style="background:none; border:none; box-shadow:none;max-height:50%;"></p>
	- **view** (shortcut **Q**) allows you to move around the scene
	- **Translation** (shortcut **W**) allows you to move the object
	- **Rotate** (shortcut **E**) allows you to rotate the object
	- **stop** (shortcut **R**) allows you to change the scale



2 - Mesh
------------
</br><img data-src="webGL_files/images/Pts18.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Polyhedral approximation
-------------------------

Approximation by flat facets from points distributed over the
surface area

 <div style="text-align: right; float: right; right; width: 60%">
1.  Take into account the geometry of the object to preserve the edges
    vivid
2.  Rely on a face configuration
3.  Triangulate
4.  Put the triangles together in strips
5.  Pooling vertices (indexed geometry)

</div>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/Unity/im9a.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/im9b.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/Unity/im9c.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="4">
</br><img data-src="webGL_files/Unity/im9d.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="4">
</br><img data-src="webGL_files/Unity/im9e.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>



Mesh complexity
----------------------

Increase resolution **&rarr;** Costly
</br><img data-src="webGL_files/Unity/im10.png" style="max-width:100%;max-height:100%;left:0pt;">



Backface culling
----------------

- Rendering optimization
- The fastest way to display a polygon is not to display it
- Can be activated or not

<img data-src="webGL_files/images/Pts23.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Example with a V-shape
---------------------------

- Only one side will be displayed at the end

<img data-src="webGL_files/images/Pts24.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Vertex order
--------------------------

- Used to determine whether front or rear face
- Front panel if trigonometric direction

<img data-src="webGL_files/images/Pts25.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Example
-------
<iframe  marginwidth="300" width="1000" height="500" data-src="https://webglfundamentals.org/webgl/webgl-3d-step4.html" data-preload></iframe>



Example
-------

Geometry modelled under 3DS Max 

![](webGL_files/Unity/im11a.png)<!-- .element: style=" width: 34%;" -->
<!-- <img data-src="webGL_files/Unity/im11a.png" style="max-height:30%"></br> -->

Geometry visualised under Unity
<!-- <img data-src="webGL_files/Unity/im11b.png" style="max-height:30%;"></br> -->

![](webGL_files/Unity/im11b.png)<!-- .element: style=" width: 34%;" -->



Construction of land under Unity
----------------------------------
<div>
- Add the package **Environment**
- Create a field (menu **GameObject**) with **Resolution** =
    50x50x50x50
- Modify the **relief** of the terrain by modifying the height map
- Add **textures** using the resources of the package
    **Environment**
- Add **trees** in the same way
- Add **grass** in the same way
</div><!-- .element: style="float: left; width: 65%;" -->

<span class="fragment fade-in" data-fragment-index="0">
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/Unity/im12a.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/im12b.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/Unity/im12c.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="4">
</br><img data-src="webGL_files/Unity/im12d.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="4">
<span class="fragment fade-out" data-fragment-index="5">
</br><img data-src="webGL_files/Unity/im12e.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="5">
<span class="fragment fade-out" data-fragment-index="6">
</br><img data-src="webGL_files/Unity/im12f.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="6">
<span class="fragment fade-out" data-fragment-index="7">
</br><img data-src="webGL_files/Unity/im12g.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="7">
<span class="fragment fade-out" data-fragment-index="8">
</br><img data-src="webGL_files/Unity/im12h.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="8">
<span class="fragment fade-out" data-fragment-index="9">
</br><img data-src="webGL_files/Unity/im12i.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="9">
<span class="fragment fade-out" data-fragment-index="10">
</br><img data-src="webGL_files/Unity/im12j.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="10">
<span class="fragment fade-out" data-fragment-index="11">
</br><img data-src="webGL_files/Unity/im12k.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="11">
<span class="fragment fade-out" data-fragment-index="12">
</br><img data-src="webGL_files/Unity/im12l.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	</span>				
<span class="fragment fade-in" data-fragment-index="12">
</br><img data-src="webGL_files/Unity/im12m.png" style="background:none; border:none; box-shadow:none;width:40%;position:absolute;top:150pt;left:500pt;">
</span>	



Unity File
-------------
<div>
- Record the **scene**
- Register the **project**
- Exit
- Open the project
- Add **water**
</div><!-- .element: style="float: left; width: 65%;" -->
<img data-src="webGL_files/Unity/im13.png" style="width:34%;float:right;">



Explore the Island
---------------
<div>
- Add invisible walls around the island
- Import the package **Character**
- Add a **FPSController** in the middle of the island
- →**Run** and observe the island
</div><!-- .element: style="float: left; width: 65%;" -->

<span>
	<div class="fragment fade-out" data-fragment-index="1">
		<img data-src="webGL_files/Unity/im14a.png" style="background:none; border:none; box-shadow:none;width:34%;position:absolute;top:150pt;left:500pt;">
	</div>	
</span>		
<span>
	<div class="fragment fade-in" data-fragment-index="1">
		<img data-src="webGL_files/Unity/im14b.png" style="background:none; border:none; box-shadow:none;width:34%;position:absolute;top:150pt;left:500pt;">
	</div>
</span>



Using Blender
----------------------

- Many **free 3D models** exist on the internet
- Otherwise, the easiest way to use **Blender**
- Training with internet tutorials
- Import in **FBX** format

<img data-src="webGL_files/Unity/im15.png" style="width:50%;">



Mesh optimization
------------------------

- Low Poly / High Poly
- Unity 3D announces 64k per object
- Play on calculation time / accuracy
- It is better to play with textures and other tricks
- Depends on equipment (changes over time)

<img data-src="webGL_files/Unity/im16.jpg" style="width:50%;">



Follow up :
-------

[Colours and materials](#materiau)