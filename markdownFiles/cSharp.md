# Le C sharp



## Langage machine
<pre><code class="hljs" data-trim contenteditable>
0011000100000000 00000000 00110001 
0000000100000001 00110011 00000001 
0000001001010001 00001011 00000010 
0010001000000010 00001000 01000011 
0000000100000000 01000001 00000001 
0000000100010000 00000010 00000000 
0110001000000000 00000000 00000000 
</code></pre>



## Les mêmes instructions en Français 

1. Enregistrer le numéro 0 dans l'emplacement de mémoire 0. 
+ Enregistrer le numéro 1 dans l'empl. de mém. 1. 
+ Enregistrer la val de l'empl. de mém. 1 dans empl. mém. 2. 
+ Soustraire le numéro 11 de la valeur dans l'empl. de mém. 2. 
+ Si la valeur de l'empl. de la mémoire 2 est le nombre 0, passer à l' instruction 9. 
+ Ajouter la valeur de l'empl. de mém. 1 à l'empl. de mém. 0. 
+ Ajouter le numéro 1 à la valeur de l'empl. de mém. 1. 
+ Continuer avec l'instruction 3. 
+ Sortie de la valeur de l'empl. de mém. 0. 



## Avec des noms de variable et en anglais 

<pre><code class="hljs" data-trim contenteditable>
Set"total" to 0.
Set"count" to 1. [loop]
Set"compare" to "count".
Subtract11 from "compare".
If"compare" is zero, continue at [end]. 
Add"count" to "total".
Add1 to "count".
Continueat [loop].
[end]
Output"total".
</code></pre>



## Le programme en C Sharp 

<pre><code class="hljs" data-trim contenteditable>
int total = 0; count = 1; 
while(count <= 10) {
	total+= count;
	count+= 1; 
}
Debug.log(total);
//→ 55
</code></pre>



## Conclusion 

- Le même programme peut être exprimé de différentes sortes 
- Un bon langage informatique permet d'effectuer des actions de haut niveau 



## Le C sharp 
- Créé en septembre **2000** avec la plateforme .NET 
- Commercialisé par **Microsoft** depuis 2002
- C# = langage de programmation orientée objet 
- Tous les types sont des objets
- Langage très flexible et facile 
- Utilisé par **Unity** 	




## Le code, et quoi faire avec 

- Le code est du texte qui fait un programme 
- Lire et <b>écrire</b> du code est la seule façon d'apprendre 
- <font color=blue>&rarr;</font>Copier/coller des exemples 	
- <font color=blue>&rarr;</font>Modifier les exemples 	



<!-- 
##Variables 

	- Les nombres sont  
	-  

<divclass='right' style='float:right;width:{{right.width}}'>
<iframe width='600px' height='700px' src="jssandbox.html" seamless></iframe>
</div>

-->

## Quatre notions importantes 

- Variables 
- Structures de contrôle 
- Fonctions 
- Les objets



## Valeurs 

- Un nombre, ex : <code>13, 9.81, 2.998e8,(100+4)*11</code> <small>(2<sup>64</sup> nombres possibles sur 64 bits soit  18 quintillion <font color=blue>&rarr;</font> 18 avec 18 zeros derrière)</small> 
- Une chaîne de caractères, ex : <code>"toto"</code> 
- Un booléen : <code>true, false</code> 
<fontcolor=blue>&rarr;</font> <code>(4==5) donne un booléen</code>




## Variables 

- Une variable est définie par son <code>type</code>. ex : <code>float x=5.0F;</code> 
- La valeur d'une variable peut changer 
<pre><code data-trim contenteditable>	
int dette = 140; 
dette= dette - 35;	
</code></pre>	
- Les variables commencent par une lettre et ne doivent pas être un mot déjà réservé. 
<span class="fragment fade-out" data-fragment-index="1">
<b>exemple de mot réservé?</b> 
</span>
<span class="fragment fade-in" data-fragment-index="1">
<code>breakcase catch class const continue debugger default delete do else enum export extends false finally for function if implements using in instanceof interface let new null package private protected public return static super switch this throw true try typeof var void while with yield
</code></span>



## Les structures de contrôle



## Flux simple 
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/flux1.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">

- Actions exécutées à la suite les unes des autres 

<pre><code data-trim contenteditable>
int theNumber = Number(prompt("Pick a number", "")); 
alert(" Your number is the square root of " +
theNumber* theNumber);
</code></pre>



## Condition 
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/flux2.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">

- Actions exécutées avec une condition 

<pre><code data-trim contenteditable>
int theNumber = Number(prompt("Pick a number", "")); 
if(!isNaN(theNumber))
{
	alert(" Your number is the square root of " +
theNumber* theNumber);
}
</code></pre>



## Conditions multiples 
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/flux3.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">

- Actions exécutées avec plusieurs conditions 

<pre><code data-trim contenteditable> 
int theNumber = Number(prompt("Pick a number", ""));
if(num < 10) {
 alert("Small");
}	
else if (num < 100) {
   alert("Medium");
 } 
 else {
    alert("Large");
 }   
</code></pre>



## Boucles <code>for</code> et <code>while</code> 
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/flux4.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;"><br>

- Actions exécutées plusieurs fois 

<pre><code data-trim contenteditable> 
int number = 0;
while(number <= 12) {
	Debug.Log(number);
	number= number + 2; 
}
</code></pre>
<pre><code data-trim contenteditable> 
int result = 1;
int counter = 0;
while(counter < 10) {
	result= result * 2;
	counter= counter + 1; 
}
</code></pre>



## Les fonctions 

- Les fonctions sont définies par : 
<pre><code data-trim contenteditable>	
type nomFonction(desParametres){
	...
	//desinstructions;
	...
	return uneValeur;
}
</code></pre>
Pour utiliser une fonction il suffit de l'appeler : 
	<pre><code data-trim contenteditable>
nomFonction(param);
</code></pre>



## Les objets
- Les variables sont des **objets**
- Définis par des **attributs**
   - Ex : la couleur d'un objet 3D
- Définis par des **méthodes**
   - Ex : faire tourner un objet 3D
- Les attributs sont accessibles avec le "**.**"
	<pre><code data-trim contenteditable>
    avion.longueur=10.0F;
    avion.avance();
</code></pre>

