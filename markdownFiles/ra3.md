# RÉALITÉ AUGMENTÉE

[PF Villard](http://members.loria.fr/PFVillard/)



## Introduction
<span class="menu-title" style="display: none">Introduction (RA)</span>



### Introduction
- Compléter notre perception du **monde réel** avec des **éléments fictifs**

- Incruster de façon réaliste des **objets virtuels 3D** dans une **séquence d’images**

- Pour les puristes : interactif, **temps réel**



### Difficultés
<div style="position:absolute;  top:100pt;left:100pt;height:70%; margin:0 auto;">
<img data-src="webGL_files/RA/ra1.png" >
</div>
<br><br><br><br>

- Perspective <!-- .element: class="fragment" data-fragment-index="1" -->
- Forme <!-- .element: class="fragment" data-fragment-index="2" -->
- Lumière <!-- .element: class="fragment" data-fragment-index="3" -->
<br><br><br><br><br><br><br><br>



### Exemple
<div style="  width:640px; height:480px;   margin:0 auto;">
  <img  src="webGL_files/RA/ra2a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra2b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra2c.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="webGL_files/RA/ra2d.png" style="position:absolute;top:0;left:0;" />      
</div>



### Projection perspective 
  <img src="webGL_files/RA/ra3a.png"  />    



### Objectif = Trouver la bonne transformation



### Exemple de transformation
<div style="  width:640px; height:400px;   margin:0 auto;">
  <img  src="../dinoChampi/nothing.jpg" style="position:absolute;top:20;left:0;" />
  <img class="fragment" src="../dinoChampi/initial.jpg" style="position:absolute;top:20;left:0;" />
</div>



### Position de l'image 
  <img src="../dinoChampi/camera.png" style="border:none;width:640px; height:480px;"/>



### Objectif
<div style="  width:640px; height:400px;   margin:0 auto;">
  <img  src="../dinoChampi/nothing.jpg" style="position:absolute;top:20;left:0;" />
  <img class="fragment" src="../dinoChampi/initial.jpg" style="position:absolute;top:20;left:0;" />
  <img class="fragment" src="../dinoChampi/final.jpg" style="position:absolute;top:20;left:0;" />
  <img class="fragment" src="../dinoChampi/transform.jpg" style="position:absolute;top:20;left:0;" />
  <img class="fragment" src="../dinoChampi/transform2.jpg" style="position:absolute;top:20;left:0;" />
</div>



### Translation 1
<img  src="../dinoChampi/translation1.gif" style="width:640px; height:480px;">



### Translation 2
<img  src="../dinoChampi/translation2.gif" style="width:640px; height:480px;">



#### Mise à l'échelle
<img  src="../dinoChampi/scale.gif" style="width:640px; height:480px;">



### Rotation
<img  src="../dinoChampi/rotation.gif" style="width:640px; height:480px;">



### Image finale
<img  src="../dinoChampi/final.jpg" style="width:640px; height:480px;">




<table>
  <tr>
    <td>
<img  src="../dinoChampi/translation1.gif" style="width:320px; height:240px;   margin:0 auto;">
    </td>
    <td><b>&rarr;</b></td>
    <td>
<img  src="../dinoChampi/translation2.gif" style="width:320px; height:240px;   margin:0 auto;">
    </td>
  </tr>
  <tr>
    <td>Translation 1</td> <td></td><td>Translation 2  &nbsp;&nbsp; <b>&darr;</b></td>
  </tr>
<!--   <tr>
  <td></td><td></td><td></td>
</tr> -->
   <tr>
    <td>
<img  src="../dinoChampi/rotation.gif" style="width:320px; height:240px;   margin:0 auto;">
    </td>
    <td><b>&larr;</b></td>
    <td>
<img  src="../dinoChampi/scale.gif" style="width:320px; height:240px;   margin:0 auto;">
    </td>
  </tr>
  
  <tr>
    <td>Rotation</td> <td></td><td>Echelle</td>
  </tr> 
</table>



### Techniques d'inversion de perspective
<span class="menu-title" style="display: none">Techniques (RA)</span>



### Inversion de perspective
- Retrouver le point de vue des yeux en temps réel
- Différentes méthodes
  - <b>Capteurs</b> : robustes, peu précis
  - <b>Analyse d’image</b> : précis, peu robuste



### Capteurs 
- **+** ou **-** précis
- **+** ou **-** soumis aux perturbations
- Environnement de taille **+** ou **-** réduite
- Position ou rotation

<table>
<tr>
<td><div style="height:100px">![alt text](webGL_files/RA/ra4.png) </div></td>
<td><div style="height:100px">![alt text](webGL_files/RA/ra5.png) </div></td>
<td><div style="height:100px">![alt text](webGL_files/RA/ra6.png) </div></td>
<td><div style="height:100px">![alt text](webGL_files/RA/ra7.png) </div></td>
<td><div style="height:100px">![alt text](webGL_files/RA/ra8.gif) </div></td>
</tr>
<tr>
<td>Magnétique</td>
<td>Acoustique </div></td>
<td>Optique</td>
<td>Inertiel</td>
<td>GPS</td>
</tr>
</table>



### Exemple dans Sketchup
![](webGL_files/RA/ra10.png)



### Exemple dans Sketchup
<div style="  width:1200px; height:900px;   margin:0 auto;">
  <img src="webGL_files/RA/sketchup/sketchup.001.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.003.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.004.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.005.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.006.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.007.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.008.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.009.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.010.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.011.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="webGL_files/RA/sketchup/sketchup.012.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
</div>



### La méthode de l’inversion de perspective
Trouver les transformations par connaissance des points dans le monde réel

![](webGL_files/RA/ra11.png)



### Calcul automatique par marqueurs fiduciaux
- **Fiduciaux** = détectables et identifiables

![](webGL_files/RA/ra15.png)



### Qualité du marqueur
![](webGL_files/RA/ra17.png)<!-- .element: style="width: 80%;" -->



### Conseil pour une bonne qualité de marqueur
- Avoir un maximum de **détails**
- Avoir un **contraste** élevé
- L'image doit être **assez grande**
- L'image doit être acquise en **position plane**
- L'image doit être acquise en **évitant les reflets**
- Le marqueur doit être **facile à manipuler** (Ex : *cartes à jouer*)



## Applications
<span class="menu-title" style="display: none">Applications (RA)</span>



### Maintenance industrielle
--------  

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/daqri_glasses.png) | ![](webGL_files/Intro/daqri.gif)<!-- .element: style="width: 80%;" -->



### Jeux vidéo
--------

<div style="height:300px">![alt text](img/minecraft.mp4) </div>
<div style="height:300px">![alt text](img/pokemonGo.mp4) </div>



### Films
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/film1.jpg) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/film2.gif) </div>



### Culture
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/culture1.jpg) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/culture2.gif) </div>



### Architecture
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archi.gif) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archi2.jpg) </div>



### Archéologie
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archeo1.jpg) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archeo2.jpg) </div>



### eCommerce
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/ecom2.png) </div>
<div style="height:300px"><iframe width="560" height="315" src="https://www.youtube.com/embed/vDNzTasuYEw?start=20" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>



### Publicité

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/pub1.gif) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/pub2.jpg)  </div>



### Sport
--------

<div style="height:200px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport1.png) ![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport2.jpg)  </div>
<div style="height:200px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport3.jpg) ![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport4.jpg)  </div>



### Education
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/edu1.gif) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/edu2.jpg)  </div>



### Exemple : Création d'une scène avec RA

![](webGL_files/RA/ra19.png)<!-- .element: style="width: 55%" -->



### Résultat
![](webGL_files/RA/ra22V2.mp4)<!-- .element: style="width: 1% -->



### Dans le médical <!-- omit in toc -->

<div style="height:500px"> ![](img/anim.gif)</div>



## Modèle respiratoire<!-- omit in toc --> 

![](http://members.loria.fr/PFVillard/files/results/model_respi.gif)



## Modèle cardiaque<!-- omit in toc --> 
   
![](https://team.inria.fr/curative/files/2017/11/P5prolapse.gif)



##  <!-- omit in toc --> 
   
![](https://team.inria.fr/curative/files/2011/07/sofa.gif)<!-- .element: style="height: 400px" -->![](https://team.inria.fr/curative/files/2022/07/perfect.gif)<!-- .element: style="height: 400px" -->



## Dans la pédagogie 



# Apprentissage de l'algorithmie <!-- omit in toc --> 

<!--![](webGL_files/RA/exemple/algo2.mp4) .element: style="width: 1% -->
<video controls autoplay loop>
  <source src="webGL_files/RA/exemple/algo.mp4" type="video/mp4">
</video>



## Apprentissage de la géométrie <!-- omit in toc --> 

<!--![](webGL_files/RA/exemple/geometrie.mp4) .element: style="width: 1% -->
<video controls autoplay loop>
  <source src="webGL_files/RA/exemple/geo.mp4" type="video/mp4">
</video>



## Apprentissage du magnétisme <!-- omit in toc --> 

<!-- ![](webGL_files/RA/exemple/magnetisme.mp4).element: style="width: 1% -->
<video controls autoplay loop>
  <source src="webGL_files/RA/exemple/magnetisme.mp4" type="video/mp4">
</video>



## Apprentissage des équations chimiques

<!--![](webGL_files/RA/exemple/molecules.mp4) .element: style="width: 1% -->
<video controls autoplay loop>
  <source src="webGL_files/RA/exemple/molecules.mp4" type="video/mp4">
</video>



## Apprentissage de la composition d'une fleur

<!-- ![](webGL_files/RA/exemple/fleur.mp4)<!-- .element: width=50% -->
<video controls autoplay loop>
  <source src="webGL_files/RA/exemple/fleur.mp4" type="video/mp4">
</video>



## Apprentissage du système solaire <!-- omit in toc -->

<!-- ![](webGL_files/RA/exemple/fleur.mp4)<!-- .element: width=50% -->
<video controls autoplay loop>
  <source src="webGL_files/RA/exemple/solaire.mp4" type="video/mp4">
</video>



### Apprentissage de la composition d'une fleur

![](lamap/video22/fleur.mp4)



### Apprentissage de langue <!-- omit in toc -->

![](lamap/video22/langue.mp4)



### Apprentissage des Réseaux Trophiques <!-- omit in toc -->

![](lamap/video22/trophic.mp4)



### Apprentissage des séismes <!-- omit in toc -->

![](lamap/video22/seisme.mp4)