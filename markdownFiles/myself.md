My story
---------

- **2002-2006**: PhD in University of Lyon 
- **2006-2007**: Post-doc in CIMIT in Boston
- **2007-2009**: Post-doc in Imperial College in London
- **Since 2009**: Associate professor in University of Lorraine

- Main focuses:

**Augmented and virtual reality** in Medicine

&rarr; Check my <a href="http://members.loria.fr/PFVillard/">webpage</a> for more details



Research
--------
- Respiration modeling
- Heart valve simulation
- Deformation and motion of tongue, diaphragm, liver, ...

|   |   |
:-------------------------:|:-------------------------:
![](https://members.loria.fr/PFVillard/files/results/model_respi.gif)  | ![](https://team.inria.fr/curative/files/2011/07/sofa.gif)<!-- .element: style="width: 50%;" -->