# Exercices


<p>
	<small><a href="http://members.loria.fr/PFVillard/">PF Villard</a></small>
</p>



## Spinning a cube



- Create a <b>cube</b> in an empty scene 
- Create the script <code>RotationCube</code> with the following code : 
<pre><code class="hljs" data-trim contenteditable>
float vitesse=50.0F;
void Update () 
{ 
	transform.Rotate(Vector3.right*Time.deltaTime*vitesse);
}
</code></pre>
- Apply this script to the <b>cube</b>. Try it. 
- Add the function <code>OnGUI()</code> and in the body of the function put : 
<pre><code class="hljs" data-trim contenteditable>
GUI.Box(new Rect(Xmin,Ymin,dX,dY),"texte") ; 
</code></pre>
  - Use the right settings to display the text 



-  Add another line in the function <code>OnGUI()</code> to have a horizontal <b>slider</b> that will vary the speed with : 
<pre><code class="hljs" data-trim contenteditable>
vitesse=GUI.HorizontalSlider(new Rect(Xmin,Ymin,dX,dY),variable, 
   nombre minimum, nombre maximum) ; 
</code></pre>
-  Use the right parameters to be able to vary the speed 

<img data-src="../webGL_files/Unity/script2.png" style="max-height:50%;"> 



## Demo
<iframe  class="stretch"  data-src="demo/rotatingCube/index.html"/>



<p align="left">&rarr; Creation of a stopwatch in the upper right-hand corner</p>
-  Create a new script called <b>Timer</b> with the following code
<pre><code class="hljs" data-trim contenteditable>       
private int timer;  /*counter start*/; 
private float wait; /*waiting time*/; 
private bool update =false; 
void Update () {
  if(!update){ 
    StartCoroutine("Pause");
}}
void OnGUI() {
  GUI.Label(new Rect(/*some values*/),
      timer.ToString());
}
IEnumerator Pause(){
  update=true;
  yield return new WaitForSeconds(wait);
  /*increment the timer variable*/ ; 
  update=false;
}
</code></pre>
-  Find out what to put in place of green words 



## Open a door



## Subject
1. Create a scene with a door
+ Detect when a **FPS** is close
+ Gradually open a door
+ Combine the two



## Demo
<iframe class="stretch" data-src="demo/porteWebGL/index.html"/>



## Preamble
- Import the package <a href="package/Activity2.unitypackage">Activity2.unitypackage</a>
- Make a scene with a door and its upright
- Adding a floor and walls
- Add textures contained in the "*Environment*" package (See Package *Characters*)  
<img data-src="../webGL_files/Unity/porte1.png"><!-- .element: style="width: 30%" -->
<img data-src="../webGL_files/Unity/porte2.png"><!-- .element: style="width: 50%" -->



## "FPS detection" script
- Recover the position of the **FPS**
- Display it in the console (**Debug**)
- Indicate when the position is close
**Help: **Help: **
  - Use **FindGameObjectWithTag**
  - Use the attribute **transform**
  - Use the attribute **position**
  - Use **if**
  - Use **Vector3.Distance(X,Y)**



## Door opening script
- Duplicate the door and put it in the open position
- Uncheck "*Mesh Renderer*" to avoid seeing it
- Declare global variables:
  - A **GameObject** for the open door
  - Two **Vector3** for the open and closed position of the door
  - A **float** for the opening/closing time
  - A **float** for the time when the animation is finished
  - A **bool** to know if the door is open or closed

<img data-src="../webGL_files/Unity/porte3.png"><!-- .element: style="width: 10%" -->