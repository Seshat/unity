# Faire un menu

[PF Villard](http://members.loria.fr/PFVillard/)



## Le menu principal

- Utiliser les panneaux (**GameObject**&rarr;**UI**&rarr;**Panel**)
- Choisir une image de fond sur internet
 - la convertir en Sprite (avec **Textute Type**)
- Ajouter un composant **Vertical Layout** et régler la mise en page
- Ajouter des boutons  (**GameObject**&rarr;**UI**&rarr;**Button**)

![](webGL_files/menu/menu1.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Lien vers un panneau statique

- Ajouter un nouveau panneau avec :
	- du texte
	- une image
	- un bouton de retour au menu

![](webGL_files/menu/menu2.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Créer une scène simple

- Enregistrer la scène et la rajouter dans **File**&rarr;**Build Settings**
- Créer une nouvelle scène
- Créer une sphère avec une texture simple
- Ajouter un bouton de retour au menu
- Enregistrer la scène et la rajouter dans **File**&rarr;**Build Settings** 

![](webGL_files/menu/menu3.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Vers un panneau de bouton

- Ajouter un nouveau panneau avec :
	- plusieurs boutons de niveaux
	- un bouton de retour au menu

![](webGL_files/menu/menu4.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Script pour passer d'un menu à l'autre
- Chaque menu est un **GameObject**
- Ils peuvent être activés ou non avec **SetActive(true/false)**

```
public GameObject menu1;
public GameObject menu2;
public GameObject menu3;
	
public void ShowMenu2()
     {
         menu1.SetActive(false);
         menu2.SetActive(true);
         menu3.SetActive(false);
     }
```



## Script pour charger une scène
- Utiliser :

```
using UnityEngine.SceneManagement;
```
- Puis :

```
SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
```

avec le nom de la scène à la place de **SampleScene**  




## Association bouton / action
- Sélectionner un bouton
- Cliquer sur **+** dans la partie **On Click()**
- Mettre le **GameObject** qui contient le script dans **None (Object)**
- Sélectionner la bonne fonction dans **No Function**



## Résultat

<iframe  class="stretch"  data-src="demo/menu/index.html"/>