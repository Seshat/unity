# Exercices

1. Faire tourner un cube
+ Faire bouger un cube
+ Faire un escalier
+ Ouvrir une porte

**Ne pas oublier pas d’enregistrer régulièrement les scènes**
<p>
	<small><a href="http://members.loria.fr/PFVillard/">PF Villard</a></small>
</p>



## Faire tourner un cube



-  Créer un <b>cube</b> dans une scène vide 
-  Créer le script <code>RotationCube</code> avec le code suivant : 
<pre><code class="hljs" data-trim contenteditable>
float vitesse=50.0F;
void Update () 
{ 
	transform.Rotate(Vector3.right*Time.deltaTime*vitesse);
}
</code></pre>
-  Appliquer ce script au <b>cube</b>. Essayer. 
-  Rajouter la fonction <code>OnGUI()</code> et dans le corps de la fonction mettre : 
<pre><code class="hljs" data-trim contenteditable>
GUI.Box(new Rect(Xmin,Ymin,dX,dY),"texte") ; 
</code></pre>
	-  Utiliser les bons paramètres pour pouvoir afficher le texte 



-  Ajouter une autre ligne dans la fonction <code>OnGUI()</code> pour avoir un <b>slider horizontal</b> qui fera varier la vitesse avec : 
<pre><code class="hljs" data-trim contenteditable>
vitesse=GUI.HorizontalSlider(new Rect(Xmin,Ymin,dX,dY),variable, 
   nombre minimum, nombre maximum) ; 
</code></pre>
-  Utiliser les bons paramètres pour pouvoir faire varier la vitesse 

<img data-src="webGL_files/Unity/script2.png" style="max-height:50%;"> 



## Demo
<iframe  class="stretch"  data-src="demo/rotatingCube/index.html"/>



<p align="left">&rarr; Création d'un chronomètre en haut à droite</p>
-  Créer un nouveau script appelé <b>Timer</b> avec le code suivant
<pre><code class="hljs" data-trim contenteditable>       
private int timer;  /*début du compteur*/; 
private float attente; /*temps d’attente*/; 
private bool actualiser =false; 
void Update () {
  if(!actualiser){ 
    StartCoroutine("Pause");
}}
void OnGUI() {
  GUI.Label(new Rect(/*quelques valeurs*/),
      timer.ToString());
}
IEnumerator Pause(){
  actualiser=true;
  yield return new WaitForSeconds(attente);
  /*incrémentation sur la variable timer*/ ; 
  actualiser=false;
}
</code></pre>
-  Trouver ce qu’il faut mettre à la place des mots en vert 



## Bouger un cube



## Sujet
1. Créer un cube
+ Faire un script qui fait avancer le cube
+ Faire un script qui fait avancer/reculer le cube
+ Rajouter une GUI pour changer la vitesse
+ Faire un script qui change la couleur du cube en fonction de la vitesse
+ Faire un changement de couleur tout d'abord vers le rouge, puis vers le vert et enfin vers le bleu 



## Demo
<iframe class="stretch" data-src="demo/movingCube/index.html"/>



## Aide
- Ne pas oublier pas d’utiliser des **float**
<pre><code>float vitesse=0.0F;
</code></pre>
- Pour assigner une nouvelle position *(x, y, z)* :
<pre><code>transform.position=new Vector3(x,y,z);
</code></pre>
- Aide pour changer la couleur <a href="https://docs.unity3d.com/ScriptReference/Material.SetColor.html">ici</a>
- Aide pour créer une couleur <a href="https://docs.unity3d.com/ScriptReference/Color-ctor.html">ici</a>



## Faire un escalier



## Sujet
1. Créer une première marche
+ Créer automatiquement **5** marches
+ Créer une GUI pour faire varier le nombre de marches
+ Créer une GUI pour faire varier la largeur des marches



## Demo
<iframe class="stretch" data-src="demo/escalierWebGL/index.html"/>



## Préambule
- Faire une marche horizontale avec un **Cube**
- Faire une marche verticale
- Rassembler tout dans un **GameObject**   
<img data-src="webGL_files/Unity/escalier1.png"><!-- .element: style="width: 40%" -->
<img data-src="webGL_files/Unity/escalier2.png"><!-- .element: style="width: 40%" -->



## Script
- Faire un **GameObject** vide "*Escalier*"
- Créer un **tag** "*autreMarche*"
- Créer le script avec :
	- des variables globales :
<pre><code>public GameObject uneMarche; //Objet créé
	GameObject escalierInstance;//Objet à créer
	public GameObject[] objects;//Tableau de marches
</code></pre>
	- l'affichage du nombre de marches dans **Debug**
- pour associer un tag :
 <pre><code>escalierInstance.gameObject.tag="autreMarche";</code></pre>



- pour détruire des marches :

 <pre><code>objects = GameObject.FindGameObjectsWithTag("autreMarche");
foreach (var gameObj in objects)
{
    Destroy( gameObj);
}
</code></pre> 	



## Ouvrir une porte



## Sujet
1. Créer une scène avec une porte
+ Détecter lorsqu'un **FPS** est proche
+ Ouvrir progressivement une porte
+ Combiner les deux



## Demo
<iframe class="stretch" data-src="demo/porteWebGL/index.html"/>



## Préambule
- Importer le package <a href="package/Activite2.unitypackage">Activite2.unitypackage</a>
- Faire une scène avec une porte et son montant
- Rajouter un sol et des murs
- Rajouter des textures contenues dans le package "*Environment*" (Cf Package *Characters*)  
<img data-src="webGL_files/Unity/porte1.png"><!-- .element: style="width: 30%" -->
<img data-src="webGL_files/Unity/porte2.png"><!-- .element: style="width: 50%" -->



## Script "détection du FPS"
- Récupérer la position du **FPS**
- L’afficher dans la console (**Debug**)
- Indiquer quand la position est proche
**Aide :**
  - Utiliser **FindGameObjectWithTag**
  - Utiliser l'attribut **transform**
  - Utiliser l'attribut **position**
  - Utiliser **if**
  - Utiliser **Vector3.Distance(X,Y)**



## Script "ouverture de porte"
- Dupliquer la porte et la mettre en position ouverte
- Décocher "*Mesh Renderer*" pour ne pas la voir
- Déclarer des variables globales :
  - Un **GameObject** pour la porte ouverte
  - Deux **Vector3** pour la position ouverte et fermée de la porte
  - Un **float** pour la durée d'ouverture/fermeture
  - Un **float** pour le temps où l'animation est finie
  - Un **bool** pour savoir si la porte est ouverte ou fermée

<img data-src="webGL_files/Unity/porte3.png"><!-- .element: style="width: 10%" -->



Suite :
-------

[Réalité augmentée](#ra)