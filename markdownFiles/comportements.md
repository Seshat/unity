# Comportements physiques

[PF Villard](http://members.loria.fr/PFVillard/)



Plan
----

1.  Objets physiques
2.  Gestion de contact



Objets rigides
--------------

<img data-src="webGL_files/Unity/phys3.png" style="width:25%;">

<div>
- **Physics** → **Rigidbody**
- Définis par :
	-   Position
	-   Vitesse
	-   Accélération
	-   Forces exercées
</div><!-- .element: style="float:left; width:49%;" -->

Quelles relations ?

<!-- <div style="text-align: right; float: right; width: 50%">
<img data-src="webGL_files/Unity/phys3.png" style="max-height:30%;">
</div>
 -->
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/phys1.png" style="width:30%;">
<img data-src="webGL_files/Unity/phys2.png" style="width:30%;">
</span>



RigidBody
---------

-   **Mass** : La masse de l'objet
-   **Drag** : Résistance de l'objet au mouvement (comme la résistance
    à l'air par exemple)
-   **Angular Drag** : Résistance de l'objet pour la rotation
-   **Use Gravity** : L'objet subit-il la gravité ou non
-   **Use kinematic** : Si activé, l'objet n'est plus sujet aux
    forces, collisions et autres. Il n'est géré que par les scripts.
-   **Interpolate** : Adapte les calculs de mouvements au framerate
    (peut être couteux en temps de calcul)
-   **Collision Detection** : Adapte les calculs de collision
-   **Constraints** : Permet d'empêcher le mouvement ou la rotation sur
    une ou plusieurs dimensions



Gestion de collision
--------------------

<div>
- **Détecter** s'il y a une collision
	-   En utilisant de la géométrie dans l'espace
	-   S'il y a une collision, mesurer la distance
- **Répondre** à la collision
	-   En calculant une force pour extraire l'objet
- **Physics** → **XXXCollider** avec **XXX** pouvant être :
	-   Box
	-   Sphère
	-   Capsule
	-   Mesh
</div><!-- .element: style="float: left; width: 79%;" -->

<img data-src="webGL_files/Unity/phys4a.png" style="width:20%;float:left;">
<img data-src="webGL_files/Unity/phys4b.png" style="width:20%;float:left;"> 



Detection de collision
----------------------

- Exemple avec une sphère :
	- Sphère de centre (x<sub>s</sub>, y<sub>s</sub>, z<sub>s</sub>) et rayon R
	- Point de position (x<sub>u</sub>, y<sub>u</sub>, z<sub>u</sub>)

<img data-src="webGL_files/Unity/distance.png" style="width:30%;background-color:white;"></p>

<img data-src="webGL_files/Unity/collision_detection.gif" style="width:30%;background-color:white;">



Force de réponse à la collision
-------------------------------

- Calcul de la direction de réponse :

<img  data-src="webGL_files/Unity/direction.png" style="width:15%;background-color:white;vertical-align:top">
- Calcul de l'intensité de la force :

<img data-src="webGL_files/Unity/force.png" style="width:20%;background-color:white;vertical-align:top">

<img data-src="webGL_files/Unity/collision_response.gif" style="width:25%;background-color:white;">



En pratique
-----------

1.  Créer plusieurs **cylindres**, deux **Plans**, en incliner un
+  Créer une **Sphere** positionnée au-dessus du plan
+  Ajouter un **RigidBody** à la sphère et au cylindres
+  Changer les paramètres et observer le comportement

<iframe  style="height:500px;width:600px;" data-src="demo/quilles/index.html"/>



Suite :
-------

[Menu](unity_3.html)