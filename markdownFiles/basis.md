# Quelques bases sur la programmation de scripts
<p>
	<small><a href="http://members.loria.fr/PFVillard/">PF Villard</a></small>
</p>



## Interaction avec un ordinateur
- La communication avec un ordinateur n’est pas naturelle

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/inter1.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Interaction avec un ordinateur
- Difficile de faire comprendre <b>exactement</b> ce qu’on veut

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/inter2.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Interaction avec un ordinateur
- Le problème du langage

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/inter3.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Variable 
- L’ordinateur a une mémoire pour se rappeler de tout
- Pour se rappeler de tout, il faut utiliser des variables
- Par exemple pour dessiner des carrés, l’ordinateur peut se rappeler combien on veut en dessiner
	- Il faut utiliser une variable pour ça : <b>carres</b> 	

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var1.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Variable 
- L’ordinateur va donc stocker la variable dans sa mémoire 

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var2.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Variable  
1. Pour retrouver l’information, il suffit de donner le nom de la variable
2. L’ordinateur cherchera dans sa mémoire pour redonner l’information 

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var3.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Utilisation de variables 
- Possibilité d’utiliser les variables avec un peu de maths 
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var4.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



 
## Modification de la valeur d’une variable
- ex: on veut dessiner deux nouveaux carrés

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var5.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">
- Attention : le programme oublie la valeur des variables à chaque fois qu’il redémarre



## Calcul mathématique 

- Rappel : on a utilisé 

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var6.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">
- On peut utiliser les autres signes 

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var7.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">
- On peut faire des calculs compliqués

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var8.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">
	



## Types String 

- Il n’y a pas que des nombres à manipuler, il y a aussi des chaînes de caractères (<b>String</b>)  => mots entre guillemets
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var9.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Condition

- Jusqu’à maintenant, l’ordinateur suit des instructions 
- Il ne prend pas de décision 
- Comment lui faire effectuer un choix ? 

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/var1.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Condition 

- Avec l'instruction «<b>si</b>», un ordinateur compare deux choses pour prendre une décision
- Une façon de comparer  est de vérifier si deux choses sont les mêmes 
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond2.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Condition 
- Pour ce faire :
<ol style="font-size:large">
- taper «<b>si</b>» 
- parenthèse ouvrante
- la première chose que vous voulez que l'ordinateur regarde
- deux signes égal
- la deuxième chose que vous voulez que l'ordinateur regarde 
- parenthèse fermante
- accolade ouvrante
- instructions que l'ordinateur doit suivre si les deux choses sont les mêmes
- accolade fermante
</ol>
		
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond4.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;">




## Condition 

- Possibilité aussi de vérifier si une condition n’est pas vérifiée avec <b>!=</b> à la place de <b>==</b>
- Possibilité d’utiliser <b>sinon</b> dans la condition <b>si</b> 
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond5.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Condition 

- Conditions imbriquées 
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond6.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Boucle 

- Objectif : ordinateur répète quelque chose pour **toujours**
- Une boucle pour faire quelque chose plus d'une fois 
- Un type de boucle est la **boucle infinie**

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond3.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Boucle 

- Boucle tant que utilisée de la façon suivante :
<ol>
- tapez <b>tant que(</b> condition <b>)</b> 
- accolade ouvrante
- instructions que vous voulez que l'ordinateur répète
- accolade fermante 
</ol>
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond7.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Boucle 
- Les boucles sont pratiques pour compter
- Le programme ci-dessous commence à 1 et continue de compter
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond8.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Boucle 

- Arrêt dans une boucle

	- Il y a une instruction intitulée <b>arret</b>
	- Quand un ordinateur voit l'instruction <b>arret</b>, il cesse de répéter les choses.
	
	
<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond9.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



## Boucle 

- La boucle <b>pour</b> s’utilise de la façon suivante
<ol style="font-size:large">
- tapez <b>pour</b>
- parenthèse ouvrante 
- initialiser la variable à incrémenter (ex: <b>n=0</b>)
- point virgule
- écrire une condition (ex: <b>n&lt; 100</b>)
	- point virgule
- écrire une incrémentation sur la variable (ex <b>n=n+1</b>)
- parenthèse fermante
- accolade ouvrante
- instructions que vous voulez que l'ordinateur répète
- accolade fermante 
</ol>

<img data-src="https://seshat.gitlabpages.inria.fr/javascript/harmo_files/images/cond10.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;">
