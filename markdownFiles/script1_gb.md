# Scripts in Unity

[PF Villard](http://members.loria.fr/PFVillard/)



## Scripts in Unity
- Introduction
- Scripts linked to **GameObject**
- Debugging
- User interface



## Introduction



## Script writing
- Changes the behaviour of objects
- Written in <b>C&#35;</b>
- Important functions :
    - <code>Start</code> : Called when a script is instantiated
	- <code>Update</code> : Called to each frame
	- <code>OnGUI</code> : Used to display a GI (score, health,...)
	- <code>OnCollisionEnter</code> : Collision detection
	- <code>OnTriggerEnter</code> : Collision with a trigger

<img data-src="webGL_files/Unity/phys5.png"><!-- .element: style="width: 30%" -->



## Important classes 
- Mathematics
 	- <code>Vector3, Quaternion, Mathf, Ray,</code> ...
- Audio
	- <code>AudioClip, audio,</code> ...
- Physique
	- <code>Rigidbody, Collider, Physics,</code> ...
- GUI
	- <code>Texture2D, GUI,</code> ...
- Other
	- <code>GameObject, Input, Application,</code> ...

<img data-src="webGL_files/Unity/phys6.png"><!-- .element: style="width: 15%" -->



## Script creation

- Right click on the tab **Project****
- Select **Create** then **C#Script**
- Rename the script with the desired name
- Double-click on the script to open it
<p align="center">
![](scFig/sc1.png)
</p>



## Un premier script
<pre><code  data-trim data-noescape>using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NewBehaviourScript : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	void Update () {
	}
}
</code></pre>



## Code compilation

- Is done each time you save and return to Unity: : ![](scFig/sc2.png)
- Allows Unity to validate the script
- If we add an error, it will be reported back up:<!-- .element: class="fragment" data-fragment-index="1" -->

<pre><code >void Start()
	{
		score=500;
	}
</code></pre><!-- .element: class="fragment" data-fragment-index="1" -->

![](scFig/sc3.png)<!-- .element: class="fragment" data-fragment-index="2" -->



## Execution of the code

- Execution: script must be assigned to a **GameObject**
- Do a *slide/drop* of the script to the **gameObject**
- Can be seen in the Inspector panel of the Object Game
<p align="center">
![](scFig/sc4.png)
</p>



## Organization

- Use a specific folder for scripts
- Avoid long and complex folder trees
- Explicitly name folders
- Separate scripts related to *UI* and features
- Comment the code



## Scripts linked to GameObject



## Role of MonoBehavior
<pre><code > public class NewBehaviourScript : MonoBehaviour {... }
</code></pre>
- Linked to a **GameObject** 
- Allows access to many features:
  - **Awake** called to the very first instantiation
  - **Start** called to creation after Awake
  - **Update** called at each *frame* (ex: 60 times/sec)
  - **onDestroy** called for the destruction of the **GameObject**
- Doc : <a href="https://docs.unity3d.com/ScriptReference/MonoBehaviour.html">here</a>



<iframe class="stretch" data-src="https://docs.unity3d.com/ScriptReference/MonoBehaviour.html"/>



## Communication between entities
- Before communicating with a script, it must be **located**
- List of methods related to the search for a **GameObject**:
  - <code>GameObject.Find(string name)</code>
  - <code>GameObject.FindGameObjectWithTag(string tag)</code>
  - <code>GameObject.FindGameObjectsWithTag(string tag)</code>



## Le debugging



## Debugging the code
- Utiliser le log :

<pre><code>void Start()
{
	Debug.Log("Enter the Start() method");
}
</code></pre>

- Use exception handling:

<pre><code>try{
	int result=100/0; // Error
}
catch()System.Exception e{
	Debug.LogException("Impossible division");
}
</code></pre>



## Debugging graphically
- Draw a line between points:

<pre><code>Debug.DrawLine(startpoint, finishpoint, color);//Add the 
										 Duration if not in Update
</code></pre>

ex :

<pre><code>void Update()
{
	Debug.DrawLine(Vector3.zero,new Vector(10, 0,0), Color.green);
}
</code></pre>



## User interface



## GUI creation tools
- Using **MonoBehavior**
  - The method <code>OnGUI</code> is called at each frame
- Two positioning solutions:
  - GUI** : Fixed resolution
<pre><code>//Example :
GUI.Button(new Rect(0,0,80,20),"Click")
</code></pre>
  - **GUILayout** : Variable resolution 

<pre><code>//Example :
GUILayout.Button("Click")
</code></pre>



## GUI Elements (1)
- **Button** &rarr; in a <code>if</code> block
	- ex :
<pre><code>if (GUI.Button(new Rect(0,0,80,20), "Click"))
	Debug.Log("Click !");</code></pre>

- **RepeatButton** similar to Button, repeats itself at each frame 
- **Label** displays a label (no interaction)

<pre><code>GUI.Label(new Rect(0,0,80,20), "Hello !")</code></pre>

- **Toggle** displays a checkbox
	- ex :

<pre><code>GUI.Label(new Rect(0,0,80,20), "Hello !")</code></pre>



## GUI Elements (2)
- **Toolbar**, **SelectionGrid** control the creation of toolbars
	- &rarr; <a href="https://docs.unity3d.com/ScriptReference/GUI.Toolbar.html"> documentation</a>
- **HorizontalSlider, VerticalSlider** Value modifier by selection
	- &rarr; <a href="https://docs.unity3d.com/ScriptReference/GUI.HorizontalSlider.html"> documentation</a>
- **HorizontalScrollbar, VerticalScrollbar** Similar to the previous ones with the addition of a control size
	- &rarr; <a href="https://docs.unity3d.com/ScriptReference/GUI.HorizontalScrollbar.html"> documentation</a>



## Exercises
A series of exercises is available [here](#exo)
