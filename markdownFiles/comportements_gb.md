# Physical Behaviours

[PF Villard](http://members.loria.fr/PFVillard/)



Physical Behaviours
----

1.  Physical objects
+   Contact management



Rigid objects
--------------

<img data-src="webGL_files/Unity/phys3.png" style="width:25%;">

<div>
- **Physics** → **Rigidbody**
- Defined by:
	- Position
	- Speed
	- Acceleration rate
	- Forces exerted
</div><!-- .element: style="float:left; width:49%;" -->

Which relationships?

<!-- <div style="text-align: right; float: right; width: 50%">
<img data-src="webGL_files/Unity/phys3.png" style="max-height:30%;">
</div>
 -->
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/phys1.png" style="width:30%;">
<img data-src="webGL_files/Unity/phys2.png" style="width:30%;">
</span>



RigidBody
---------

-   **Mass** : object mass
-   **Drag** : resistance of the object to movement (such as air resistance for example)
-   **Angular Drag** : Resistance of the object for rotation
-   **Use Gravity** : Does the object undergo gravity or not
-   **Use kinematic** : If activated, the object is no longer subject to forces, collisions and others. It is only managed by scripts
-   **Interpolate** : Adapts motion calculations to framerate (can be expensive in computation time)
-   **Collision Detection** :  Adapts collision calculations
-   **Constraints** : Allows to prevent movement or rotation on one or more dimensions



Practical
-----------

1.  Create a **Plan**
2.  Tilt the plane 10 degrees
3.  Create a **Sphere** positioned above the plane
4.  Position the **camera** to see the two objects
5.  Add a **RigidBody** to the sphere
6.  Change the parameters and observe the behaviour

<p align="center"><img data-src="webGL_files/Unity/phys7.png" style="background:none; border:none; box-shadow:none;width:50%;"></p>



Gestion de collision
--------------------

<div>
- **Detect** if there is a collision
	- By using geometry in space
	- If there is a collision, measure the distance
- **Response** to the collision
	- By calculating a force to extract the object
- **Physics** → **XXXCollider** with **XXX** that can be :
	- Boxes
	- Sphere
	- Capsule
	- Mesh
</div><!-- .element: style="float: left; width: 79%;" -->

<img data-src="webGL_files/Unity/phys4a.png" style="width:20%;float:left;">
<img data-src="webGL_files/Unity/phys4b.png" style="width:20%;float:left;"> 



Collision detection 
----------------------

- Example with a sphere:
	- Sphere with centre (x<sub>s</sub>, y<sub>s</sub>, z<sub>s</sub>) and radius R
	- Point with position (x<sub>u</sub>, y<sub>u</sub>, z<sub>u</sub>)

<img data-src="webGL_files/Unity/distance.png" style="width:30%;background-color:white;"></p>

<img data-src="webGL_files/Unity/collision_detection.gif" style="width:30%;background-color:white;">



Response force to the collision
-------------------------------

- Direction:

<img data-src="webGL_files/Unity/direction.png" style="width:15%;background-color:white;vertical-align:top">
- Intensity :

<img data-src="webGL_files/Unity/force.png" style="width:20%;background-color:white;vertical-align:top">

<img data-src="webGL_files/Unity/collision_response.gif" style="width:25%;background-color:white;">