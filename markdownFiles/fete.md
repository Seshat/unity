# RÉALITÉ VIRTUELLE

[PF Villard](http://members.loria.fr/PFVillard/)



### Mondes virtuels

- De plus en plus commun, univers avec **avatars**
- Les utilisateurs peuvent vivre une «second life»
- **Économie** réelle :
  - Acheter, vendre devenir riche
  - Entreprises réelles avec pub et services
- Plusieurs **activités** :
  - Jeux
  - Éducation, culture

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Second_Life_11th_Birthday_Live_Drax_Files_Radio_Hour.jpg/220px-Second_Life_11th_Birthday_Live_Drax_Files_Radio_Hour.jpg)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://upload.wikimedia.org/wikipedia/en/thumb/6/63/WoWScrnShot_011710_164414.jpg/220px-WoWScrnShot_011710_164414.jpg)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://upload.wikimedia.org/wikipedia/en/7/74/Minecraft_city_hall.png)<!-- .element: style="height: 150px; border-style: none;" -->



### Environnement Virtuel 3D

- Suit le développement de l’informatique
  - algorithme
  - hardware
- Format internet vrml puis X3D
- Lié aux connections **serveur/client**
- Moteur de jeux  pour modéliser des mondes virtuels
   - ex: UNITY3D

![](https://media.giphy.com/media/FxCZAIapF7HeU/giphy.gif)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://thumbs.gfycat.com/SaltyGrayCero-max-1mb.gif)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://static1.squarespace.com/static/58586fa5ebbd1a60e7d76d3e/t/5912d8ae414fb5703828be66/1494407358523/)<!-- .element: style="height: 150px; border-style: none;" -->



![](../webGL_files/RV/faked2.jpg)<!-- .element: style="width: 40%; border-style: none;" -->
![](../webGL_files/RV/faked1.jpg)<!-- .element: class="fragment" data-fragment-index="1" style="width: 40%; border-style: none;" -->



![](../webGL_files/RV/rv.001.png)<!-- .element: style="width: 90%; border-style: none;" -->



![](../webGL_files/RV/rv.002.png)<!-- .element: style="width: 90; border-style: none;"% -->



![](../webGL_files/RV/rv.013.png)<!-- .element: style="height: 150px; border-style: none;" -->  ![](https://orig00.deviantart.net/6592/f/2017/353/0/a/3d_anaglyph_yoda_gif_by_gogu1234-dbx8s1m.gif)<!-- .element: style="height: 150px; border-style: none;" --> 

![](../webGL_files/RV/rv.015.png)<!-- .element: style="height: 150px; border-style: none;"% --> ![](../webGL_files/RV/rv.016.png)<!-- .element: style="height: 150px; border-style: none;" -->



![](../webGL_files/RV/rv.017.png)<!-- .element: style="height: 250px; border-style: none;"% -->![](../webGL_files/RV/rv.018.png)<!-- .element: style="height: 250px; border-style: none;" -->

![](../webGL_files/RV/rv.019.png)<!-- .element: style="width: 60%; border-style: none;" -->



![](../webGL_files/RV/rv.020.png)<!-- .element: style="width: 20%; border-style: none;" -->

![](https://i.gifer.com/Kf6R.gif)<!-- .element: style="width: 60%; border-style: none;" -->



![](../webGL_files/RV/rv.022.png)<!-- .element: style="border-style: none;" -->

![](https://thumbs.gfycat.com/GoodnaturedColorfulGreatdane-size_restricted.gif)