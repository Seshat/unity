My story
---------

- **2002-2006**: Thèse à l'Université de Lyon 
- **2006-2007**: Post-doc au CIMIT à Boston
- **2007-2009**: Post-doc à Imperial College à Londres
- **Depuis 2009**: Maître de conférence à l’UL

- Expertise: **Réalité augmentée et virtuelle** en Médecine

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/simulator.jpg)  | ![](webGL_files/Intro/anim.gif)<!-- .element: style="width: 40%;" -->



Recherches
--------
- Modélisation de la respiration
- Simulation de valves cardiaques
- Déformations et mouvements d'organes

|   |   |
:-------------------------:|:-------------------------:
![](https://members.loria.fr/PFVillard/files/results/model_respi.gif)  | ![](https://team.inria.fr/curative/files/2011/07/sofa.gif)<!-- .element: style="width: 50%;" -->



Réalité augmentée et virtuelle
--------
- Qu'est-ce que la réalité virtuelle ?
- Qu'est-ce que la réalité augmentée ?<!-- .element: class="fragment" data-fragment-index="1" -->
- Quelle est la différence entre les deux ?<!-- .element: class="fragment" data-fragment-index="2" -->

![](webGL_files/Intro/difference.jpg)<!-- .element: style="width: 50%;" -->	



Réalité augmentée et virtuelle
--------

|   |   |   |
----|---|---|
**Réalité virtuelle** | **Réalité augmentée** |   |
 |  |  |
<input type="checkbox"  id="a1"/><label for="a1"><span></span>  </label> | <input type="checkbox"  id="a2"/><label for="a2"><span></span>  </label> |  Le plus immersif  | 
<input type="checkbox"  id="b1"/><label for="b1"><span></span>  </label> | <input type="checkbox"  id="b2"/><label for="b2"><span></span>  </label> |  Enrichit le plus l’expérience utilisateur  | 
 <input type="checkbox"  id="c1"/><label for="c1"><span></span>  </label> | <input type="checkbox"  id="c2"/><label for="c2"><span></span>  </label> |  Utilise un dispositif sous forme de casque |
 <input type="checkbox"  id="d1"/><label for="d1"><span></span>  </label> | <input type="checkbox"  id="d2"/><label for="d2"><span></span>  </label> |  Ne coupe pas l’utilisateur de la réalité  |



Réalité augmentée et maintenance industrielle
--------  

- Exemple 1 : **Daqri**

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/daqri_glasses.png) | ![](webGL_files/Intro/daqri.gif)<!-- .element: style="width: 8	0%;" -->



Réalité augmentée et maintenance industrielle
--------  

- Exemple 2 : **Edusafe** au CERN

![](webGL_files/Intro/radioactivity.jpg)

&rarr; Niveaux de radioactivité



Réalité augmentée et maintenance industrielle
--------  

- Exemple 3 : **iAR**

|   |   |
:-------------------------:|:-------------------------:
![](webGL_files/Intro/iAR_ar1.jpg) | ![](webGL_files/Intro/iAR.gif)<!-- .element: style="width: 100%;" -->



Réalité augmentée et maintenance industrielle
--------  

<iframe src="https://youtu.be/d3YT8j0yYl0?t=60">
</iframe>