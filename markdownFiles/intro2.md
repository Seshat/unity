## CONCLUSION



### Réalité augmentée et virtuelle

- Qu'est-ce que la réalité virtuelle ?
- Qu'est-ce que la réalité augmentée ?<!-- .element: class="fragment" data-fragment-index="1" -->
- Quelle est la différence entre les deux ?<!-- .element: class="fragment" data-fragment-index="2" -->

![](webGL_files/Intro/difference.jpg)<!-- .element: style="width: 50%;" -->	



### Réalité augmentée et virtuelle

|   |   |   |
----|---|---|
**Réalité virtuelle** | **Réalité augmentée** |   |
 |  |  |
<input type="checkbox"  id="a1"/><label for="a1"><span></span>  </label> | <input type="checkbox"  id="a2"/><label for="a2"><span></span>  </label> |  Le plus immersif  | 
<input type="checkbox"  id="b1"/><label for="b1"><span></span>  </label> | <input type="checkbox"  id="b2"/><label for="b2"><span></span>  </label> |  Enrichit le plus l’expérience utilisateur  | 
 <input type="checkbox"  id="c1"/><label for="c1"><span></span>  </label> | <input type="checkbox"  id="c2"/><label for="c2"><span></span>  </label> |  Utilise un dispositif sous forme de casque |
 <input type="checkbox"  id="d1"/><label for="d1"><span></span>  </label> | <input type="checkbox"  id="d2"/><label for="d2"><span></span>  </label> |  Ne coupe pas l’utilisateur de la réalité  |



### Merci de votre attention