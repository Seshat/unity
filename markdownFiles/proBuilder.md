# ProBuilder

[PF Villard](http://members.loria.fr/PFVillard/)



## Introduction

- Accessible via l'Asset Store
- Pour construir des **géométries** de base ou avancées **directement dans Unity**
- Conception de textures UV complètes
- Export de modèles possible

![](webGL_files/proBuilder/pb1.png)<!-- .element: style="width: 35%; border-style: none;" -->



## Utilisation de l'interface graphique

- Selectionner **Tools**>**ProBuilder**>**ProBuilder Window**
- Selectionner **New Shape**
	- Selectionner une forme et la **paramétrer**

![](webGL_files/proBuilder/pb4.png)<!-- .element: style="width: 60%; border-style: none;" -->



- Utiliser la barre d'édition pour modifier la forme:
	- objet, points, arrête ou surface
	- Déplacement (&rarr; **extrusion**), rotation, scale
	- Peut-être combiné avec **shift**

![](webGL_files/proBuilder/pb6.png)<!-- .element: style="width: 60%; border-style: none;" -->



## Autres outils
<!-- The Toolbar is color-coded to help you quickly choose tools by type:
Orange for Tool Panels
Blue for Selection Tools
Green for Object Actions
Red for Geometry Actions (Vertex, Edge, Face) -->
Barre d'outils *coloriée* pour choisir rapidement :
- **Orange** : panneaux d'outils
- **Bleu**: outils de selection (ex: inverser la selection) 
- **Vert**: actions sur l'objet
- **Rouge**: Action géométrique (ex: rayon, suppression, ...)

![](webGL_files/proBuilder/pb3.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Texturing
- Ouvrir l'éditeur de matériel
- Glisser/lacher des matériaux
- Appliquer le matériau (alt+nb)
- ou selectionner des faces
- Pour l'édition des textures UV, ouvrir **UV Editor**	
	- Move, Rotate, Scale

![](webGL_files/proBuilder/pb5.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Exports possible
-  **Tools**>**ProBuilder**>**Export**
- Plusieurs formats possibles :
	- stl, obj, ply, asset Unity

![](webGL_files/proBuilder/pb7.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Exercice
- Construire un terrain avec un **chateau**
	- une colline pour y accéder
	- des murs crénelés
	- Une porte
	- Des tours
	- Un chemin de ronde



## Astuces
- **Subdiviser** les surfaces
- Utiliser l'outil d'**extrusion**
- **Fusionner** les faces
- Sélectionner **toutes les faces concernées** par une extrusion pour avoir une hauteur constante



## Résultat

<iframe  class="stretch"  data-src="demo/chateau/index.html"/>