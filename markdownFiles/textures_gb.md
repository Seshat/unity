# Textures

[PF Villard](http://members.loria.fr/PFVillard/)



How Texturing Works
---------------------

<img data-src="webGL_files/images/tex1.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



How Texturing Works 
---------------------

<img data-src="webGL_files/images/tex2.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Royalty-free textures
------------------------

1.  [Humus](http://www.humus.name/index.php?page=Textures)
2.  [Open Game Arts'](http://opengameart.org/textures/all)  
3.  [TurboSquid](http://www.turbosquid.com/Search/TextureMaps/free)
4.  [Autodesk's AREA](http://area.autodesk.com/downloads/textures)



UV textures 
---------------

</br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex3_1.png" style="background:none; border:none; box-shadow:none;max-width:220%;position:absolute;top:100pt;left:100pt;max-width:220%;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
<img data-src="webGL_files/images/tex3_2.png" style="background:none; border:none; box-shadow:none;max-width:220%;position:absolute;top:100pt;left:100pt;">
</span>
<span class="fragment fade-in" data-fragment-index="2">
<img data-src="webGL_files/images/tex3_3.png" style="background:none; border:none; box-shadow:none;max-width:220%;position:absolute;top:100pt;left:100pt;">
</span>            



<img data-src="webGL_files/images/tex34.png" style="background:none; border:none; box-shadow:none;max-height:100%;">



UV textures demo
----------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_geometries.html" data-preload></iframe>



Texture mapping
---------------

<iframe  width="800" height="500" data-src="https://threejs.org/examples/webgl_loader_md2_control.html" data-preload></iframe>



Texture mapping
---------------

<img data-src="webGL_files/images/tex4.png" style="background:none; border:none; box-shadow:none;max-width:100%;"><br>
<span class="fragment fade-in" data-fragment-index="1">
<font color="blue"> Texture Atlas</font><br>
<b>Mosaic</b>
</span>



UV Coordinates demo
---------------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV.html" data-preload></iframe>



UV Coordinates Distortion
-----------------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV2.html" data-preload></iframe>



Quiz on distortion
-----------------------

**What is the source of the distortions?**

<ul class="noBullet">
    <li><input type="checkbox" id="option1"/>
    <label for="option1"> <span></span> The original texture was distorted before the corners were moved</label></li>
    <li><input type="checkbox" id="option2"/>
    <label for="option2"> <span></span> The Three.js library has a bug </label></li>
    <li><input type="checkbox" id="option3"/>
    <label for="option3"> <span></span>  The square is made up of a set of small triangles </label></li>
    <li><input type="checkbox" id="option4"/>
    <label for="option4"> <span></span>  The square consists of only two triangles </label></li>
</ul>



Textures in Unity
-------------------

</br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/Unity/text2.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:70pt;left:140pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/text3.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:70pt;left:140pt;">
</span> </span>
<span class="fragment fade-in" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/text4.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:70pt;left:140pt;">
</span> 



How to apply a texture
-----------------------------------

- Repeat
- Repeat in mirror
- Stretch at the edges

<iframe  width="800" height="400" data-src="webGL/demo/texturesUV3.html" data-preload></iframe>



Modes of application of a texture
-----------------------------------

**→** Only available through shader writing

<img data-src="webGL_files/Unity/text1.png" style="background:none; border:none; box-shadow:none;width:25%;">



Texture transformation
-------------------------

<img data-src="webGL_files/images/tex6.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Texture magnification
------------------------

<span class="fragment fade-out" data-fragment-index="3">
<img data-src="webGL_files/images/tex7.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:0pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="5">
<img data-src="webGL_files/images/tex10.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:0pt;">
</span></span>                
<span class="fragment fade-in" data-fragment-index="5">
<img data-src="webGL_files/images/tex11.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:0pt;">
</span> 

<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
<img data-src="webGL_files/images/tex8.png" style="background:none; border:none; box-shadow:none;max-width:80%;position:absolute;top:100pt;left:400pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
<img data-src="webGL_files/images/tex9.png" style="background:none; border:none; box-shadow:none;max-width:100%;position:absolute;top:100pt;left:400pt;">
</span></span>  
 <span class="fragment fade-in" data-fragment-index="6">
<img data-src="webGL_files/images/tex12.png" style="background:none; border:none; box-shadow:none;max-width:80%;position:absolute;top:100pt;left:400pt;">
</span>



Texture magnification
------------------------

<img data-src="webGL_files/images/tex13.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Texture magnification in **Unity**
---------------------------------------

<img data-src="webGL_files/Unity/text5.png" style="background:none; border:none; box-shadow:none;width:300px;height:200px;"> &nbsp;
<img data-src="webGL_files/Unity/text6.png" style="background:none; border:none; box-shadow:none;width:300px;height:200px;">   



Texture magnification
------------------------

<table>
<tr>
    <td><b>Magnification</b></td><td><b>Minification</b></td>
</tr>
<tr>
    <td>
    <img data-src="webGL_files/images/tex15.png" style="background:none; border:none; box-shadow:none;height:300px;">
    </td>
    <td>
    <img data-src="webGL_files/images/tex16.png" style="background:none; border:none; box-shadow:none;height:300px;">            
    </td>
</tr>
<tr>
    <td>Texels/<b>Pixel</b> < <font color="blue"> 1</font> </td>
    <td>Texels/<b>Pixel</b> > <font color="blue"> 1</font> </td>                
</tr>
</table>



Texture magnification demo
-------------------------------------
<iframe  class="stretch" data-src="webGL/demo/texturesUV4.html" data-preload></iframe>



From fuzzy to sharp
------------------------

**Why do edges go from blurry to sharp?**

<ul class="noBullet">
    <li><input type="checkbox" id="optiond1"/>
    <label for="optiond1"> <span></span>  It is the phenomenon of unzoom on a texture </label></li>
    <li><input type="checkbox" id="optiond2"/>
    <label for="optiond2"> <span></span>  Linear interpolation is no longer used</label></li>
    <li><input type="checkbox" id="optiond3"/>
    <label for="optiond3"> <span></span>  On average, each pixel covers more than one texel </label></li>
    <li><input type="checkbox" id="optiond4"/>
    <label for="optiond4"> <span></span>  The magnification filter is no longer applied </label></li>
</ul>



Unzoom on a texture 
------------------------

<table>
    <tr>
        <td><b>Magnification</b></td><td><b>Minification</b></td>
    </tr>
    <tr>
        <td>
        <img data-src="webGL_files/images/tex15.png" style="background:none; border:none; box-shadow:none;height:300px;">
        </td>
        <td>
        <img data-src="webGL_files/images/tex16.png" style="background:none; border:none; box-shadow:none;height:300px;">            
        </td>
    </tr>
    <tr>
        <td>Texels/<b>Pixel</b> < <font color="blue"> 1</font> </td>
        <td>Texels/<b>Pixel</b> > <font color="blue"> 1</font> </td>                
    </tr>
</table>



Unzoom on a texture Demo
---------------------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV5.html" data-preload></iframe>



Mipmapping
----------

<table>
<tr>
    <td><b>Magnification</b></td><td><b>Minification</b></td>
</tr>
<tr>
    <td>
    <img data-src="webGL_files/images/tex15.png" style="background:none; border:none; box-shadow:none;height:300px;">
    </td>
    <td>
    <img data-src="webGL_files/images/tex16.png" style="background:none; border:none; box-shadow:none;height:300px;">            
    </td>
</tr>
<tr>
    <td>Texels/<b>Pixel</b> < <font color="blue"> 1</font> </td>
    <td>Texels/<b>Pixel</b> > <font color="blue"> 1</font> </td>                
</tr>
</table>



Mipmapping
----------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex17.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:50pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/tex17b.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:50pt;">
</span>



Mipmaps chain 
-----------------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex18.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:0pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/tex18b.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:100%;position:absolute;top:100pt;left:0pt;">
</span>



Mipmaps demo
--------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV6.html" data-preload></iframe>



Quiz on increasing the size of a mipmap
---------------------------------------------------

<ul>
<li> <font color="blue">MIP: </font> <b>"Multum In Pavo"</b> = much in a small space</li>
<li>Given a 32x32x1 channel texture, memory taken up is 1024 bytes. How much memory is used in total by a mipmap version of this texture?</li>
<li> <b>Answer : </b>  &nbsp;<input type="text-area" style="font-size:20pt" size="5"> </li>
</ul>



Examples in Unity
-------------------

</br></br></br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/Unity/text7.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/text8.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>



Examples in Unity
-------------------

<img data-src="webGL_files/Unity/text8c.png" style="background:none; border:none; box-shadow:none;max-height:90%;">



Anisotropy
-----------

<p style="text-align: left;">
<img data-src="webGL_files/images/tex21.png" style="background:none; border:none; box-shadow:none;width:60%;">
</p>
<div style="position:absolute;top:100pt;left:450pt;width: 420px">
  <ul class="noBullet">
  <li><font color="blue"> Anisotropic sampling</font></li><br>
  <li><b>Sampling :</b> retrieving a texture’s colour for a fragment</li><br>
  <li><b>Anisotropic :</b> having a different value in different directions</li>
  </ul>
<div>



Anisotropy demo
-----------------------

<iframe  class="stretch" data-src="webGL/demo/texturesUV7.html" data-preload></iframe>



Example in Unity
------------------

</br></br></br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/Unity/text9a.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>
<span class="fragment fade-out" data-fragment-index="2">
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/text9b.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
<img data-src="webGL_files/Unity/text9c.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="3">
<img data-src="webGL_files/Unity/text9d.png" style="background:none; border:none; box-shadow:none;max-height:90%;position:absolute;top:100pt;left:140pt;">
</span>



Transparency Mapping
--------------------

<img data-src="webGL_files/images/tex19.png" style="background:none; border:none; box-shadow:none;width:50%;">
<img data-src="webGL_files/Unity/text10.png" style="background:none; border:none; box-shadow:none;width:40%;">



Transparency Mapping
--------------------

<img data-src="webGL_files/images/tex22.png" style="background:none; border:none; box-shadow:none;max-width:200%;">



The Meaning of Alpha
-------------------------------------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/tex23b.png" style="background:none; border:none; box-shadow:none;max-width:200%;position:absolute;top:100pt;left:10pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/tex23.png" style="background:none; border:none; box-shadow:none;max-width:200%;position:absolute;top:100pt;left:10pt;">
</span>



Premultiplied Alpha
-------------------

<ul class="noBullet">
<li>What is the advantage of having a <font color="green">premultiplied Alpha</font> </b>?</li><br>
<span class="fragment fade-in" data-fragment-index="1">
<li>C=<b>&alpha;<sub>s</sub>C<sub>s</sub></b>+(1-<b>&alpha;<sub>s</sub></b>)*<font color="blue">C<sub>d</sub></font> </li> <br>
</span>

<div class="fragment fade-in" data-fragment-index="2">
  <li><font color="green">C<sub>p</sub></font>=<b>&alpha;<sub>s</sub>C<sub>s</sub></b> </li>
  <li>C=<font color="green">C<sub>p</sub></font>+(1-<b>&alpha;<sub>s</sub></b>)*<font color="blue">C<sub>d</sub></font> </li>
</div>
</ul>



Particles and Billboards
-----------------------
<iframe  width="800" height="300" data-src="http://mrdoob.com/lab/javascript/webgl/clouds/" data-preload></iframe><br>

Other examples:

<div>
-   [Simple squares](http://threejs.org/examples/#webgl_points_random)
-   [Circle
    cutouts](http://threejs.org/examples/#webgl_points_billboards)
-   [potree.org](http://potree.org/)
-   [Spheres](http://threejs.org/examples/#webgl_points_billboards_colors)
-   [Snowflakes](http://threejs.org/examples/#webgl_points_sprites)
-   [Clouds](http://mrdoob.com/lab/javascript/webgl/clouds/)
-   [Lensflare](http://threejs.org/examples/#webgl_lensflares)
</div> <!-- .element: style="height:150px;overflow:auto;"-->



Displacement and normal map
--------------------------
<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_materials_displacementmap.html" data-preload></iframe>



Displacement and normal map
--------------------------
<img data-src="webGL_files/images/tex25.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Displacement and normal map
--------------------------
<img data-src="webGL_files/images/tex26.png" style="background:none; border:none; box-shadow:none;max-width:100%;">



Example in Unity
------------------

</br></br></br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/Unity/text11a.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:240pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/Unity/text11b.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:240pt;">
</span>



Displacement and normal map
--------------------------

<p>where does the displacement of the surface happen?</p>
<ul class="noBullet">
    <li><input type="checkbox" id="b1"/>
    <label for="b1"> <span></span> The <b>Vertex Shader</b> </label></li>
    <li><input type="checkbox" id="b2"/>
    <label for="b2"> <span></span> During <b>rasteurisation</b> </label></li>
    <li><input type="checkbox" id="b3"/>
    <label for="b3"> <span></span> The <b>Fragment Shader</b> </label></li>
    <li><input type="checkbox" id="b4"/>
    <label for="b4"> <span></span> The <b>Z-Buffer</b> </label></li>
</ul>
<img data-src="webGL_files/images/tex27.png" style="background:none; border:none; box-shadow:none;max-width:100%;">   



Fire creation in Unity
----------------------------

- Create an environment composed of 6 walls (cubes)
- Apply any **material** to these walls
- Add a cylinder with a **wood texture**
- Add a **particle system** (from **GameObject**)

<iframe  class="stretch"  data-src="demo/fire/index.html"/>



## Parametrization solution:
- **Start Lifetime** to change the flame height:
![](./webGL_files/PS/LifeTime.gif)<!-- .element: style="width: 10%;" -->
- **Start Speed** to modify the propagation:
![](./webGL_files/PS/StartSpeed.gif)<!-- .element: style="width: 10%;" -->
- **Start Size** to change the size of a particle:
![](./webGL_files/PS/StartSize.gif)<!-- .element: style="width: 5%;" -->
- The parameters of **Shape**
- **Rate over Time** of **Emission** to modify the number of particles
- The **Size Over Lifetime** curve to have a flame shape
- Material with a shader **Particles/Additive** with a flame texture in **Renderer** (such as [this one](./webGL_files/PS/ParticleFirecloud.png))



Light mapping
-------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_materials_lightmap.html" data-preload></iframe>



Light mapping
-------------

<img data-src="webGL_files/images/tex29.png" style="background:none; border:none; box-shadow:none;max-width:35%;"> 
<img data-src="webGL_files/images/tex28.png" style="background:none; border:none; box-shadow:none;max-width:25%;"> 
<img data-src="webGL_files/Unity/text13.png" style="background:none; border:none; box-shadow:none;max-width:35%;">



Skybox
------

<iframe  class="stretch" data-src="http://stemkoski.github.io/Three.js/Skybox.html" data-preload></iframe>



Skybox
------

 <img data-src="webGL_files/images/tex30.png" style="background:none; border:none; box-shadow:none;width:50%;">
<p style="max-height:80%;position:absolute;top:100pt;right:10pt;">&rarr;Texture type: <b>cube map</b></p>



Skybox sur l'île
-----------------

- Go to the **asset store** to import a (small) package of
    **skybox** (ex: **Cope!**)
- Select **Window**>>**Rendering**>**Lightning Settings**
- Apply the skybox previously downloaded

<img data-src="common/CourseCoversmall.png" style="background:none; border:none; box-shadow:none;width:50%;">



Reflection map
-------------

<iframe  class="stretch" data-src="https://threejs.org/examples/webgl_materials_cars.html" data-preload></iframe>



Reflection map
-------------

<img data-src="webGL_files/images/tex31.png" style="background:none; border:none; box-shadow:none;max-height:80%;">



Sphere with Reflection map
-------------------------

- Component** -> **Rendering** -> **Reflection Probe**
- Create new material with **Metallic** and **Smoothness** at 1

<img data-src="webGL_files/images/reflexionMap.png" style="background:none; border:none; box-shadow:none;width:40%;">



Refraxion map
-------------

<iframe  class="stretch" data-src="http://stemkoski.github.io/Three.js/Refraction.html" data-preload></iframe>
