# Introduction à la réalité virtuelle

[PF Villard](http://members.loria.fr/PFVillard/)



## Préhistoire de la RV
<div>
- Simuler la sensation de présence d’un **espace virtuel**
- Difficultés :
  - Simuler le mouvement (marche)
  - Réalisme Graphique
  - Appareillage
  - &rarr; Peu de chance
- Ex: Le Cobaye
</div><!-- .element: style="width: 50%; float: left;" -->

![](webGL_files/RV/rv1.png)<!-- .element: style="width: 30%; border-style: none;" -->

![](http://fr.web.img4.acsta.net/c_215_290/medias/nmedia/18/36/14/90/18455828.jpg)<!-- .element: style="height: 150px; border-style: none;" --> ![](https://supermarcey.files.wordpress.com/2014/05/the-lawnmower-man-fi.jpg)<!-- .element: style="height: 150px; border-style: none;" --> 



## Vie virtuelle

- De plus en plus commun, univers avec **avatars**
- Les utilisateurs peuvent vivre une «second life»
- **Économie** réelle :
  - Acheter, vendre devenir riche
  - Entreprises réelles avec pub et services
- Plusieurs **activités** :
  - Jeux
  - Éducation, culture

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Second_Life_11th_Birthday_Live_Drax_Files_Radio_Hour.jpg/220px-Second_Life_11th_Birthday_Live_Drax_Files_Radio_Hour.jpg)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://upload.wikimedia.org/wikipedia/en/thumb/6/63/WoWScrnShot_011710_164414.jpg/220px-WoWScrnShot_011710_164414.jpg)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://upload.wikimedia.org/wikipedia/en/7/74/Minecraft_city_hall.png)<!-- .element: style="height: 150px; border-style: none;" -->



## Environnement Virtuel 3D

- Suit le développement de l’informatique
  - algorithme
  - hardware
- Format internet vrml puis X3D
- Lié aux connections **serveur/client**
- Moteur de jeux  pour modéliser des mondes virtuels
   - ex: UNITY3D

![](https://media.giphy.com/media/FxCZAIapF7HeU/giphy.gif)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://thumbs.gfycat.com/SaltyGrayCero-max-1mb.gif)<!-- .element: style="height: 150px; border-style: none;" -->
![](https://static1.squarespace.com/static/58586fa5ebbd1a60e7d76d3e/t/5912d8ae414fb5703828be66/1494407358523/)<!-- .element: style="height: 150px; border-style: none;" -->



![](webGL_files/RV/faked2.jpg)<!-- .element: style="width: 40%; border-style: none;" -->
![](webGL_files/RV/faked1.jpg)<!-- .element: class="fragment" data-fragment-index="1" style="width: 40%; border-style: none;" -->



![](webGL_files/RV/rv.001.png)<!-- .element: style="width: 90%; border-style: none;" -->



![](webGL_files/RV/rv.002.png)<!-- .element: style="width: 90; border-style: none;"% -->



## 6 règles fondamentales d’immersion

- Guider avec la lumière
- Tirer parti du décor et de l’échelle
- Créer un univers sonore
- Interagir avec le regard
- Ne pas négliger le visuel
- S’inspirer de projets existants



## Guider avec la lumière

- L’utilisateur a la liberté totale de regarder à 360°
- La lumière est très souvent utilisée pour attirer le regard
  - ex : Aura lumineuse cliquable

![](webGL_files/RV/rv.004.png)<!-- .element: style="width: 70%; border-style: none;" -->



## Tirer parti du décor et de l’échelle

- Un environnement exagérément grand permet d’indiquer à l’utilisateur qu’il est petit
- Augmenter l’échelle lors de l’utilisation de porte ou de plafond



## Créer un univers sonore
- Le son et les bruitages contribuent à situer l’utilisateur dans l’univers 3D
- Utiliser des zones d’influence
- Permet de créer une atmosphère
  - Ex: fire, water, animals,…

![](webGL_files/RV/rv.006.png)<!-- .element: style="width: 90; border-style: none;"% -->



## Interagir avec le regard

- Ne pas utiliser de manettes, clavier, souris 
- Utilisation du regard à la place
  - ex: Allumer la lumière en regardant l’interrupteur
  - ex: Un objet devient lumineux quand on le regarde

![](webGL_files/RV/rv.007.png)<!-- .element: style="width: 50%; border-style: none;" -->



## Ne pas négliger le visuel

- L’application doit toujours être belle et fluide
- L’utilisateur doit avoir envie de découvrir et d’explorer l’univers
- Trouver un compromis entre fluidité et beauté

![](webGL_files/RV/rv.008.png)<!-- .element: style="width: 90; border-style: none;"% -->



## S’inspirer de projets existants 

- Beaucoup de projets existent sur Unity, Unreal Engine, Google VR, etc…
- Libre et Open Source
- Il existe beaucoup de modèles 3D et de scripts gratuits

![](webGL_files/RV/rv.009.png)<!-- .element: style="width: 60%; border-style: none;" -->



## Quelle interface pour la VR?

- Affichage 3D 
- Réalité augmentée

![](https://i.makeagif.com/media/9-27-2015/6DPYpu.gif) | ![](https://media.giphy.com/media/Hx0uWHnh1A6bK/giphy.gif)<!-- .element: style="height: 165px;" -->
--- | ---
MYO | Leap Motion



## Appareils

- But : améliorer la visualisation de la scène
- 2 caractéristiques:
  - Meilleure impression de 3D
  - Meilleure impression d’immersion
- Peuvent être combinées



## Lunettes passives
- Peuvent être très simples : rouge/bleu
  - Pas très précise
  - Bon marché
  - Pas d’immersion

![](webGL_files/RV/rv.013.png)<!-- .element: style="height: 150px; border-style: none;" -->  ![](https://orig00.deviantart.net/6592/f/2017/353/0/a/3d_anaglyph_yoda_gif_by_gogu1234-dbx8s1m.gif)<!-- .element: style="height: 150px; border-style: none;" --> 



- Plus compliqué : lunettes polarisées
  - Précision dépend du calibrage
  - Plus cher, avec ou sans capteur
  - Pas d’immersion

![](webGL_files/RV/rv.015.png)<!-- .element: style="height: 150px; border-style: none;"% --> ![](webGL_files/RV/rv.016.png)<!-- .element: style="height: 150px; border-style: none;" -->



## Lunettes actives

- Plus cher, avec ou sans capteur

![](webGL_files/RV/rv.017.png)<!-- .element: style="height: 250px; border-style: none;"% -->![](webGL_files/RV/rv.018.png)<!-- .element: style="height: 250px; border-style: none;" -->



## Google Cardboard

- Possible de bricoler soi-même mais achat des lentilles nécessaire
  - Configuration minimum: 5” screen, full HD, 1.6 GHz 4-cores processor , accelerometer, gyroscope and Android or iOS.
- 2 to 20€

![](webGL_files/RV/rv.019.png)<!-- .element: style="width: 60%; border-style: none;" -->



## Oculus Rift/Go/Quest/Half Dome

![](webGL_files/RV/rv.020.png)<!-- .element: style="width: 20%; border-style: none;" -->

![](https://i.gifer.com/Kf6R.gif)<!-- .element: style="width: 60%; border-style: none;" -->



## HTC Vive

![](webGL_files/RV/rv.022.png)<!-- .element: style="border-style: none;" -->

![](https://s.yimg.com/ny/api/res/1.2/3Dzh.rSEKYPV09DUBUdhjQ--/YXBwaWQ9aGlnaGxhbmRlcjt3PTY0MDtoPTM2MQ--/https://o.aolcdn.com/hss/storage/midas/9c4aa7d1cf85d6413047e1d96d5e490f/203888599/mat+smith+jeeboman.gif)



## Prochaines avancées en RV:
- Meilleure **résolution** de l’écran (comme smartphone)
- Amélioration du focus: 
  - Lunettes avec **eye-tracker**
  - **Position variable** de l’écran
<!-- interaction avec la scène
calcul précis avec bonne résolution uniquement où il y a le focus -->

![](webGL_files/RV/rv1.gif)<!-- .element: style="width: 40%; border-style: none;" -->
![](webGL_files/RV/rv2.gif)<!-- .element: style="width: 40%; border-style: none;" -->



## Intégration avec Unity

![](webGL_files/RV/RV.jpg)<!-- .element: style="width: 40%; border-style: none;" -->

&rarr; Voir mode d'emploi à jour sur [la page web d'Unity](https://docs.unity3d.com/Manual/VROverview.html)



## Exemple avec CardBoard

![](webGL_files/RV/RV29.jpg)<!-- .element: style="border-style: none;width: 50%;" -->


## 1 - Dans la scène Unity

|   |   |
|---|---|
|Permettre la VR dans **Edit** -> **Project Settings** -> **Player Settings**| Modifier les paramètres de la caméra pour les champs de [separation](https://docs.unity3d.com/ScriptReference/Camera-stereoSeparation.html) et [convergence](https://docs.unity3d.com/ScriptReference/Camera-stereoConvergence.html)   
|![](webGL_files/RV/RV31.png)<!-- .element: style="border-style: none;width: 80%;" --> | ![](webGL_files/RV/RV30.png)<!-- .element: style="border-style: none;width: 80%;" --> |


## 2 - Faire un projet pour Smartphone
- Sélectionner **File** -> **Build Settings**
- Sélectionner une scène
- Sélectionner l'OS correspondant au Smartphone
- Cliquer sur **Build**

![](webGL_files/RV/RV32.png)<!-- .element: style="border-style: none;width: 40%;" -->


## 3 - Faire le lien avec le Smartphone

- Avec **iOS** : [doc](https://docs.unity3d.com/Manual/BuildSettingsiOS.html)
- Avec **Android** : [doc](https://docs.unity3d.com/Manual/android-BuildProcess.html)


## 4 - Compiler le projet

- Double cliquer sur le projet à compiler (extension **.xcworkspace** pour iOS)

![](webGL_files/RV/RV33.png)<!-- .element: style="border-style: none;width: 40%;" -->


## Pour iOS
- Signer le projet pour compiler sous iOS

|   |   |   |
|---|---|---|
|Appuyer sur **Automatically manage signing** |Sélectionner la [Team](https://9to5mac.com/2016/03/27/how-to-create-free-apple-developer-account-sideload-apps/)|Compiler|
|![](webGL_files/RV/RV34.png)<!-- .element: style="border-style: none;width: 100%;" --> | ![](webGL_files/RV/RV35.png)<!-- .element: style="border-style: none;width: 100%;" --> | ![](webGL_files/RV/RV36.png)<!-- .element: style="border-style: none;width: 100%;" --> |




## vidéo 360 

- **Principe**: Utiliser la VR pour visionner Vidéo/Photos en 360°
- **Outil**: Camera 360 + Software
  - Quizz / Score / Scorm
  - Branching scenario

![](img/video360/video360.png)



## Exemples

- [Speedernet Sphere](https://youtu.be/Io40xQP0gs0)
- [TP de chimie par Uptale](https://youtu.be/k2-0MYwy7CM )

|Chasse aux risques |Visite virtuelle |Jeu de rôle dans un laboratoire de chimie |
|----|----|-----|
|![](img/video360/ex1.png)|![](img/video360/ex2.png)|![](img/video360/ex3.png) |



## Exemple en distillation continue

Rencontre sur le terrain - Storyboard - Prototype brut - Séquençage - Tournage VR 360° - Tournage pastilles vidéos additionnelles - Enrichissements  - Tests

![](img/video360/ex.png)<!-- .element: style="width:500px" -->