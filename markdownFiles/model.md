# Modélisation sous Unity

[PF Villard](http://members.loria.fr/PFVillard/)



Plan
----

1.  Espace 3D
2.  Maillage
3.  Construction
4.  Modélisation de terrain
5.  Utilisation de Blender

![](webGL_files/Unity/im1.png)<!-- .element: style="width: 50%;" -->



Points, segments et triangles
-----------------------------

-   Pas besoin de plus !
-   Pas de volume

<img data-src="webGL_files/images/Pts14.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Point
-----
</br><img data-src="webGL_files/images/Pts15.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Segment
-------
</br><img data-src="webGL_files/images/Pts16.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Triangle
--------
</br><img data-src="webGL_files/images/Pts17.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Espace dans Unity
-----------------
<img data-src="webGL_files/Unity/im8.png" style="background:none; border:none; box-shadow:none;max-width:90%;max-height:60%;">



Création et manipulation d'objets
----------------------------------

- Créer un nouveau **projet 3D**
- Ajouter un **Cube** (**GameObject**→**3D Object**)
- Un cube de taille 1x1x1 est alors créé aux **coordonnées** (0,0,0)
- Pour le manipuler, utiliser les boutons en haut à gauche.
<p align="center"><img data-src="webGL_files/Unity/jeu6.png" style="background:none; border:none; box-shadow:none;max-height:50%;"></p>
	-   **vue** (raccourci **Q**) permet de se déplacer dans la scène
	-   **translation** (raccourci **W**) permet de déplacer l'objet
	-   **rotation** (raccourci **E**) permet de faire tourner l'objet
	-   **scale** (raccourci **R**) permet de modifier l'échelle



2 - Maillage
------------
</br><img data-src="webGL_files/images/Pts18.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Approximation polyédrique
-------------------------

Approximation par des facettes planes à partir de points répartis sur la
surface

 <div style="text-align: right; float: right; width: 60%">
1.  Tenir compte de la géométrie de l'objet pour préserver les arêtes
    vives
2.  S'appuyer sur un paramétrage des faces
3.  Trianguler
4.  Réunir les triangles en bandes (strip)
5.  Mettre les sommets en commun (géométrie indexée)
</div>
<span class="fragment fade-out" data-fragment-index="1">
</br><img data-src="webGL_files/Unity/im9a.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<span class="fragment fade-out" data-fragment-index="2">
</br><img data-src="webGL_files/Unity/im9b.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="2">
<span class="fragment fade-out" data-fragment-index="3">
</br><img data-src="webGL_files/Unity/im9c.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="3">
<span class="fragment fade-out" data-fragment-index="4">
</br><img data-src="webGL_files/Unity/im9d.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>
<span class="fragment fade-in" data-fragment-index="4">
</br><img data-src="webGL_files/Unity/im9e.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;position:absolute;top:170pt;left:0pt;">
</span>	</span>



Complexité du maillage
----------------------

Augmenter la résolution **&rarr;** Coûteux
</br><img data-src="webGL_files/Unity/im10.png" style="max-width:100%;max-height:100%;left:0pt;">



Backface culling
----------------

-   Optimisation du rendu
-   La façon la plus rapide d'afficher un polygone est de ne pas
    l'afficher
-   Peut être activé ou non

<img data-src="webGL_files/images/Pts23.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Exemple avec une forme en V
---------------------------

-   Une seule face sera affichée au final

<img data-src="webGL_files/images/Pts24.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Ordonnancement des sommets
--------------------------

-   Utilisé pour déterminer si face avant ou arrière
-   Face avant si sens trigonométrique

<img data-src="webGL_files/images/Pts25.png" style="background:none; border:none; box-shadow:none;max-width:50%;max-height:50%;">



Exemple
-------
<iframe  class="stretch" data-src="https://webglfundamentals.org/webgl/webgl-3d-step4.html" data-preload></iframe>



Exemple
-------

Géométrie modélisée sous 3DS Max 

![](webGL_files/Unity/im11a.png)<!-- .element: style=" width: 34%;" -->
<!-- <img data-src="webGL_files/Unity/im11a.png" style="max-height:30%"></br> -->

Géométrie visualisée sous Unity
<!-- <img data-src="webGL_files/Unity/im11b.png" style="max-height:30%;"></br> -->

![](webGL_files/Unity/im11b.png)<!-- .element: style=" width: 34%;" -->



Construction de terrain sous Unity
----------------------------------



### Importer le package

Importer le package Requisite Assets avec uniquement le package **Environment**

<img data-src="webGL_files/altar/altar13.png"><!-- .element: style="width: 80%" -->



### Créer un terrain (menu **GameObject**) 

```Terrain Settings``` &rarr; **Résolution** = 50x50x50

<img data-src="webGL_files/terr/terr1.png"><!-- .element: style="width: 60%" -->



### Modifier le relief du terrain

```Paint terrain``` &rarr; ```Raise or Lower Terrain```

<img data-src="webGL_files/terr/terr2.png"><!-- .element: style="width: 60%" -->



### Ajouter une texture

1. ```Paint terrain``` &rarr; ```Paint texture```
+ ```Edit Terrain Layers``` &rarr; ```Create layer```
+ Choix d'une texture

<img data-src="webGL_files/terr/terr3.png"><!-- .element: style="width: 60%" -->



### Ajouter d'autres textures

+ ```Edit Terrain Layers```1. ```Paint terrain``` ```Create layer```
+ Choix d'autres textures

<img data-src="webGL_files/terr/terr4.png"><!-- .element: style="width: 60%" -->



### Ajouter des arbres

1. ```Paint trees```
+ ```Edit Trees``` &rarr; ```Add Tree```
+ &#9737;  &rarr; choix d'un arbre
+ **Add**
+ Selection sur la carte
 
<img data-src="webGL_files/terr/terr5.png"><!-- .element: style="width: 60%" -->



### Résultat
Possibilité de modifier la densité et la hauteur

<img data-src="webGL_files/terr/terr6.png"><!-- .element: style="width: 60%" -->



### Ajouter de l'herbe

1. ```Paint details```
+ ```Edit details``` &rarr; ```Add Grass Texture```
+ &#9737;  &rarr; choix d'une texture d'herbe avec profil
+ **Add**
  
<img data-src="webGL_files/terr/terr7.png">



### Ajouter de l'herbe

Mettre l'**Opacity** et **Target Strength** au minimum
<img data-src="webGL_files/terr/terr8.png"><!-- .element: style="width: 65%" -->



Fichier Unity
-------------
<div>
-   Enregistrer la **scène**
-   Enregistrer le **projet**
-   Quitter
-   Ouvrir le projet
-   Ajouter de **l'eau**
</div><!-- .element: style="float: left; width: 65%;" -->
<img data-src="webGL_files/terr/water.jpg" style="width:34%;float:right;">



Explorer l'Ile
---------------
<div>
-   Ajouter des murs invisibles autour de l'île
-   Ajouter un **FPSController** au milieu de l'île
-   →**Run** et observer l'île
</div><!-- .element: style="float: left; width: 65%;" -->

<span>
	<div class="fragment fade-out" data-fragment-index="1">
		<img data-src="webGL_files/Unity/im14a.png" style="background:none; border:none; box-shadow:none;width:34%;position:absolute;top:150pt;left:500pt;">
	</div>	
</span>		
<span>
	<div class="fragment fade-in" data-fragment-index="1">
		<img data-src="webGL_files/Unity/im14b.png" style="background:none; border:none; box-shadow:none;width:34%;position:absolute;top:150pt;left:500pt;">
	</div>
</span>



Ajouter une animation
---------------
-   Ajouter un ponton à partir de l'asset store
-   Ajouter un bateau à partir de l'asset store
-   Utiliser l'outil d'animation (**Window**&rarr;**animation**)

<span>
	<div class="fragment fade-out" data-fragment-index="1">
		<img data-src="webGL_files/Unity/animation.png" style="background:none; border:none; box-shadow:none;width:50%;position:absolute;top:200pt;left:200pt;">
	</div>	
</span>		
<span>
	<div class="fragment fade-in" data-fragment-index="1">
		<img data-src="webGL_files/Unity/animation2.png" style="background:none; border:none; box-shadow:none;width:50%;position:absolute;top:200pt;left:200pt;">
	</div>
</span>
<span>
	<div class="fragment fade-in" data-fragment-index="2">
		<img data-src="webGL_files/Unity/animation3.png" style="background:none; border:none; box-shadow:none;width:50%;position:absolute;top:200pt;left:200pt;">
	</div>
</span>



Demo
----

<video  autoplay loop controls muted playsInline>
<source src="webGL_files/terr/anim.mp4" type="video/mp4" />
</video>



Utilisation de Blender
----------------------

-   De nombreux **models 3D** gratuits existent sur internet
-   Sinon, le plus simple utiliser **Blender**

<img data-src="webGL_files/Unity/im15.png" style="width:50%;">



Optimisation du maillage
------------------------

-   Low Poly / High Poly
-   Unity 3D annonce 64k par objet
-   Joue sur temps de calcul / précision
-   Il vaut mieux jouer sur les textures et autres astuces
-   Dépend du matériel (change avec le temps)

<img data-src="webGL_files/Unity/im16.jpg" style="width:50%;">