# Le pipeline graphique

[PF Villard](http://members.loria.fr/PFVillard/)



Plan
----

1.  Notions de *fps*
+  Le pipeline graphique



<iframe  class="stretch" data-src="webGL/demo/intro1.html" data-preload></iframe>



Interactivité
-------------

-   **6 fps** utilisable mais lent
-   Entre **30 fps** et **60 fps** pour les jeux vidéos
-   Lié au matériel
-   Les écrans sont en général à **60Hz**
-   **A tester sur son ordinateur perso !**



<iframe  class="stretch" data-src="https://www.clicktorelease.com/tmp/threejs/mblur/" data-preload></iframe>



Le pipeline graphique
---------------------

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;max-width:100%;max-height:100%;">

Enchaînement séquentiel d'opérations appliquées aux données pour
obtenir l'image



Pipeline graphique : 1 - Unity
------------------------------

→ Elaboration d'une description abstraite de la scène

-   **géométrie** (formes, emplacement)
-   **photométrie** (couleurs, matériaux, textures, opacité, \...)
-   **ambiance** (lumières, son, brouillard)
-   **comportement** (animation, interaction)
-   **visualisation** (type de projection, position d'observation)

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">



Pipeline graphique : 2 - Projection en 2D
-----------------------------------------

→ Pour chaque vertex :

-   Transforme la position **3D** en position **2D**
-   Calcule les **attributes** (ex : **couleur**)
-   Procède au **clipping**

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">

→ Sur le **GPU** et programmable via le **Vertex Shader**



<iframe  class="stretch" data-src="https://www.realtimerendering.com/udacity/transforms.html" data-preload></iframe> 



Clipping
--------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/cam16.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/cam16b.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>



Clipping
--------

</br></br></br></br></br></br></br></br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/cam17_1.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>
<span class="fragment fade-out" data-fragment-index="2">
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/cam17_2.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span></span>
<span class="fragment fade-out" data-fragment-index="3">
<span class="fragment fade-in" data-fragment-index="2">
<img data-src="webGL_files/images/cam17_3.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span></span>
<span class="fragment fade-out" data-fragment-index="4">
<span class="fragment fade-in" data-fragment-index="3">
<img data-src="webGL_files/images/cam17_4.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span></span>
<span class="fragment fade-in" data-fragment-index="4">
<img data-src="webGL_files/images/cam17_5.png" style="background:none; border:none; box-shadow:none;max-width:200%;max-height:200%;position:absolute;top:100pt;left:150pt;">
</span>
<div style="text-align: left;">
<p><b  class="fragment fade-in" data-fragment-index="5">vertex shader &rarr;</b>(X,Y,Z,W) &nbsp;&nbsp;<font color="blue"  class="fragment fade-in" data-fragment-index="5">Coordonnées clippées</font></p> 
<div class="fragment fade-out" data-fragment-index="5">
<p style="margin-left: 360px;"> W=0 : vecteur</p>
<p style="margin-left: 360px;"> W=1 : point</p>
</div>



Le vertex shader
----------------

<br><br><br><br><br><br><br><br><br>
<span class="fragment fade-out" data-fragment-index="1">
<img data-src="webGL_files/images/shad3_1.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:100pt;">
</span>
<span class="fragment fade-in" data-fragment-index="1">
<img data-src="webGL_files/images/shad3_2.png" style="background:none; border:none; box-shadow:none;max-height:80%;position:absolute;top:100pt;left:100pt;">
</span>



Pipeline graphique : 3 - Rasterisation
--------------------------------------

→ Mise en place des triangles :

-   Recherche les pixels contenus dans chaque triangle
-   Utilise l'algorithme de **Bresenham** :

<img data-src="webGL_files/Unity/Bresenham.png" style="background:none; border:none; box-shadow:none;width:20%;">

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">
→ Sur le **GPU**



Pipeline graphique : 4 - Couleur des pixels
-------------------------------------------

→ Pour chaque triangle :
-   Interpole les **attributes** des vertex sur tout le triangle
-   Calcule la couleur de chaque pixel

<img data-src="webGL_files/images/col1.png" style="background:none; border:none; box-shadow:none;width:50%;">

→ Sur le **GPU** et programmable via le **Fragment Shader**



Le fragment shader
------------------

<img data-src="webGL_files/images/shad4.png" style="background:none; border:none; box-shadow:none;max-height:100%;">



Pipeline graphique : 5 - Z-Buffer
---------------------------------

→ Teste la visibilité des pixels :

-   Teste la **profondeur**
-   Mélange les **couleurs**

<img data-src="webGL_files/Unity/zbuffer.png" style="background:none; border:none; box-shadow:none;width:30%;">

→ Sur le **GPU**



Suite : Modélisation 3D sous Unity
----------------------------------

[Modélisation 3D](#model)
